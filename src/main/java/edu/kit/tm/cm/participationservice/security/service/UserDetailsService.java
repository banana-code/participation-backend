package edu.kit.tm.cm.participationservice.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.security.domain.repositories.AccountRepository;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private static final String NO_SUCH_EMAIL_MSG = "Could not find User with email %s";

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        // we are using email addresses as usernames
        Account userDetails = accountRepository.findByEmail(username);
        if (userDetails == null) {
            throw new UsernameNotFoundException(String.format(NO_SUCH_EMAIL_MSG, username));
        }

        return userDetails;
    }

}

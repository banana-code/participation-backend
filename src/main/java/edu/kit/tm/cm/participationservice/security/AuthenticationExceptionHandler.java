package edu.kit.tm.cm.participationservice.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import edu.kit.tm.cm.participationservice.web.messages.response.ErrorMessage;

@Component
public class AuthenticationExceptionHandler {

    /**
     * AuthenticationException class -> error message
     */
    private Map<Class<?>, String> messages;

    public AuthenticationExceptionHandler() {
        messages = new HashMap<Class<?>, String>();
        messages.put(BadCredentialsException.class, "login_password_wrong");
        messages.put(UsernameNotFoundException.class, "login_email_wrong");
        messages.put(UnconfirmedLoginException.class, "account_unconfirmed");
        messages.put(LockedException.class, "account_locked");
        messages.put(DisabledException.class, "account_disabled");
        messages.put(AccountExpiredException.class, "account_expired");
        messages.put(CredentialsExpiredException.class, "account_credentials_expired");
    }

    public ErrorMessage handleAuthenticationException(AuthenticationException e) {
        // TODO we might return a more abstract errors object
        ErrorMessage errorList = new ErrorMessage();

        String message = messages.get(e.getClass());
        if (message != null) {
            // message from hashmap
            errorList.addError(null, message, null, null);
        } else {
            // hashmap cannot handle exception, so we just return the exception message
            errorList.addError(null, e.getMessage(), null, null);
        }

        return errorList;
    }
}

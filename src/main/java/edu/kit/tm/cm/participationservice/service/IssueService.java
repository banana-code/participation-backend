package edu.kit.tm.cm.participationservice.service;

import java.util.Set;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.Issue.IssueDates;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.repositories.IssueRepository;
import edu.kit.tm.cm.participationservice.web.controller.utility.checkers.GroupMemberChecker;
import edu.kit.tm.cm.participationservice.web.messages.request.IssueMessage;

@Service
public class IssueService {

    @Autowired
    private IssueRepository issueRepository;

    @Autowired
    private GroupMemberChecker groupMemberChecker;

    /**
     * return all issues of group that executer can see (published issues or unpublished issues that executer created).
     *
     * @param executer
     * @param group
     * @return
     */
    @PreAuthorize("@authorizationHandler.userIsAllowedToGetIssues(#executer, #group)")
    public Set<Issue> getVisibleIssuesOfGroup(User executer, GroupEntity group) {
        return issueRepository.findVisibleForUserByGroup(group, executer);
    }

    /**
     * executer creates an issue in group.
     *
     * @param executer
     * @param group
     * @param issueMessage
     * @return
     */
    @PreAuthorize("@authorizationHandler.userIsAllowedToCreateIssue(#executer, #group)")
    public Issue createIssue(User executer, GroupEntity group, IssueMessage issueMessage) {
        DateTime startDate = issueMessage.getStartDateTime();

        // there might be a sending delay
        if (startDate != null && startDate.isBeforeNow()) {
            startDate = DateTime.now();
        }

        Issue createdIssue = new Issue(issueMessage.getTitle(),
                issueMessage.getQuestion(),
                issueMessage.getDescription(),
                issueMessage.getCurrentSolution(),
                new IssueDates(startDate,
                        issueMessage.getSolutionEndDate(),
                        issueMessage.getVoteEndDate()),
                group);

        return issueRepository.save(createdIssue);
    }

    /**
     * user wants to modify issue in group.
     *
     * @param executer
     * @param group
     * @param issue
     * @param changeMessage
     * @return
     */
    @PreAuthorize("@authorizationHandler.userIsAllowedToChangeIssue(#executer, #issue)")
    public Issue changeIssue(User executer, Issue issue, IssueMessage changeMessage) {
        GroupEntity group = issue.getGroup();
        // The creator wants to make changes or publish his Issue
        if (issue.getCreatedBy().equals(executer) && !issue.isPublished()) {
            issue.setTitle(changeMessage.getTitle());
            issue.setDescription(changeMessage.getDescription());
            issue.setCurrentSolution(changeMessage.getCurrentSolution());
            issue.setQuestion(changeMessage.getQuestion());
            DateTime startDate = changeMessage.getStartDateTime();
            if (startDate != null && startDate.isBeforeNow()) {
                startDate = DateTime.now();
            }
            issue.setStartDate(startDate);
            issue.setSolutionEndDate(changeMessage.getSolutionEndDate());
            issue.setVoteEndDate(changeMessage.getVoteEndDate());
            return issueRepository.save(issue);
        }
        // A moderator wants to make spelling changes to the String values
        if (groupMemberChecker.userHasModerationRightsInGroup(executer, group)
                && issue.isPublished()) {
            issue.setTitle(changeMessage.getTitle());
            issue.setDescription(changeMessage.getDescription());
            issue.setCurrentSolution(changeMessage.getCurrentSolution());
            issue.setQuestion(changeMessage.getQuestion());
            return issueRepository.save(issue);
        }
        // should not happen because authorizationHandler handles this
        // (might still happen because of a race condition)
        throw new AccessDeniedException("you must be the creator of the issue or an moderator");
    }

    @PreAuthorize("@authorizationHandler.userIsAllowedToDeleteIssue(#executer, #issue)")
    public void deleteIssue(User executer, Issue issue) {
        issue.getGroup().getIssues().remove(issue);
        issueRepository.delete(issue);
    }
}

package edu.kit.tm.cm.participationservice.security.domain.model;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.kit.tm.cm.participationservice.domain.model.PersistableEntity;
import edu.kit.tm.cm.participationservice.domain.model.User;

/**
 * A account contains authentication information and data about the user whose account it is.
 */
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = "email", name = "unique__account__email") })
// TODO inheriting is only a workaround since the intended won't work:
// ID as identity copy (http://docs.jboss.org/hibernate/stable/annotations/reference/en/html/entity.html#d0e855)
public class Account extends PersistableEntity implements UserDetails {

    private static final long serialVersionUID = 679649845382993263L;

    private static final Set<AccountState> ENABLED_STATES = EnumSet.of(AccountState.ADMIN, AccountState.VALID);

    public enum AccountState {
        ADMIN, VALID, UNCONFIRMED, LOCKED
    }

    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private User user;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private AccountState state = AccountState.VALID;

    @OneToOne(optional = true, cascade = CascadeType.ALL)
    private AuthorizationKey confirmationKey;

    @OneToOne(optional = true, cascade = CascadeType.ALL)
    private AuthorizationKey passwordResetKey;

    protected Account() {}

    /**
     * creates a confirmed account.
     *
     * @param email
     *            the email of the account to login
     * @param user
     *            the user to which these details belong
     * @param password
     *            plain text password
     */
    public Account(User user, String email, String password) {
        this(user, email, password, null);
    }

    /**
     * @param email
     *            the email of the account to login
     * @param user
     *            the user to which these details belong
     * @param password
     *            plain text password
     * @param confirmationKey
     *            if null the account is confirmed
     */
    public Account(User user, String email, String password, AuthorizationKey confirmationKey) {
        super();
        this.user = user;
        setEmail(email);
        setPassword(new BCryptPasswordEncoder().encode(password));
        setConfirmationKey(confirmationKey);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty
    public User getUser() {
        return user;
    }

    public AccountState getState() {
        // admin does not have to confirm
        if (state.equals(AccountState.ADMIN)) {
            return state;
        }
        if (confirmationKey != null) {
            return AccountState.UNCONFIRMED;
        }
        return state;
    }

    public void setState(AccountState state) {
        this.state = state;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            bcrypt encoded password
     */
    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    public AuthorizationKey getConfirmationKey() {
        return confirmationKey;
    }

    public void setConfirmationKey(AuthorizationKey confirmationKey) {
        if (confirmationKey != null) {
            state = AccountState.UNCONFIRMED;
        } else if (state == AccountState.UNCONFIRMED) {
            state = AccountState.VALID;
        }
        this.confirmationKey = confirmationKey;
    }

    @JsonIgnore
    public AuthorizationKey getPasswordResetKey() {
        return passwordResetKey;
    }

    public void setPasswordResetKey(AuthorizationKey passwordResetKey) {
        this.passwordResetKey = passwordResetKey;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (state == AccountState.ADMIN) {
            List<SimpleGrantedAuthority> authorities = new LinkedList<SimpleGrantedAuthority>();
            SimpleGrantedAuthority adminAuthority = new SimpleGrantedAuthority(AccountState.ADMIN.toString());
            authorities.add(adminAuthority);
            return authorities;
        } else {
            return Collections.emptySet();
        }
    }

    @JsonIgnore
    @Override
    public String getUsername() {
        return email;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return state != AccountState.LOCKED;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return ENABLED_STATES.contains(state);
    }
}

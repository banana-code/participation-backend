package edu.kit.tm.cm.participationservice.web.controller.utility.checkers;

import org.springframework.stereotype.Component;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.Membership;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.NotFoundHttpException;

@Component
public class PathChecker {

    public void checkPath(GroupEntity group) {
        if (group == null) {
            throw new NotFoundHttpException("Did not find the Group");
        }
    }

    public void checkPath(GroupEntity group, Issue issue) {
        checkPath(group);
        if (!issue.getGroup().equals(group)) {
            throw new NotFoundHttpException("Did not find the Issue in the Group");
        }
    }

    public void checkPath(Issue issue, Solution solution) {
        if (!solution.getIssue().equals(issue)) {
            throw new NotFoundHttpException("Did not find the Solution in the Issue");
        }
    }

    public void checkPath(GroupEntity group, Issue issue, Solution solution) {
        checkPath(group, issue);
        checkPath(issue, solution);
    }

    public void checkPath(GroupEntity group, Membership membership) {
        checkPath(group);
        if (!membership.getGroup().equals(group)) {
            throw new NotFoundHttpException("Did not find the Membership in the Group");
        }
    }
}

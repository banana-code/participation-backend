package edu.kit.tm.cm.participationservice.web.validator.utility;

public class IndexParser {

    private static final int MIN_INDEX_PARTS = 3;

    protected IndexParser() {}

    public static class Index {

        private String tableName;
        private String[] fields;

        protected Index() {}

        public String getTableName() {
            return tableName;
        }

        public String[] getFields() {
            return fields;
        }
    }

    /**
     *
     * @param indexName
     * @return null if failed
     */
    public static Index parseIndex(String indexName) {
        String[] indexParts = indexName.split("__");
        Index result = new Index();
        if (indexParts.length < MIN_INDEX_PARTS) {
            return null;
        }
        result.tableName = indexParts[1];
        int nFields = indexParts.length - 2;
        result.fields = new String[nFields];
        for (int i = 2; i < indexParts.length; i++) {
            result.fields[i - 2] = indexParts[i];
        }
        return result;
    }
}

package edu.kit.tm.cm.participationservice.web.controller.utility.aspect.sqlexception;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException;
import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException.SqlExceptionType;

@Component
public class H2SqlExceptionCatcher implements AbstractSqlExceptionParser {

    private static final String FIELD_NAME = "field";

    @Value("${dbType}")
    private String dbType;

    private Map<Pattern, SqlExceptionType> exceptionMapping = new HashMap<>();

    public H2SqlExceptionCatcher() {
        exceptionMapping.put(Pattern.compile("index(.*?)\\((?<" + FIELD_NAME + ">.*?)\\)"),
                SqlExceptionType.UNIQUE);
        exceptionMapping.put(Pattern.compile("NULL\\snot\\sallowed(.*?)\"(?<" + FIELD_NAME + ">.*?)\""),
                SqlExceptionType.NULL);
    }

    @Override
    public SqlException parseException(Object entity, DataIntegrityViolationException e) {
        if (!dbType.equals("H2")) {
            return null;
        }
        String exceptionMessage = e.getMostSpecificCause().getMessage();
        for (Pattern pattern : exceptionMapping.keySet()) {
            Matcher matcher = pattern.matcher(exceptionMessage);
            if (matcher.find()) {
                // exceptionType
                SqlExceptionType exceptionType = exceptionMapping.get(pattern);

                // fields involved in exception
                String field = matcher.group(FIELD_NAME);
                Errors errors = new BeanPropertyBindingResult(entity, entity.getClass().getSimpleName());
                String[] fields = field.split(",");
                for (int i = 0; i < fields.length; i++) {
                    fields[i] = fields[i].trim();
                }

                return new SqlException(fields, exceptionType, errors);
            }
        }
        return null;
    }
}

package edu.kit.tm.cm.participationservice.email;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import edu.kit.tm.cm.participationservice.security.domain.model.Account;

@Service
public class EmailService {

    @Autowired
    private EmailTemplates mailTemplates;

    @Autowired
    private JavaMailSender mailSender;

    /**
     * @param to
     *            destination account
     * @param key
     *            key to confirm account.
     * @throws MessagingException
     * @throws MailException
     */
    public void sendRegistrationConfirmationMail(Account to, String key) throws MessagingException {
        mailSender.send(mailTemplates.createRegistrationConfirmationMail(to, key));
    }

    /**
     * @param to
     *            destination account
     * @param key
     *            key to confirm password reset request.
     */
    public void sendPasswordResetRequestMail(Account to, String key) {
        try {
            mailSender.send(mailTemplates.createPasswordResetRequestMail(to, key));
        } catch (MailException | MessagingException e) {
            // TODO we might throw these exceptions
            e.printStackTrace();
        }
    }
}

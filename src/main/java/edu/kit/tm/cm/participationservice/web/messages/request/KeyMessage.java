package edu.kit.tm.cm.participationservice.web.messages.request;

import org.hibernate.validator.constraints.NotEmpty;

public class KeyMessage {

    @NotEmpty
    private String key;

    protected KeyMessage() {}

    public KeyMessage(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}

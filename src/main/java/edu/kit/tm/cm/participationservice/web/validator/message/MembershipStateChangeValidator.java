package edu.kit.tm.cm.participationservice.web.validator.message;

import java.util.EnumSet;
import java.util.Set;

import edu.kit.tm.cm.participationservice.domain.model.Membership.MembershipState;
import edu.kit.tm.cm.participationservice.web.messages.request.MembershipStateChangeMessage;
import edu.kit.tm.cm.participationservice.web.validator.utility.AbstractValidator;
import edu.kit.tm.cm.participationservice.web.validator.utility.ErrorsHelper;

public class MembershipStateChangeValidator extends AbstractValidator<MembershipStateChangeMessage> {

    private static final Set<MembershipState> MEMBER_STATES = EnumSet.of(MembershipState.MEMBER,
            MembershipState.MODERATOR, MembershipState.OWNER);

    protected MembershipStateChangeValidator() {
        super(MembershipStateChangeMessage.class);
    }

    @Override
    public void validateInternal(MembershipStateChangeMessage membershipStateChangeMessage, ErrorsHelper errorsHelper) {
        if (!isMemberState(membershipStateChangeMessage.getMembershipState())) {
            errorsHelper.reject("membershipState", "membership_state_invalid");
        }
    }

    private boolean isMemberState(String state) {
        for (MembershipState membershipState : MEMBER_STATES) {
            if (membershipState.name().equals(state)) {
                return true;
            }
        }
        return false; // Called when no match is found
    }
}

package edu.kit.tm.cm.participationservice.web.messages.request;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import edu.kit.tm.cm.participationservice.GlobalConstants;

public class SolutionMessage {

    @NotEmpty(message = GlobalConstants.EM_SOLUTION_TITLE_EMPTY)
    @Size(max = GlobalConstants.MAX_SOLUTION_TITLE_LENGTH, message = GlobalConstants.EM_SOLUTION_TITLE_TOO_LONG)
    private String title;

    @Size(max = GlobalConstants.MAX_SOLUTION_DESCRIPTION_LENGTH,
            message = GlobalConstants.EM_SOLUTION_DESCRIPTION_TOO_LONG)
    private String description;

    protected SolutionMessage() {}

    public SolutionMessage(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}

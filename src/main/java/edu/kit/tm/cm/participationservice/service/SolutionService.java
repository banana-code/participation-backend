package edu.kit.tm.cm.participationservice.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.repositories.SolutionRepository;
import edu.kit.tm.cm.participationservice.web.messages.request.SolutionMessage;
import edu.kit.tm.cm.participationservice.web.validator.utility.ErrorsHelper;

@Service
public class SolutionService {

    @Autowired
    private SolutionRepository solutionRepository;

    /**
     * executer wants to get all issues that are visible (issues that executer created and pulished issues)
     * 
     * @param executer
     * @param issue
     * @return
     */
    @PreAuthorize("@authorizationHandler.userIsAllowedToGetSolutions(#executer, #issue)")
    public Set<Solution> getVisibleSolutions(User executer, Issue issue) {
        return solutionRepository.findVisibleForUserByIssue(issue, executer);
    }

    /**
     * executer tries to create a solution for issue.
     * 
     * @param executer
     * @param issue
     * @param solutionMessage
     * @return
     */
    @PreAuthorize("@authorizationHandler.userIsAllowedToCreateSolution(#executer, #issue)")
    public Solution createSolution(User executer, Issue issue, SolutionMessage solutionMessage) {
        return solutionRepository.save(
                new Solution(solutionMessage.getTitle(), solutionMessage.getDescription(), issue));
    }

    /**
     * executer tries to change solution.
     * 
     * @param executer
     * @param solution
     * @param solutionMessage
     * @return
     */
    @PreAuthorize("@authorizationHandler.userIsAllowedToChangeSolution(#executer, #solution)")
    public Solution changeSolution(User executer, Solution solution, SolutionMessage solutionMessage) {
        solution.setTitle(solutionMessage.getTitle());
        solution.setDescription(solutionMessage.getDescription());
        return solutionRepository.save(solution);
    }

    @PreAuthorize("@authorizationHandler.userIsAllowedToDeleteSolution(#executer, #solution)")
    public void deleteSolution(User executer, Solution solution) {
        ErrorsHelper errorsHelper = new ErrorsHelper(solution);
        // solution is a zeroOption
        if (solution.getIssue().getZeroOption().equals(solution)) {
            errorsHelper.reject(GlobalConstants.EM_SOLUTION_DELETION_ZERO_OPTION).throwErrors();
        }
        solution.getIssue().getSolutions().remove(solution);
        solutionRepository.delete(solution);
    }
}

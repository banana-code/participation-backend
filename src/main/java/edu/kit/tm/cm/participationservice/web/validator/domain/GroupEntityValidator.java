package edu.kit.tm.cm.participationservice.web.validator.domain;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException.SqlExceptionType;
import edu.kit.tm.cm.participationservice.web.validator.utility.AbstractValidator;
import edu.kit.tm.cm.participationservice.web.validator.utility.MultiKeyMap;

public class GroupEntityValidator extends AbstractValidator<GroupEntity> {

    protected GroupEntityValidator() {
        super(GroupEntity.class);
    }

    @Override
    protected void sqlExceptionErrorMessageMapping(MultiKeyMap errorMapping) {
        errorMapping.put(GlobalConstants.EM_GROUP_TITLE_TAKEN, SqlExceptionType.UNIQUE, "title");
    }
}

package edu.kit.tm.cm.participationservice.domain.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;

/**
 * A User is a person who uses the system, e.g. creates issues and solutions.<br>
 * This class contains no information about the authentication.<br>
 * If a user is deleting his account this object will remain.
 */
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = "alias", name = "unique__user__alias") })
public class User extends PersistableEntity {

    private static final long serialVersionUID = 4091145500656350849L;

    @Column(nullable = false)
    private String alias;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;

    @OneToOne(cascade = CascadeType.REMOVE, mappedBy = "user")
    private Account account;

    protected User() {}

    public User(String alias) {
        setAlias(alias);
    }

    public void setAlias(String alias) {
        Assert.notNull(alias, "The alias can not be null");
        this.alias = alias;
    }

    public String getAlias() {
        if (account == null) {
            return GlobalConstants.ALIAS_DELETED_USER;
        }
        return alias;
    }

    @JsonIgnore
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @JsonIgnore
    public DateTime getRegistrationDate() {
        return registrationDate == null ? null : new DateTime(registrationDate);
    }
}

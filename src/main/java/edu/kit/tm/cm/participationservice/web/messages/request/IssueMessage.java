package edu.kit.tm.cm.participationservice.web.messages.request;

import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;

import edu.kit.tm.cm.participationservice.GlobalConstants;

public class IssueMessage {

    @NotEmpty(message = GlobalConstants.EM_ISSUE_TITLE_EMPTY)
    @Size(max = GlobalConstants.MAX_ISSUE_TITLE_LENGTH, message = GlobalConstants.EM_ISSUE_TITLE_TOO_LONG)
    private String title;
    private String question;

    // TODO MAXLENGTH currentSolution and Question

    @Size(max = GlobalConstants.MAX_ISSUE_DESCRIPTION_LENGTH, message = GlobalConstants.EM_ISSUE_DESCRIPTION_TOO_LONG)
    private String description;
    private String currentSolution;

    private String startDate;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime solutionEndDate;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime voteEndDate;

    public IssueMessage() {}

    public IssueMessage(String title, String question, String description, String currentSolution,
            String startDate, DateTime solutionEndDate, DateTime voteEndDate) {
        this.title = title;
        this.question = question;
        this.description = description;
        this.currentSolution = currentSolution;
        this.startDate = startDate;
        this.solutionEndDate = solutionEndDate;
        this.voteEndDate = voteEndDate;
    }

    public String getTitle() {
        return title;
    }

    public String getQuestion() {
        return question;
    }

    public String getDescription() {
        return description;
    }

    public String getCurrentSolution() {
        return currentSolution;
    }

    public String getStartDate() {
        return startDate;
    }

    public DateTime getSolutionEndDate() {
        return solutionEndDate;
    }

    public DateTime getVoteEndDate() {
        return voteEndDate;
    }

    public DateTime getStartDateTime() {
        if (startDate == null) {
            return null;
        }
        if (startDate.equals("now")) {
            return DateTime.now();
        }
        DateTime date = new DateTime(startDate);
        return date;
    }
}

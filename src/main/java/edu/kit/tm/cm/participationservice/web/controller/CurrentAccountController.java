package edu.kit.tm.cm.participationservice.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.service.AccountService;
import edu.kit.tm.cm.participationservice.web.messages.request.AliasChangeMessage;
import edu.kit.tm.cm.participationservice.web.messages.request.PasswordChangeMessage;

public class CurrentAccountController extends AbstractController {

    private static final String PREFIX = "/me";

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = PREFIX, method = RequestMethod.GET)
    @ResponseBody
    public Account getCurrentAccount(@AuthenticationPrincipal Account account) {
        return account;
    }

    @RequestMapping(value = PREFIX + "/password", method = RequestMethod.PATCH)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public void changePassword(@AuthenticationPrincipal Account account,
            @Valid @RequestBody PasswordChangeMessage passwordChange) {
        accountService.changePassword(account.getUser(), passwordChange);
    }

    @RequestMapping(value = PREFIX + "/alias", method = RequestMethod.PATCH)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public void changeAlias(@AuthenticationPrincipal Account account,
            @Valid @RequestBody AliasChangeMessage aliasChange) {
        accountService.changeAlias(account.getUser(), aliasChange);
    }

    @RequestMapping(value = PREFIX, method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteMe(@AuthenticationPrincipal Account account) {
        accountService.deleteOwnAccount(account.getUser());
    }
}

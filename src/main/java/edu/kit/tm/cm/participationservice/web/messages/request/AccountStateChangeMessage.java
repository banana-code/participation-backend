package edu.kit.tm.cm.participationservice.web.messages.request;

import edu.kit.tm.cm.participationservice.security.domain.model.Account.AccountState;

public class AccountStateChangeMessage {

    private AccountState accountState;

    protected AccountStateChangeMessage() {}

    public AccountStateChangeMessage(AccountState accountState) {
        this.accountState = accountState;
    }

    public AccountState getAccountState() {
        return accountState;
    }

}

package edu.kit.tm.cm.participationservice.web.validator.domain;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException.SqlExceptionType;
import edu.kit.tm.cm.participationservice.web.validator.utility.AbstractValidator;
import edu.kit.tm.cm.participationservice.web.validator.utility.MultiKeyMap;

public class UserValidator extends AbstractValidator<User> {

    protected UserValidator() {
        super(User.class);
    }

    @Override
    protected void sqlExceptionErrorMessageMapping(MultiKeyMap errorMapping) {
        errorMapping.put(GlobalConstants.EM_ALIAS_TAKEN, SqlExceptionType.UNIQUE, "alias");
    }
}

package edu.kit.tm.cm.participationservice.domain.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.web.controller.utility.checkers.UserStatistics;

/**
 * An Issue is a problem which is discussed by users. Every issue belongs to a group which is a "category"/group of
 * people who share the same interests.
 */
@Entity
public class Issue extends AuditableEntity implements Publishable {

    private static final long serialVersionUID = -3968676844011324972L;

    public enum IssueState {
        UNPUBLISHED, OPEN, VOTE_ONLY, CLOSED
    }

    @Column(nullable = false)
    private String title;

    @Column(columnDefinition = "TEXT")
    private String question;

    @Column(length = GlobalConstants.MAX_ISSUE_DESCRIPTION_LENGTH)
    private String description;

    @Column(columnDefinition = "TEXT")
    private String currentSolution;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime startDate;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime solutionEndDate;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime voteEndDate;

    @OneToOne(cascade = { CascadeType.REMOVE, CascadeType.PERSIST })
    private Solution zeroOption = new Solution(GlobalConstants.ZERO_OPTION_DEFAULT_TITLE,
            GlobalConstants.ZERO_OPTION_DEFAULT_DESCRIPTION, this);

    @ManyToOne
    @JoinColumn(nullable = false)
    private GroupEntity group;

    // TODO for testing purpose lazy loading currently unavailable
    // TODO without eager tests won't work
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "issue", fetch = FetchType.EAGER)
    private Set<Solution> solutions = new HashSet<Solution>();

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "issue", fetch = FetchType.EAGER)
    private Set<Comment> comments = new HashSet<Comment>();

    protected Issue() {}

    public Issue(String title, String question, String description, String currentSolution,
            IssueDates dates, GroupEntity group) {
        setTitle(title);
        setQuestion(question);
        setDescription(description);
        setCurrentSolution(currentSolution);
        setStartDate(dates.getStartDate());
        setSolutionEndDate(dates.getSolutionEndDate());
        setVoteEndDate(dates.getVoteEndDate());
        this.group = group;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String newVal) {
        question = newVal;
    }

    @JsonIgnore
    public Solution getZeroOption() {
        return zeroOption;
    }

    public long getZeroOptionID() {
        return this.zeroOption.getId();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String desc) {
        description = desc;
    }

    public String getCurrentSolution() {
        return currentSolution;
    }

    public void setCurrentSolution(String currentSolution) {
        this.currentSolution = currentSolution;
    }

    @JsonIgnore
    public Set<Solution> getSolutions() {
        return solutions;
    }

    public int getSolutionCount() {
        return solutions.size();
    }

    /**
     * @return solution title of the solution with the least resistance.
     */
    public String getBestSolution() {
        if (solutions.isEmpty()) {
            return zeroOption.getTitle();
        }
        return Collections.max(solutions).getTitle();
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getVoteEndDate() {
        return voteEndDate;
    }

    public void setVoteEndDate(DateTime voteEndDate) {
        this.voteEndDate = voteEndDate;
    }

    public DateTime getSolutionEndDate() {
        return solutionEndDate;
    }

    public void setSolutionEndDate(DateTime solutionEndDate) {
        this.solutionEndDate = solutionEndDate;
    }

    @JsonIgnore
    public GroupEntity getGroup() {
        return group;
    }

    public void setGroup(GroupEntity group) {
        this.group = group;
    }

    @JsonIgnore
    public Set<Comment> getComments() {
        return comments;
    }

    /**
     * @return true if publishedDate is set and publishedDate is before now.
     */
    @Override
    @JsonIgnore
    public boolean isPublished() {
        return startDate != null && startDate.isBeforeNow();
    }

    @JsonProperty
    public long getGroupId() {
        return group.getId();
    }

    @JsonProperty
    public String getGroupTitle() {
        return group.getTitle();
    }

    /**
     * a user is participating in an issue if he/she:<br>
     *
     * <ul>
     * <li>created this issue</li>
     * <li>created a solution</li>
     * <li>rated a solution</li>
     * </ul>
     *
     * @return the number of participated users.
     */
    @JsonProperty
    public int getParcitipatedUserCount() {
        int nUsers = 0;
        Set<Membership> members = this.group.getMembers();
        for (Membership member : members) {
            if (UserStatistics.userIsParticipatingInIssue(member.getUser(), this)) {
                nUsers++;
            }
        }
        return nUsers;
    }

    public IssueState getState() {
        if (startDate == null || solutionEndDate == null || voteEndDate == null || startDate.isAfterNow()) {
            return IssueState.UNPUBLISHED;
        } else if (solutionEndDate.isAfterNow() && voteEndDate.isAfterNow()) {
            return IssueState.OPEN;
        } else if (voteEndDate.isAfterNow()) {
            return IssueState.VOTE_ONLY;
        } else {
            return IssueState.CLOSED;
        }
    }

    /**
     * this class contains the dates information about an issue.
     */
    public static class IssueDates {

        private DateTime startDate;
        private DateTime solutionEndDate;
        private DateTime voteEndDate;

        public IssueDates(DateTime startDate, DateTime solutionEndDate, DateTime voteEndDate) {
            this.startDate = startDate;
            this.solutionEndDate = solutionEndDate;
            this.voteEndDate = voteEndDate;
        }

        public DateTime getStartDate() {
            return startDate;
        }

        public DateTime getSolutionEndDate() {
            return solutionEndDate;
        }

        public DateTime getVoteEndDate() {
            return voteEndDate;
        }
    }
}

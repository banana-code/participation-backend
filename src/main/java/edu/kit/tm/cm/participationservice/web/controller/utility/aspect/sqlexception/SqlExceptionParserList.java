package edu.kit.tm.cm.participationservice.web.controller.utility.aspect.sqlexception;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException;

@Component
public class SqlExceptionParserList implements AbstractSqlExceptionParser {

    private List<AbstractSqlExceptionParser> sqlExceptionParsers = new LinkedList<>();

    @Autowired
    protected SqlExceptionParserList(ConfigurableApplicationContext ctx) {
        Collection<AbstractSqlExceptionParser> allAbstractSqlExceptionCatcher = BeanFactoryUtils.
                beansOfTypeIncludingAncestors(ctx, AbstractSqlExceptionParser.class).values();

        for (AbstractSqlExceptionParser curSqlExceptionCatcher : allAbstractSqlExceptionCatcher) {
            sqlExceptionParsers.add(curSqlExceptionCatcher);
        }
    }

    @Override
    public SqlException parseException(Object entity, DataIntegrityViolationException e) {
        for (AbstractSqlExceptionParser curSqlExceptionCatcher : sqlExceptionParsers) {
            SqlException eNew = curSqlExceptionCatcher.parseException(entity, e);
            if (eNew != null) {
                return eNew;
            }
        }
        return null;
    }
}

package edu.kit.tm.cm.participationservice.web.validator.utility;

import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.ErrorMessageException;

/**
 * wrapper class around error, with additional methods to add errors.
 */
public class ErrorsHelper {

    private Errors errors;
    private String field;
    private boolean lastConditionTrue;

    /**
     * creates a new <code>BeanPropertyBidingResult</code> with <code>target</code> and its class name as parameter.
     *
     * @param target
     */
    public ErrorsHelper(Object target) {
        this.errors = new BeanPropertyBindingResult(target, target.getClass().getSimpleName());
    }

    /**
     * creates a new <code>BeanPropertyBidingResult</code> with <code>target</code> and <code>targetname</code> as
     * parameter.
     *
     * @param target
     */
    public ErrorsHelper(Object target, String targetname) {
        this.errors = new BeanPropertyBindingResult(target, targetname);
    }

    /**
     * wraps this ErrorsHelper around the errors parameter.
     *
     * @param errors
     */
    public ErrorsHelper(Errors errors) {
        this.errors = errors;
    }

    /**
     * @param field
     *            all following reject() calls will use this parameter, <br>
     *            unless you specify following rejects with an additional field parameter.
     */
    public void useField(String field) {
        this.field = field;
    }

    /**
     * reject message with the predefined field.<br>
     * to predefine a field use <code>useField(String fieldName)</code>
     *
     * @param message
     * @return
     */
    public ErrorsHelper reject(String message) {
        reject(this.field, message);
        return this;
    }

    /**
     * rejects field with a message.
     *
     * @param field
     * @param message
     * @return
     */
    public ErrorsHelper reject(String field, String message) {
        // we resolve codes at frontend,
        // so we simply use the given errorCode as defaultMessage
        errors.rejectValue(field, null, message);
        return this;
    }

    /**
     * reject if <code>condition</code> is true with message and the predefined field.<br>
     * to predefine a field use <code>useField(String fieldName)</code>
     *
     * @param message
     * @param condition
     */
    public ErrorsHelper rejectIf(String message, boolean condition) {
        return rejectIf(this.field, message, condition);
    }

    /**
     * reject if <code>condition</code> is true with message and field parameter.
     *
     * @param field
     * @param message
     * @param condition
     * @return
     */
    public ErrorsHelper rejectIf(String field, String message, boolean condition) {
        // we resolve codes at frontend,
        // so we simply use the given errorCode as defaultMessage
        int errorCount = errors.getErrorCount();
        if (condition) {
            reject(field, message);
        }
        lastConditionTrue = errors.getErrorCount() > errorCount;
        return this;
    }

    /**
     * reject with message, if the fieldvalue of the predefined field is empty.<br>
     * to predefine a field use <code>useField(String fieldName)</code>
     *
     * @param message
     * @return
     */
    public ErrorsHelper rejectIfEmpty(String message) {
        return rejectIfEmpty(this.field, message);
    }

    /**
     * reject with message, if the field with the name <code>field</code> is empty.
     *
     * @param field
     * @param message
     * @return
     */
    public ErrorsHelper rejectIfEmpty(String field, String message) {
        // TODO should call rejectIf
        int errorCount = errors.getErrorCount();
        ValidationUtils.rejectIfEmpty(errors, field, null, message);
        lastConditionTrue = errors.getErrorCount() > errorCount;
        return this;
    }

    /**
     * reject with message, if the fieldvalue of the predefined field is empty or whitespace.<br>
     * to predefine a field use <code>useField(String fieldName)</code>
     *
     * @param message
     * @return
     */
    public ErrorsHelper rejectIfEmptyOrWhitespace(String message) {
        return rejectIfEmptyOrWhitespace(this.field, message);
    }

    /**
     * reject with message, if the field with the name <code>field</code> is empty or whitespace.
     *
     * @param field
     * @param message
     * @return
     */
    public ErrorsHelper rejectIfEmptyOrWhitespace(String field, String message) {
        // TODO should call rejectIf
        int errorCount = errors.getErrorCount();
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, field, null, message);
        lastConditionTrue = errors.getErrorCount() > errorCount;
        return this;
    }

    /**
     * throws an ErrorMessageException with this ErrorHelpers object.
     */
    public void throwErrors() {
        throw new ErrorMessageException(this);
    }

    /**
     * @return true if the last rejectIf** call added an error.
     */
    public boolean lastConditionTrue() {
        return lastConditionTrue;
    }

    public Errors getErrors() {
        return errors;
    }

    public boolean hasErrors() {
        return errors.hasErrors();
    }
}

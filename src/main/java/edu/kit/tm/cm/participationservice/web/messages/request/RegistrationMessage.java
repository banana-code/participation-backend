package edu.kit.tm.cm.participationservice.web.messages.request;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import edu.kit.tm.cm.participationservice.GlobalConstants;

public class RegistrationMessage {

    @NotEmpty(message = GlobalConstants.EM_ALIAS_EMPTY)
    @Size(max = GlobalConstants.MAX_ALIAS_LENGTH, message = GlobalConstants.EM_ALIAS_TOO_LONG)
    private String alias;

    @NotEmpty
    private String email;

    @NotEmpty(message = GlobalConstants.EM_PASSWORD_INVALID)
    private String password;

    protected RegistrationMessage() {}

    public RegistrationMessage(String alias, String email, String password) {
        this.alias = alias;
        this.email = email;
        this.password = password;
    }

    public String getAlias() {
        return alias;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}

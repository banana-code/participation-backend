package edu.kit.tm.cm.participationservice.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import edu.kit.tm.cm.participationservice.domain.model.Comment;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.repositories.CommentRepository;
import edu.kit.tm.cm.participationservice.web.messages.request.CommentMessage;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @PreAuthorize("@authorizationHandler.userIsAllowedToGetCommentsFromIssue(#executer, #issue)")
    public List<Comment> getCommentsFromIssue(User executer, Issue issue) {
        return convertSetToSortedList(issue.getComments());
    }

    @PreAuthorize("@authorizationHandler.userIsAllowedToCreateCommentAtIssue(#executer, #issue)")
    public Comment createCommentAtIssue(User executer, Issue issue, CommentMessage commentMessage) {
        return commentRepository.save(new Comment(issue, commentMessage.getContent()));
    }

    @PreAuthorize("@authorizationHandler.userIsAllowedToGetCommentsFromSolution(#executer, #solution)")
    public List<Comment> getCommentsFromSolution(User executer, Solution solution) {
        return convertSetToSortedList(solution.getComments());
    }

    @PreAuthorize("@authorizationHandler.userIsAllowedToCreateCommentAtSolution(#executer, #solution)")
    public Comment createCommentAtSolution(User executer, Solution solution, CommentMessage commentMessage) {
        return commentRepository.save(new Comment(solution, commentMessage.getSolutionCommentType(),
                commentMessage.getContent()));
    }

    @PreAuthorize("@authorizationHandler.userIsAllowedToGetComment(#executer, #comment)")
    public Comment getOneComment(User executer, Comment comment) {
        return comment;
    }

    @PreAuthorize("@authorizationHandler.userIsAllowedToCreateResponse(#executer, #comment)")
    public Comment createResponse(User executer, Comment comment, CommentMessage commentMessage) {
        return commentRepository.save(new Comment(comment, commentMessage.getContent()));
    }

    @PreAuthorize("@authorizationHandler.userIsAllowedToChangeComment(#executer, #comment)")
    public Comment changeComment(User executer, Comment comment, CommentMessage commentMessage) {
        comment.setContent(commentMessage.getContent());
        return commentRepository.save(comment);
    }

    @PreAuthorize("@authorizationHandler.userIsAllowedToChangeComment(#executer, #comment)")
    public void deleteComment(User executer, Comment comment) {
        commentRepository.delete(comment);
    }

    private List<Comment> convertSetToSortedList(Set<Comment> comments) {
        List<Comment> responsesList = new ArrayList<Comment>(comments);
        Collections.sort(responsesList);
        return responsesList;
    }
}

package edu.kit.tm.cm.participationservice.web.messages.request;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.domain.model.Comment.SolutionCommentType;

public class CommentMessage {

    @NotEmpty(message = GlobalConstants.EM_COMMENT_EMPTY)
    @Size(max = GlobalConstants.MAX_COMMENT_LENGTH, message = GlobalConstants.EM_COMMENT_TOO_LONG)
    private String content;

    private SolutionCommentType solutionCommentType;

    public CommentMessage() {}

    public String getContent() {
        return content;
    }

    public SolutionCommentType getSolutionCommentType() {
        return solutionCommentType;
    }

}

package edu.kit.tm.cm.participationservice.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import edu.kit.tm.cm.participationservice.domain.model.Comment;
import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.service.CommentService;
import edu.kit.tm.cm.participationservice.web.messages.request.CommentMessage;

public class CommentController extends AbstractController {

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/groups/{groupID}/issues/{issueID}/comments", method = RequestMethod.GET)
    @ResponseBody
    public List<Comment> getCommentsFromIssue(
            @AuthenticationPrincipal Account account,
            @PathVariable("issueID") Issue issue,
            @PathVariable("groupID") GroupEntity group) {

        return commentService.getCommentsFromIssue(account.getUser(), issue);
    }

    @RequestMapping(value = "/groups/{groupID}/issues/{issueID}/comments", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Comment createCommentAtIssue(
            @AuthenticationPrincipal Account account,
            @PathVariable("issueID") Issue issue,
            @PathVariable("groupID") GroupEntity group,
            @Valid @RequestBody CommentMessage commentMessage) {

        return commentService.createCommentAtIssue(account.getUser(), issue, commentMessage);
    }

    @RequestMapping(
            value = "/groups/{groupID}/issues/{issueID}/solutions/{solutionID}/comments",
            method = RequestMethod.GET)
    @ResponseBody
    public List<Comment> getCommentsFromSolution(
            @AuthenticationPrincipal Account account,
            @PathVariable("solutionID") Solution solution,
            @PathVariable("issueID") Issue issue,
            @PathVariable("groupID") GroupEntity group) {

        return commentService.getCommentsFromSolution(account.getUser(), solution);
    }

    @RequestMapping(
            value = "/groups/{groupID}/issues/{issueID}/solutions/{solutionID}/comments",
            method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Comment createCommentAtSolution(
            @AuthenticationPrincipal Account account,
            @PathVariable("solutionID") Solution solution,
            @PathVariable("issueID") Issue issue,
            @PathVariable("groupID") GroupEntity group,
            @Valid @RequestBody CommentMessage commentMessage) {

        return commentService.createCommentAtSolution(account.getUser(), solution, commentMessage);
    }

    @RequestMapping(
            value = "/comments/{commentID}", method = RequestMethod.GET)
    @ResponseBody
    public Comment getOneComment(
            @AuthenticationPrincipal Account account,
            @PathVariable("commentID") Comment comment) {

        return commentService.getOneComment(account.getUser(), comment);
    }

    @RequestMapping(
            value = "/comments/{commentID}", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Comment createResponse(
            @AuthenticationPrincipal Account account,
            @PathVariable("commentID") Comment comment,
            @Valid @RequestBody CommentMessage commentMessage) {

        return commentService.createResponse(account.getUser(), comment, commentMessage);
    }

    @RequestMapping(
            value = "/comments/{commentID}", method = RequestMethod.PATCH)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Comment changeComment(
            @AuthenticationPrincipal Account account,
            @PathVariable("commentID") Comment comment,
            @Valid @RequestBody CommentMessage commentMessage) {

        return commentService.changeComment(account.getUser(), comment, commentMessage);
    }

    @RequestMapping(
            value = "/comments/{commentID}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteComment(
            @AuthenticationPrincipal Account account,
            @PathVariable("commentID") Comment comment) {

        commentService.deleteComment(account.getUser(), comment);
    }
}

package edu.kit.tm.cm.participationservice.domain.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Membership;
import edu.kit.tm.cm.participationservice.domain.model.User;

public interface MembershipRepository extends JpaRepository<Membership, Long> {

    List<Membership> findByUser(User user);

    List<Membership> findByGroup(GroupEntity group);

    Membership findByUserAndGroup(User user, GroupEntity group);

    @Query("SELECT m "
            + "FROM Membership m "
            + "WHERE m.group = :group "
            + "AND (state = 'MEMBER' OR state = 'OWNER' OR state = 'MODERATOR')")
    List<Membership> findAllMemberByGroup(@Param("group") GroupEntity group);

    @Query("SELECT m "
            + "FROM Membership m WHERE m.group = :group AND m.state = 'OWNER'")
    List<Membership> findOwnerByGroup(@Param("group") GroupEntity group);

}

package edu.kit.tm.cm.participationservice.web.controller.utility.exceptions;

import org.springframework.validation.Errors;

public class SqlException extends RuntimeException {

    private static final long serialVersionUID = -5164282260178658175L;

    public enum SqlExceptionType {
        NULL, UNIQUE
    };

    private String[] fields;
    private SqlExceptionType exceptionType;
    private Errors errors;

    /**
     * @param fields
     * @param exceptionType
     * @param errors
     *            errors validated by an abstractValidator.
     */
    public SqlException(String[] fields, SqlExceptionType exceptionType, Errors errors) {
        this.fields = fields;
        this.exceptionType = exceptionType;
        this.errors = errors;
    }

    public String[] getFields() {
        return fields;
    }

    /**
     * @return errors validated by an abstractValidator.
     */
    public SqlExceptionType getExceptionType() {
        return exceptionType;
    }

    public Errors getErrors() {
        return errors;
    }
}

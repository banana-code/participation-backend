package edu.kit.tm.cm.participationservice.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import edu.kit.tm.cm.participationservice.domain.model.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    // @Query("SELECT c "
    // + "FROM Comment c "
    // + "WHERE c.solution = :solution "
    // + "AND c.solutionCommentType = 'PRO'")
    // Set<Comment> findProCommentsOfSolution(@Param("solution") Solution solution);
    //
    // @Query("SELECT c "
    // + "FROM Comment c "
    // + "WHERE c.solution = :solution "
    // + "AND c.solutionCommentType = 'CONTRA'")
    // Set<Comment> findContraCommentsOfSolution(@Param("solution") Solution solution);
    //
    // @Query("SELECT c "
    // + "FROM Comment c "
    // + "WHERE c.solution = :solution "
    // + "AND c.solutionCommentType = 'INFO'")
    // Set<Comment> findInfoCommentsOfSolution(@Param("solution") Solution solution);

}

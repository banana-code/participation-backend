package edu.kit.tm.cm.participationservice.web.messages.request;

import org.hibernate.validator.constraints.NotEmpty;

public class PasswordResetRequestMessage {

    @NotEmpty
    private String email;

    protected PasswordResetRequestMessage() {}

    public PasswordResetRequestMessage(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}

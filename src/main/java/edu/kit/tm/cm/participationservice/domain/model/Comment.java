package edu.kit.tm.cm.participationservice.domain.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.domain.model.Vote.VoteType;

@Entity
public class Comment extends AuditableEntity implements Comparable<Comment>, Publishable {

    private static final long serialVersionUID = -1196529958100111483L;

    public enum CommentBelonging {
        ISSUE, SOLUTION, COMMENT
    }

    public enum SolutionCommentType {
        INFO, PRO, CONTRA
    }

    @Column(nullable = false, length = GlobalConstants.MAX_COMMENT_LENGTH)
    private String content;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "comment", fetch = FetchType.EAGER)
    private Set<Vote> votes = new HashSet<Vote>();

    @ManyToOne
    private Solution solution;

    @ManyToOne
    private Issue issue;

    @ManyToOne
    @JoinColumn(name = "parent_comment_id")
    private Comment parentComment;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "parentComment", fetch = FetchType.EAGER)
    private Set<Comment> responses = new HashSet<Comment>();

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private CommentBelonging commentBelonging;

    @Enumerated(EnumType.STRING)
    @Column
    private SolutionCommentType solutionCommentType;

    protected Comment() {};

    public Comment(Issue issue, String content) {
        this.issue = issue;
        commentBelonging = CommentBelonging.ISSUE;
        this.content = content;
    }

    public Comment(Solution solution, SolutionCommentType solutionCommentType, String content) {
        this.solution = solution;
        commentBelonging = CommentBelonging.SOLUTION;
        this.solutionCommentType = solutionCommentType;
        this.content = content;
    }

    public Comment(Comment parentComment, String content) {
        this.parentComment = parentComment;
        commentBelonging = CommentBelonging.COMMENT;
        this.content = content;
    }

    // Returns all responses sorted
    @JsonProperty
    public List<Comment> getResponses() {
        List<Comment> responsesList = new ArrayList<Comment>(responses);
        Collections.sort(responsesList);
        return responsesList;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @JsonProperty
    public String getContent() {
        return content;
    }

    @JsonIgnore
    public Comment getParentComment() {
        return parentComment;
    }

    @JsonIgnore
    public CommentBelonging getCommentBelonging() {
        return commentBelonging;
    }

    @JsonIgnore
    public Solution getSolution() {
        return solution;
    }

    @JsonIgnore
    public Issue getIssue() {
        return issue;
    }

    @JsonProperty
    public SolutionCommentType getSolutionCommentType() {
        return solutionCommentType;
    }

    @JsonProperty
    public VoteType getUserVote() {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return null;
        }
        for (Vote vote : votes) {
            if (vote.getCreatedBy().equals(currentUser)) {
                return vote.getType();
            }
        }
        return null;
    }

    @JsonIgnore
    public int getVotes() {
        return getUpVotes() - getDownVotes();
    }

    @JsonProperty
    public int getUpVotes() {
        if (votes.isEmpty()) {
            return 0;
        }

        int sum = 0;
        for (Vote vote : votes) {
            if (vote.getType().equals(VoteType.UP)) {
                sum++;
            }
        }
        return sum;
    }

    @JsonProperty
    public int getDownVotes() {
        if (votes.isEmpty()) {
            return 0;
        }

        int sum = 0;
        for (Vote vote : votes) {
            if (vote.getType().equals(VoteType.DOWN)) {
                sum++;
            }
        }
        return sum;
    }

    @Override
    @JsonIgnore
    public boolean isPublished() {
        if (solution != null) {
            return solution.isPublished();
        } else if (issue != null) {
            return issue.isPublished();
        } else if (parentComment != null) {
            return parentComment.isPublished();
        }
        return false;
    }

    @PrePersist
    public void check() {
        boolean a = issue != null;
        boolean b = solution != null;
        boolean c = parentComment != null;

        // Check that a Comment does only link to one other domain object
        if (!((a && b && c) ^ a ^ b ^ c)) {
            throw new IllegalArgumentException("Can link only to one other domain object");
        }

        // Check if the fitting CommentBelongingType is chosen
        if (!((issue != null && commentBelonging == CommentBelonging.ISSUE)
                || (solution != null && commentBelonging == CommentBelonging.SOLUTION)
                || (parentComment != null && commentBelonging == CommentBelonging.COMMENT))) {

            throw new IllegalArgumentException("Relation and CommentBelonging do not match");
        }

        // Check that only Issue Comments do have an IssueCommentType
        if (solutionCommentType != null && commentBelonging != CommentBelonging.SOLUTION) {
            throw new IllegalArgumentException("Only Solution Comments may have an SolutionCommentType");
        }
        // Check if all Issue Comments have an IssueCommentType
        if (solutionCommentType == null && commentBelonging == CommentBelonging.SOLUTION) {
            throw new IllegalArgumentException("Solution Comments need an SolutionCommentType");
        }

    }

    @Override
    public int compareTo(Comment o) {
        if (getVotes() > o.getVotes()) {
            return -1;
        } else if (getVotes() < o.getVotes()) {
            return 1;
        } else {
            return 0;
        }
    }

}

package edu.kit.tm.cm.participationservice.web.messages.response;

import edu.kit.tm.cm.participationservice.security.domain.model.Account;

/**
 * This contains the information of a successful login response which is an account object with the session X-Token
 * string.
 */
public class LoginSuccessMessage extends Account {

    private static final long serialVersionUID = 4271851164595643973L;

    private String rememberMeToken;

    public LoginSuccessMessage(Account account, String token) {
        super(account.getUser(), account.getEmail(), "");
        setId(account.getId());
        setState(account.getState());
        rememberMeToken = token;
    }

    public String getRememberMeToken() {
        return rememberMeToken;
    }
}

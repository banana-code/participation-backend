package edu.kit.tm.cm.participationservice.web.messages.request;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import edu.kit.tm.cm.participationservice.GlobalConstants;

public class GroupChangeMessage {

    @NotEmpty(message = GlobalConstants.EM_GROUP_INVALID_TITLE)
    @Size(max = GlobalConstants.MAX_GROUP_TITLE_LENGTH, message = GlobalConstants.EM_GROUP_TITLE_TOO_LONG)
    private String title;

    @Size(max = GlobalConstants.MAX_GROUP_DESCRIPTION_LENGTH, message = GlobalConstants.EM_GROUP_DESCRIPTION_TOO_LONG)
    private String description;

    protected GroupChangeMessage() {}

    public GroupChangeMessage(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

}

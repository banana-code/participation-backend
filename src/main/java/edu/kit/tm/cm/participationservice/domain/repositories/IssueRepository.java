package edu.kit.tm.cm.participationservice.domain.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.User;

public interface IssueRepository extends JpaRepository<Issue, Long> {

    Set<Issue> findByGroup(GroupEntity group);

    /**
     * for user means: get issues the user is allowed to see
     *
     * @param group
     * @param user
     * @return
     */
    @Query("SELECT i "
            + "FROM Issue i "
            + "WHERE i.group = :group "
            + "AND (i.startDate IS NOT NULL AND i.startDate < CURRENT_TIMESTAMP OR i.createdBy = :user)")
    Set<Issue> findVisibleForUserByGroup(@Param("group") GroupEntity group, @Param("user") User user);

}

package edu.kit.tm.cm.participationservice.demo;

import java.sql.SQLException;

import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Starts a H2 management console accessible at http://localhost:8082.<br>
 * We will start this even in production mode, so the database can be accessed locally.
 *
 * @author Raphael von der Grün
 */
@Configuration
@PropertySource("classpath:application.properties")
public class H2ServerConfiguration {

    @Value("${enableH2WebInterface}")
    private Boolean enableH2WebInterface;

    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server getH2WebServer() throws SQLException {
        if (!enableH2WebInterface) {
            return null;
        }
        return Server.createWebServer();
    }

}

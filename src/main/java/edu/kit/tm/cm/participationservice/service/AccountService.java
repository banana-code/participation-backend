package edu.kit.tm.cm.participationservice.service;

import java.util.EnumSet;
import java.util.Set;

import javax.mail.MessagingException;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.MailException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.repositories.UserRepository;
import edu.kit.tm.cm.participationservice.email.EmailService;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.security.domain.model.AuthorizationKey;
import edu.kit.tm.cm.participationservice.security.domain.model.Account.AccountState;
import edu.kit.tm.cm.participationservice.security.domain.repositories.AccountRepository;
import edu.kit.tm.cm.participationservice.security.service.PersistentHeaderTokenBasedRememberMeServices;
import edu.kit.tm.cm.participationservice.web.messages.request.AliasChangeMessage;
import edu.kit.tm.cm.participationservice.web.messages.request.KeyMessage;
import edu.kit.tm.cm.participationservice.web.messages.request.PasswordChangeMessage;
import edu.kit.tm.cm.participationservice.web.messages.request.PasswordResetConfirmationMessage;
import edu.kit.tm.cm.participationservice.web.messages.request.PasswordResetRequestMessage;
import edu.kit.tm.cm.participationservice.web.messages.request.RegistrationMessage;
import edu.kit.tm.cm.participationservice.web.messages.response.LoginSuccessMessage;
import edu.kit.tm.cm.participationservice.web.validator.utility.ErrorsHelper;

@Service
@PropertySource("classpath:application.properties")
public class AccountService {

    private static final Set<AccountState> ALLOWED_ACCOUNT_STATES = EnumSet.of(AccountState.ADMIN, AccountState.LOCKED,
            AccountState.VALID);

    @Value("${accountRegistrationRequiresConfirmation}")
    private Boolean accountRegistrationRequiresConfirmation;

    @Value("${accountConfirmationTime}")
    private Integer accountConfirmationTime;

    @Value("${passwordResetTime}")
    private Integer passwordResetTime;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private PersistentHeaderTokenBasedRememberMeServices tokenService;

    /**
     * unauthenticated user wants to create an account.
     *
     * @param registration
     * @return
     */
    public Account createAccount(RegistrationMessage registration) {
        User user = new User(registration.getAlias());
        AuthorizationKey confirmationKey = accountRegistrationRequiresConfirmation
                ? new AuthorizationKey(DateTime.now().plusHours(accountConfirmationTime))
                : null;
        Account account = new Account(user, registration.getEmail(), registration.getPassword(), confirmationKey);
        user.setAccount(account);
        account = accountRepository.save(account);
        if (accountRegistrationRequiresConfirmation) {
            // TODO we might use requestcontextholder to get this string
            try {
                emailService.sendRegistrationConfirmationMail(account, confirmationKey.getKey());
            } catch (MailException | MessagingException e) {
                accountRepository.delete(account);
                new ErrorsHelper(registration).reject("email", GlobalConstants.EM_REGISTER_EMAIL_INVALID).throwErrors();
            }
        }
        return account;
    }

    /**
     * unauthentcated user wants to confirm the account creation.
     *
     * @param registrationConfirmation
     * @return
     */
    public LoginSuccessMessage confirmAccount(KeyMessage registrationConfirmation) {
        ErrorsHelper errorsHelper = new ErrorsHelper(registrationConfirmation);

        Account account = accountRepository.findUnconfirmedAccountByRegistrationKey(registrationConfirmation.getKey());
        if (account == null) {
            errorsHelper.reject("key", GlobalConstants.EM_CONFIRMATION_KEY_INVALID).throwErrors();
        }
        account.setConfirmationKey(null);
        accountRepository.save(account);
        String token = tokenService.login(account.getEmail());
        return new LoginSuccessMessage(account, token);
    }

    /**
     * executer wants to change the alias.
     *
     * @param executer
     * @param aliasChange
     */
    public void changeAlias(User executer, AliasChangeMessage aliasChange) {
        Account account = executer.getAccount();
        account.getUser().setAlias(aliasChange.getAlias());
        accountRepository.save(account);
    }

    /**
     * user wants to change the password to authenticate.
     *
     * @param executer
     * @param passwordChangeMessage
     */
    public void changePassword(User executer, PasswordChangeMessage passwordChangeMessage) {
        ErrorsHelper errorsHelper = new ErrorsHelper(passwordChangeMessage);
        Account account = executer.getAccount();

        Account currentAccount = (Account) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!passwordEncoder.matches(passwordChangeMessage.getOldPassword(), currentAccount.getPassword())) {
            errorsHelper.reject(GlobalConstants.EM_PASSWORD_CHANGE_INCORRECT).throwErrors();
        }
        String encodedPassword = passwordEncoder.encode(passwordChangeMessage.getNewPassword());
        account.setPassword(encodedPassword);
        accountRepository.save(account);
    }

    /**
     * the user who forgot his/her password wants to reset it.
     *
     * @param executer
     */
    public void requestPasswordReset(PasswordResetRequestMessage passwordResetRequestMessage) {
        Account account = accountRepository.findByEmail(passwordResetRequestMessage.getEmail());
        if (account == null) {
            new ErrorsHelper(passwordResetRequestMessage).reject("email",
                    GlobalConstants.EM_PASSWORD_RESET_EMAIL_INVALID)
                    .throwErrors();
        }
        AuthorizationKey passwordResetKey = account.getPasswordResetKey();
        DateTime expirationDate = DateTime.now().plusHours(passwordResetTime);
        if (passwordResetKey != null) {
            passwordResetKey.setExpirationDate(expirationDate);
        } else {
            passwordResetKey = new AuthorizationKey(expirationDate);
        }
        account.setPasswordResetKey(passwordResetKey);
        emailService.sendPasswordResetRequestMail(account, passwordResetKey.getKey());
        accountRepository.save(account);
    }

    /**
     *
     * @param passwordReset
     *            message to reset password
     * @return
     */
    public LoginSuccessMessage confirmPasswordReset(PasswordResetConfirmationMessage passwordReset) {
        ErrorsHelper errorsHelper = new ErrorsHelper(passwordReset, "passwordResetConfirmation");
        Account account = accountRepository.findAccountByPasswordResetKey(passwordReset.getKey());
        if (account == null) {
            errorsHelper.reject("key", GlobalConstants.EM_CONFIRMATION_KEY_INVALID).throwErrors();
        }
        account.setPasswordResetKey(null);
        String newPassword = passwordReset.getNewPassword();
        account.setPassword(passwordEncoder.encode(newPassword));
        accountRepository.save(account);
        String token = tokenService.login(account.getEmail());
        return new LoginSuccessMessage(account, token);
    }

    public void deleteOwnAccount(User executer) {
        int random;
        do {
            random = random();
        } while (userRepository.findByAlias("deletedUser#" + random) != null);
        executer.setAlias("deletedUser#" + random);
        Account ownAccount = executer.getAccount();
        accountRepository.delete(ownAccount);
    }

    @PreAuthorize("@authorizationHandler.userIsAllowedToGetAccounts(#executer)")
    public Set<Account> findAccountsByUserAliasPrefix(User executer, String aliasPrefix) {
        return accountRepository.findAccountsByUserAliasPrefix(aliasPrefix);
    }

    @PreAuthorize("@authorizationHandler.userIsAllowedToChangeAccounts(#executer)")
    public Account changeAccountState(User executer, Account account, AccountState accountState) {
        if (ALLOWED_ACCOUNT_STATES.contains(accountState)) {
            account.setState(accountState);
            return accountRepository.save(account);
        }
        return null;
    }

    private int random() {
        return (int) (Math.random() * Integer.MAX_VALUE);
    }
}

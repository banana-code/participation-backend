package edu.kit.tm.cm.participationservice.domain.model;

/**
 * admins are not allowed to modify/delete unpublished content.
 *
 * @author Daniel
 *
 */
public interface Publishable {

    boolean isPublished();
}

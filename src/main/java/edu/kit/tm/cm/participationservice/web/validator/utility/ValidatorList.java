package edu.kit.tm.cm.participationservice.web.validator.utility;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException.SqlExceptionType;

/**
 * contains a list of validators.
 */
@Component
public class ValidatorList implements Validator, SqlExceptionHandler {

    /**
     * A collection objects that inherit AbstractValidator. <br>
     * put a Class<?> object to get all AbstractValidator objects that can validate a object of the given Class<?>.
     */
    private Map<Class<?>, Collection<AbstractValidator<?>>> validators = new HashMap<>();

    /**
     * a collection that contains validators, that do not inherit <code>AbstractValidator</code>.
     */
    private List<Validator> otherValidators = new LinkedList<>();

    @Autowired
    protected ValidatorList(ConfigurableApplicationContext ctx) {
        // get all existing AbstractValidator beans
        @SuppressWarnings("rawtypes")
        // TODO avoid warning
        Collection<AbstractValidator> allAbstractValidators = BeanFactoryUtils.
                beansOfTypeIncludingAncestors(ctx, AbstractValidator.class).values();

        Collection<Validator> allValidators = BeanFactoryUtils.
                beansOfTypeIncludingAncestors(ctx, Validator.class).values();

        // insert all validator beans into validator map.
        for (AbstractValidator<?> curValidator : allAbstractValidators) {
            Class<?> supportedClass = curValidator.getSupportedClass();
            Collection<AbstractValidator<?>> supportedClassValidators = this.validators.get(supportedClass);
            if (supportedClassValidators == null) {
                supportedClassValidators = new LinkedList<>();
                this.validators.put(supportedClass, supportedClassValidators);
            }
            supportedClassValidators.add(curValidator);
        }

        for (Validator curValidator : allValidators) {
            if (curValidator instanceof AbstractValidator<?>) {
                continue;
            }
            otherValidators.add(curValidator);
        }
    }

    @Override
    public boolean supports(Class<?> clazz) {
        if (validators.containsKey(clazz)) {
            return true;
        }
        for (Validator curValidator : otherValidators) {
            if (curValidator.supports(clazz)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        Collection<AbstractValidator<?>> typeValidators = this.validators.get(target.getClass());
        if (typeValidators != null) {
            // call abstractValidators
            for (Validator curValidator : typeValidators) {
                ValidationUtils.invokeValidator(curValidator, target, errors);
            }
        }
        // call other non abstractValidators
        for (Validator curValidator : otherValidators) {
            if (curValidator.supports(target.getClass())) {
                ValidationUtils.invokeValidator(curValidator, target, errors);
            }
        }
    }

    @Override
    public void handleSqlException(Object target, Errors errors, SqlExceptionType type, String... fields) {
        Collection<AbstractValidator<?>> typeValidators = this.validators.get(target.getClass());
        if (typeValidators == null) {
            return;
        }
        for (AbstractValidator<?> curValidator : typeValidators) {
            curValidator.handleSqlException(target, errors, type, fields);
        }
    }
}

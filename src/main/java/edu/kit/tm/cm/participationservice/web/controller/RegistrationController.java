package edu.kit.tm.cm.participationservice.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.security.domain.repositories.AccountRepository;
import edu.kit.tm.cm.participationservice.service.AccountService;
import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.ErrorMessageException;
import edu.kit.tm.cm.participationservice.web.messages.request.KeyMessage;
import edu.kit.tm.cm.participationservice.web.messages.request.PasswordResetConfirmationMessage;
import edu.kit.tm.cm.participationservice.web.messages.request.PasswordResetRequestMessage;
import edu.kit.tm.cm.participationservice.web.messages.request.RegistrationMessage;
import edu.kit.tm.cm.participationservice.web.messages.response.LoginSuccessMessage;
import edu.kit.tm.cm.participationservice.web.validator.utility.ErrorsHelper;

public class RegistrationController extends AbstractController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountRepository accountRepository;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Account register(@Valid @RequestBody RegistrationMessage registration) {
        return accountService.createAccount(registration);
    }

    @RequestMapping(value = "/confirm", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public LoginSuccessMessage registrationConfirmation(@Valid @RequestBody KeyMessage key) {
        if (accountRepository.findUnconfirmedAccountByRegistrationKey(key.getKey()) != null) {
            // key is used to confirm account.
            return accountService.confirmAccount(key);
        }
        // invalid key
        throw new ErrorMessageException(
                new ErrorsHelper(key).reject("key", GlobalConstants.EM_CONFIRMATION_KEY_INVALID));
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void requestPasswordReset(@Valid @RequestBody PasswordResetRequestMessage passwordResetRequestMessage) {
        accountService.requestPasswordReset(passwordResetRequestMessage);
    }

    @RequestMapping(value = "/setNewPassword", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public LoginSuccessMessage confirmPasswordReset(
            @Valid @RequestBody PasswordResetConfirmationMessage passwordResetConfirmation) {
        return accountService.confirmPasswordReset(passwordResetConfirmation);
    }
}

package edu.kit.tm.cm.participationservice.domain.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import edu.kit.tm.cm.participationservice.domain.model.Resistance;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.domain.model.User;

public interface ResistanceRepository extends JpaRepository<Resistance, Long> {

    @Override
    Resistance findOne(Long id);

    @Override
    void delete(Long id);

    @Override
    Page<Resistance> findAll(Pageable pageable);

    Resistance findByCreatedByAndSolution(User user, Solution solution);
}

package edu.kit.tm.cm.participationservice.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Membership;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.model.Membership.MembershipState;
import edu.kit.tm.cm.participationservice.domain.repositories.MembershipRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.UserRepository;
import edu.kit.tm.cm.participationservice.web.controller.utility.checkers.GroupMemberChecker;
import edu.kit.tm.cm.participationservice.web.messages.request.InviteMessage;
import edu.kit.tm.cm.participationservice.web.messages.request.MembershipStateChangeMessage;
import edu.kit.tm.cm.participationservice.web.validator.utility.ErrorsHelper;

@Service
public class MembershipService {

    @Autowired
    private GroupMemberChecker groupMemberChecker;

    @Autowired
    private MembershipRepository membershipRepository;

    @Autowired
    private UserRepository userRepository;

    /**
     * executer wants to get members of group.<br>
     * if executer is owner/moderator in group, this method also returns membership requests and invitations.
     *
     * @param executer
     * @param group
     * @return
     */
    @PreAuthorize("@authorizationHandler.userIsAllowedToGetMemberships(#executer, #group)")
    public List<Membership> getMembers(User executer, GroupEntity group) {
        List<Membership> members = new LinkedList<Membership>();
        if (groupMemberChecker.userHasModerationRightsInGroup(executer, group)) {
            // all memberships, including invited/requested memberships
            members = membershipRepository.findByGroup(group);
        } else {
            // a list with member/moderator/owner
            members = membershipRepository.findAllMemberByGroup(group);
        }
        return members;
    }

    /**
     * creates a membership association between affectedUser and group, <br>
     * a request or an invitation to get into the group. <br>
     * the type of membership is specified by the creator.
     *
     * @param executer
     *            the user who wants to create a membership.
     * @param group
     *            the group of the created membership object.
     * @param affectedUser
     *            the user who is part of the created membership.
     * @return
     */
    @PreAuthorize("@authorizationHandler.userIsAllowedToCreateMembership(#executer, #group, #inviteMessage)")
    public Membership inviteOrRequestMembershipInGroup(User executer, GroupEntity group, InviteMessage inviteMessage) {
        ErrorsHelper errorsHelper = new ErrorsHelper(inviteMessage);
        errorsHelper.useField("userId");

        User affectedUser = userRepository.findOne(inviteMessage.getUserId());
        if (affectedUser == null) {
            // user to invite/user who requested membership does not exist
            errorsHelper.reject(GlobalConstants.EM_INVITE_USER_ID).throwErrors();
        }

        Membership membership = membershipRepository.findByUserAndGroup(affectedUser, group);
        if (membership != null && groupMemberChecker.userIsInGroup(affectedUser, group)) {
            // affectedUser is already in group
            errorsHelper.reject(GlobalConstants.EM_INVITE_ALREADY_MEMBER).throwErrors();
        }

        if (executer.equals(affectedUser)) {
            // creator/affectedUser requests membership in group
            if (membership != null && membership.getState().equals(MembershipState.REQUESTED)) {
                // user already requested membership
                errorsHelper.reject(GlobalConstants.EM_INVITE_ALREADY_REQUESTED).throwErrors();
            }
            return requestMembershipForGroup(group, affectedUser);
        } else if (groupMemberChecker.userHasModerationRightsInGroup(executer, group)) {
            // moderator wants to invite
            if (membership != null && membership.getState().equals(MembershipState.INVITED)) {
                // user is already invited
                errorsHelper.reject(GlobalConstants.EM_INVITE_ALREADY_INVITED).throwErrors();
            }
            return inviteUserToGroup(group, affectedUser);
        } else {
            // creator does not invite himself (requests membership) and does not have moderator permission,
            // so we assume that creator wanted to invite someone else, but he does not have the rights.
            throw new AccessDeniedException("you need moderator permission to invite someone.");
        }
    }

    /**
     * this method changes the state of currentMembership to the state in membershipStateChangeMessage.
     *
     * @param executer
     *            the user who wants to change the membership
     * @param currentMembership
     *            the membership to change (it contains a group, user and state)
     * @param membershipStateChangeMessage
     *            contains the new state for currentMembership
     * @return modified currentMembership
     */
    @PreAuthorize("@authorizationHandler.userIsAllowedToChangeMembership(#executer, #currentMembership, "
            + "T(edu.kit.tm.cm.participationservice.domain.model.Membership.MembershipState)"
            + ".valueOf(#membershipStateChangeMessage.membershipState))")
    public Membership changeMembership(User executer, Membership currentMembership,
            MembershipStateChangeMessage membershipStateChangeMessage) {
        ErrorsHelper errorsHelper = new ErrorsHelper(membershipStateChangeMessage);

        GroupEntity group = currentMembership.getGroup();

        // the executer wants to change the currentMembership's state to membershipState
        MembershipState membershipState = MembershipState.valueOf(membershipStateChangeMessage.getMembershipState());

        // the last owner wants to leave his owner state
        if (membershipState != MembershipState.OWNER && currentMembership.getState() == MembershipState.OWNER) {
            // if a owner wants to leave his owner permission, we synchronize to avoid race conditions and 0 owners
            // this is still very performant, because other changes in memberships do not enter this synchronized block
            // but we can gain better performance by synchronizing a group
            synchronized (this) {
                // this is singleton (@Service)
                if (membershipRepository.findOwnerByGroup(group).size() == 1) {
                    errorsHelper.reject(GlobalConstants.EM_MEMBERSHIP_TOO_FEW_OWNER).throwErrors();
                }
                currentMembership.setState(membershipState);
                return membershipRepository.save(currentMembership);
            }
        }

        currentMembership.setState(membershipState);
        return membershipRepository.save(currentMembership);
    }

    /**
     * executer tries to delete deleteMembership.
     *
     * @param executer
     * @param deleteMembership
     */
    @PreAuthorize("@authorizationHandler.userIsAllowedToDeleteMembership(#executer, #deleteMembership)")
    public void deleteMembership(User executer, Membership deleteMembership) {
        ErrorsHelper errorsHelper = new ErrorsHelper(deleteMembership);

        GroupEntity group = deleteMembership.getGroup();

        if (deleteMembership.getState() == MembershipState.OWNER) {
            synchronized (this) {
                if (membershipRepository.findOwnerByGroup(group).size() == 1) {
                    errorsHelper.reject(GlobalConstants.EM_MEMBERSHIP_TOO_FEW_OWNER).throwErrors();
                }
                deleteMembership.getGroup().getMembers().remove(deleteMembership);
                membershipRepository.delete(deleteMembership);
                return;
            }
        }

        deleteMembership.getGroup().getMembers().remove(deleteMembership);
        membershipRepository.delete(deleteMembership);
    }

    /**
     * this method invites <code>user</code> to <code>group</code>.<br>
     * if user has requested an invitation, user becomes a member.<br>
     * if user has a more powerful membership, e.g. member/mod, this method does not change the membership.
     *
     * @param group
     *            the group <code>user</code> is invited to.
     * @param user
     *            the user which is invited to <code>group</code>
     */
    private Membership inviteUserToGroup(GroupEntity group, User user) {
        Membership curMembership = membershipRepository.findByUserAndGroup(user, group);
        if (curMembership == null) {
            curMembership = new Membership(group, MembershipState.INVITED, user);
        } else if (curMembership.getState() == MembershipState.REQUESTED) {
            curMembership.setState(MembershipState.MEMBER);
        }
        return membershipRepository.save(curMembership);
    }

    /**
     * <code>user</code> requests membership in <code>group</code>.<br>
     * if user is already invited, user becomes a member.<br>
     * if user has a more powerful membership, e.g. member/mod, this method does not change the membership.
     *
     * @param group
     *            the group user wants to be member in.
     * @param user
     *            the user who requests an invite.
     */
    private Membership requestMembershipForGroup(GroupEntity group, User user) {
        Membership curMembership = membershipRepository.findByUserAndGroup(user, group);
        if (curMembership == null) {
            curMembership = new Membership(group, MembershipState.REQUESTED, user);
        } else if (curMembership.getState() == MembershipState.INVITED) {
            curMembership.setState(MembershipState.MEMBER);
        }
        return membershipRepository.save(curMembership);
    }
}

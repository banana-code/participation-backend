package edu.kit.tm.cm.participationservice.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.web.messages.response.ErrorMessage;
import edu.kit.tm.cm.participationservice.web.messages.response.LoginSuccessMessage;

/**
 * This class adds additional information to responses after login/logout.
 */
@Component
public class AuthenticationEventHandler extends ResponseEntityExceptionHandler
        implements AuthenticationSuccessHandler, AuthenticationFailureHandler, LogoutSuccessHandler {

    private static final String TOKEN_HEADER_NAME = "X-Token";

    @Autowired
    private AuthenticationExceptionHandler authenticationExceptionHandler;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException exception) throws IOException, ServletException {

        ErrorMessage errorList = authenticationExceptionHandler.handleAuthenticationException(exception);

        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mapper.writeValueAsString(errorList);

        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.getWriter().write(responseBodyAsJson);
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {

        Account account = (Account) authentication.getPrincipal();

        // get remember-me token
        String token = response.getHeader(TOKEN_HEADER_NAME);

        LoginSuccessMessage responseMessage = new LoginSuccessMessage(account, token);

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JodaModule());

        String responseBodyAsJson = mapper.writeValueAsString(responseMessage);

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().write(responseBodyAsJson);
        response.setHeader(TOKEN_HEADER_NAME, token);
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {

        if (authentication == null) {
            // edge case, should not happen
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return;
        }
        response.setStatus(HttpStatus.OK.value());
    }
}

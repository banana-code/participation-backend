package edu.kit.tm.cm.participationservice.security.domain.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.kit.tm.cm.participationservice.security.domain.model.RememberMeToken;

public interface RememberMeTokenRepository extends JpaRepository<RememberMeToken, Long> {

    RememberMeToken findBySeries(String series);

    List<RememberMeToken> findByEmail(String email);
}

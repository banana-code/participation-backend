package edu.kit.tm.cm.participationservice.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.service.ResistanceService;
import edu.kit.tm.cm.participationservice.web.messages.request.RatingMessage;

public class ResistanceController extends AbstractController {

    @Autowired
    private ResistanceService resistanceService;

    @RequestMapping(
            value = "/groups/{groupID}/issues/{issueID}/solutions/{solutionID}/resistances",
            method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public void createResistance(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group,
            @PathVariable("issueID") Issue issue,
            @PathVariable("solutionID") Solution solution,
            @Valid @RequestBody RatingMessage ratingMessage) {

        resistanceService.createRating(account.getUser(), solution, ratingMessage);
    }
}

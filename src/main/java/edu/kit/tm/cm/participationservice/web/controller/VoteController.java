package edu.kit.tm.cm.participationservice.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.kit.tm.cm.participationservice.domain.model.Comment;
import edu.kit.tm.cm.participationservice.domain.model.Vote;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.service.VoteService;
import edu.kit.tm.cm.participationservice.web.messages.request.VoteMessage;

public class VoteController extends AbstractController {

    @Autowired
    private VoteService voteService;

    @RequestMapping(
            value = "/comments/{commentID}/votes", method = RequestMethod.POST)
    @ResponseBody
    public Vote vote(
            @AuthenticationPrincipal Account account,
            @PathVariable("commentID") Comment comment,
            @Valid @RequestBody VoteMessage voteMessage) {

        return voteService.voteComment(account.getUser(), comment, voteMessage);
    }

}

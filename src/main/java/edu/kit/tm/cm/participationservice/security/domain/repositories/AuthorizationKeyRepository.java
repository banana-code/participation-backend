package edu.kit.tm.cm.participationservice.security.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.kit.tm.cm.participationservice.security.domain.model.AuthorizationKey;

public interface AuthorizationKeyRepository extends JpaRepository<AuthorizationKey, Long> {

    @Query("SELECT k "
            + "FROM Account acc JOIN acc.confirmationKey k "
            + "WHERE acc.email = :email")
    AuthorizationKey findRegistrationKeyByEmail(@Param("email") String email);
}

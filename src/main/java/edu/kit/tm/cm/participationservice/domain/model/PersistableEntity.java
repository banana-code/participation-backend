package edu.kit.tm.cm.participationservice.domain.model;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import edu.kit.tm.cm.participationservice.security.domain.model.Account;

/**
 * A Entity with an id.
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties({ "new" })
public abstract class PersistableEntity extends AbstractPersistable<Long> {

    private static final long serialVersionUID = 6647574605305174433L;

    /**
     * @return currently authenticated account or null if not authenticated.
     */
    protected Account getCurrentAccount() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null || !auth.isAuthenticated()) {
            return null;
        }
        Object principal = auth.getPrincipal();
        if (principal != null && principal instanceof Account) {
            return ((Account) principal);
        } else {
            return null;
        }
    }

    /**
     * @return currently authenticated user or null if not authenticated.
     */
    protected User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null || !auth.isAuthenticated()) {
            return null;
        }
        Object principal = auth.getPrincipal();
        if (principal instanceof Account) {
            return ((Account) principal).getUser();
        } else {
            return null;
        }
    }
}

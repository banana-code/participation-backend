package edu.kit.tm.cm.participationservice.security;

import java.util.EnumSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.kit.tm.cm.participationservice.domain.model.Comment;
import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.Issue.IssueState;
import edu.kit.tm.cm.participationservice.domain.model.Membership;
import edu.kit.tm.cm.participationservice.domain.model.Membership.MembershipState;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.web.controller.utility.checkers.GroupMemberChecker;
import edu.kit.tm.cm.participationservice.web.messages.request.InviteMessage;

/**
 * This class contains authorization logic, e.g. if a user is allowed to delete something.<br>
 * exceptions must not be caught, because an exceptions means automatically no permission.
 * <p>
 * methods must have the following format:
 * <p>
 * <code>public boolean userIsAllowedTo{MethodType}(User user, ...) { ... }</code>
 * <p>
 * MethodType depends on the used HTTP Method: <br>
 * <table>
 * <tr>
 * <td>GET</td>
 * <td>=> Get</td>
 * </tr>
 * <tr>
 * <td>POST</td>
 * <td>=> Create</td>
 * </tr>
 * <tr>
 * <td>DELETE</td>
 * <td>=> Delete</td>
 * </tr>
 * <tr>
 * <td>PATCH</td>
 * <td>=> Change</td>
 * </tr>
 * </table>
 */
@Component
public class AuthorizationHandler {

    private static final Set<IssueState> VOTE_STATES = EnumSet.of(IssueState.OPEN, IssueState.VOTE_ONLY);

    @Autowired
    private GroupMemberChecker groupMemberChecker;

    /**
     * @param user
     * @param group
     * @return true, is <code>user</code> is allowed to change the <code>group</code>.
     */
    public boolean userIsAllowedToChangeGroup(User user, GroupEntity group) {
        return groupMemberChecker.userIsOwnerOfGroup(user, group);
    }

    /**
     * @param user
     * @param group
     * @return true, is <code>user</code> is allowed to delete the <code>group</code>.
     */
    public boolean userIsAllowedToDeleteGroup(User user, GroupEntity group) {
        return groupMemberChecker.userIsOwnerOfGroup(user, group);
    }

    /**
     * @param user
     * @param group
     * @return true, if <code>user</code> is allowed to get issues of the <code>group</code>.
     */
    public boolean userIsAllowedToGetIssues(User user, GroupEntity group) {
        return groupMemberChecker.userIsInGroup(user, group);
    }

    /**
     * @param user
     * @param issue
     * @return true, is <code>user</code> is allowed to get the single <code>issue</code>.
     */
    public boolean userIsAllowedToGetSingleIssue(User user, Issue issue) {
        GroupEntity group = issue.getGroup();
        return groupMemberChecker.userIsInGroup(user, group)
                // TODO admin should not be allowed to get unpublished issues
                && (issue.isPublished() || issue.getCreatedBy().equals(user));
    }

    /**
     * @param user
     * @param group
     * @return true, if <code>user</code> is allowed to create a <code>issue</code> in group.
     */
    public boolean userIsAllowedToCreateIssue(User user, GroupEntity group) {
        return groupMemberChecker.userIsInGroup(user, group);
    }

    /**
     * @param user
     * @param issue
     * @return true, if <code>user</code> is allowed to change the <code>issue</code>.
     */
    public boolean userIsAllowedToChangeIssue(User user, Issue issue) {
        GroupEntity group = issue.getGroup();
        return groupMemberChecker.userIsInGroup(user, group)
                // moderator and issue is published
                && ((groupMemberChecker.userHasModerationRightsInGroup(user, group) && issue.isPublished())
                // creator and issue is unpublished
                || (issue.getCreatedBy().equals(user) && !issue.isPublished()));
    }

    /**
     * @param user
     *            the user who wants to delete an issue
     * @param issue
     *            the issue to delete
     * @return true, if <code>user</code> is allowed to delete the <code>issue</code>
     */
    public boolean userIsAllowedToDeleteIssue(User user, Issue issue) {
        GroupEntity group = issue.getGroup();

        if (!groupMemberChecker.userIsInGroup(user, group)) {
            return false;
        }
        return issue.getCreatedBy().equals(user) && !issue.isPublished()
                || groupMemberChecker.userHasModerationRightsInGroup(user, group) && issue.isPublished();
    }

    /**
     * @param user
     * @param issue
     * @return true, if <code>user</code> is allowed to get <code>issue.getSolutions()</code>.
     */
    public boolean userIsAllowedToGetSolutions(User user, Issue issue) {
        GroupEntity group = issue.getGroup();
        return groupMemberChecker.userIsInGroup(user, group);
    }

    /**
     * @param user
     *            the user who wants to create a solution
     * @param issue
     *            the issue, the solution belongs to
     * @return true, if <code>user</code> is allowed to create a solution for the given <code>issue</code>
     */
    public boolean userIsAllowedToCreateSolution(User user, Issue issue) {
        GroupEntity group = issue.getGroup();

        if (!groupMemberChecker.userIsInGroup(user, group)) {
            return false;
        }
        return IssueState.OPEN.equals(issue.getState())
                || (user.equals(issue.getCreatedBy()) && IssueState.UNPUBLISHED.equals(issue.getState()));
    }

    /**
     * @param user
     *            the user who wants to change a solution
     * @param solution
     *            the the solution to change
     * @return true, if <code>user</code> is allowed to change the solution.
     */
    public boolean userIsAllowedToChangeSolution(User user, Solution solution) {
        GroupEntity group = solution.getIssue().getGroup();
        return groupMemberChecker.userIsInGroup(user, group)
                // moderator and solution is published
                && ((groupMemberChecker.userHasModerationRightsInGroup(user, group) && solution.isPublished())
                // creator and solution is unpublished
                || (solution.getCreatedBy().equals(user) && !solution.isPublished()));
    }

    /**
     * @param user
     *            the user who wants to delete a solution
     * @param solution
     *            the solution to delete
     * @return true, if <code>user</code> is allowed to delete the <code>solution</code>
     */
    public boolean userIsAllowedToDeleteSolution(User user, Solution solution) {
        Issue issue = solution.getIssue();
        GroupEntity group = issue.getGroup();

        if (!groupMemberChecker.userIsInGroup(user, group)) {
            return false;
        }
        if (solution.isPublished()) {
            return groupMemberChecker.userHasModerationRightsInGroup(user, group);
        } else {
            return user.equals(solution.getCreatedBy());
        }
    }

    /**
     * @param user
     *            the user who wants to rate
     * @param solution
     *            the solution the user wants to rate
     * @return true, if <code>user</code> is allowed to rate the given <code>solution</code>
     */
    public boolean userIsAllowedToCreateRating(User user, Solution solution) {
        Issue issue = solution.getIssue();
        GroupEntity group = issue.getGroup();

        if (!groupMemberChecker.userIsInGroup(user, group)) {
            return false;
        }
        return VOTE_STATES.contains(issue.getState())
                || (!solution.isPublished() && solution.getCreatedBy().equals(user));
    }

    /**
     * @param user
     * @param group
     * @return true, if <code>user</code> is allowed to get memberships of the <code>group</code>.
     */
    public boolean userIsAllowedToGetMemberships(User user, GroupEntity group) {
        return groupMemberChecker.userHasModerationRightsInGroup(user, group)
                || groupMemberChecker.userIsInGroup(user, group);
    }

    /**
     * @param user
     * @param group
     * @param invMsg
     * @return true, if <code>user</code> is allowed to invite others to <code>group</code>.
     */
    public boolean userIsAllowedToCreateMembership(User user, GroupEntity group, InviteMessage invMsg) {
        // invite user to group
        return groupMemberChecker.userHasModerationRightsInGroup(user, group)
                // request membership for group
                || user.getId().equals(invMsg.getUserId());
    }

    /**
     * <code>user</code> wants to change <code>toChangeMembership</code>'s state to <code>newState</code>
     *
     * @param user
     *            the user who wants to change someone's membership
     * @param toChangeMembership
     *            the membership the user wants to change
     * @param newState
     *            the new membershipstate
     * @return true, if <code>user</code> is allowed, false otherwise
     */
    public boolean userIsAllowedToChangeMembership(User user, Membership toChangeMembership, MembershipState newState) {
        MembershipState currentState = toChangeMembership.getState();
        GroupEntity group = toChangeMembership.getGroup();

        if (user.equals(toChangeMembership.getUser())) {
            // user wants to decrease his role
            if (currentState.compareTo(newState) > 0) {
                return true;
            }
            // accept invitation
            if (MembershipState.INVITED.equals(currentState)
                    && MembershipState.MEMBER.equals(newState)) {
                return true;
            }
            // leave group and request invitation
            if (MembershipState.MEMBER.equals(currentState)
                    && MembershipState.REQUESTED.equals(newState)) {
                return true;
            }
        } else {
            // moderator/owner wants to change someone else's role
            if (groupMemberChecker.userHasModerationRightsInGroup(user, group)) {
                // accept request to group
                if (MembershipState.REQUESTED.equals(currentState) && MembershipState.MEMBER.equals(newState)) {
                    return true;
                }
                // remove member role
                if (MembershipState.MEMBER.equals(currentState) && currentState.compareTo(newState) > 0) {
                    return true;
                }
            }
            // owner wants to add/remove an moderator
            if (groupMemberChecker.userIsOwnerOfGroup(user, group)) {
                // remove moderator/owner role
                if ((MembershipState.MODERATOR.equals(currentState) || MembershipState.OWNER.equals(currentState))
                        && currentState.compareTo(newState) > 0) {
                    return true;
                }
                // add moderator/owner
                // changes membership from REQUESTED/MEMBER to MODERATOR/OWNER
                if ((MembershipState.REQUESTED.equals(currentState) || MembershipState.MEMBER.equals(currentState))
                        && (MembershipState.MODERATOR.equals(newState)) || MembershipState.OWNER.equals(newState)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param user
     *            the user who wants to delete a membership
     * @param membership
     *            the membership the user wants to delete
     * @return true, if <code>user</code> is allowed to remove the given <code>membership</code>
     */
    public boolean userIsAllowedToDeleteMembership(User user, Membership membership) {
        GroupEntity group = membership.getGroup();

        return user.equals(membership.getUser()) // user wants to quit group
                || groupMemberChecker.userIsOwnerOfGroup(user, group)
                || (groupMemberChecker.userIsModeratorOfGroup(user, group)
                && (membership.getState().equals(MembershipState.MEMBER) // moderator wants to kick a member
                || membership.getState().equals(MembershipState.INVITED))); // moderator removes
                                                                            // invitation
    }

    public boolean userIsAllowedToGetComment(User user, Comment comment) {
        while (comment.getParentComment() != null) {
            comment = comment.getParentComment();
        }

        switch (comment.getCommentBelonging()) {
        case ISSUE:
            return userIsAllowedToGetCommentsFromIssue(user, comment.getIssue());
        case SOLUTION:
            return userIsAllowedToGetCommentsFromSolution(user, comment.getSolution());
        default:
            return false;
        }
    }

    // needed because admin hast access to all gets but should not be allowed to create
    public boolean userIsAllowedToCreateResponse(User user, Comment comment) {
        return userIsAllowedToGetComment(user, comment);
    }

    public boolean userIsAllowedToCreateVoteComment(User user, Comment comment) {
        return userIsAllowedToGetComment(user, comment);
    }

    public boolean userIsAllowedToGetCommentsFromIssue(User user, Issue issue) {
        GroupEntity group = issue.getGroup();
        return groupMemberChecker.userIsInGroup(user, group)
                && (issue.getCreatedBy().equals(user) || issue.getStartDate().isBeforeNow());

    }

    // needed because admin hast access to all gets but should not be allowed to create
    public boolean userIsAllowedToCreateCommentAtIssue(User user, Issue issue) {
        return userIsAllowedToGetCommentsFromIssue(user, issue);
    }

    public boolean userIsAllowedToGetCommentsFromSolution(User user, Solution solution) {
        Issue issue = solution.getIssue();
        return userIsAllowedToGetCommentsFromIssue(user, issue);
    }

    // needed because admin hast access to all gets but should not be allowed to create
    public boolean userIsAllowedToCreateCommentAtSolution(User user, Solution solution) {
        return userIsAllowedToGetCommentsFromSolution(user, solution);
    }

    public boolean userIsAllowedToChangeComment(User user, Comment comment) {
        while (comment.getParentComment() != null) {
            comment = comment.getParentComment();
        }

        GroupEntity group;
        Issue issue;
        switch (comment.getCommentBelonging()) {
        case ISSUE:
            group = comment.getIssue().getGroup();
            issue = comment.getIssue();
            return groupMemberChecker.userHasModerationRightsInGroup(user, group)
                    && (issue.getCreatedBy().equals(user) || issue.getStartDate().isBeforeNow());
        case SOLUTION:
            group = comment.getSolution().getIssue().getGroup();
            issue = comment.getSolution().getIssue();
            return groupMemberChecker.userHasModerationRightsInGroup(user, group)
                    && (issue.getCreatedBy().equals(user) || issue.getStartDate().isBeforeNow());
        default:
            return false;
        }
    }

    /**
     * true, is user is allowed to access account data.
     *
     * @param user
     *            tge user who wants to access account data.
     * @return
     */
    public boolean userIsAllowedToGetAccounts(User user) {
        return false;
    }

    /**
     * true if a user is allowed to modify account.
     *
     * @param user
     *            the user who wants to modify account.
     * @param account
     *            the account to be modified.
     * @return
     */
    public boolean userIsAllowedToChangeAccounts(User user, Account account) {
        return false;
    }
}

package edu.kit.tm.cm.participationservice.web.controller.utility.exceptions;

/**
 * This exception is thrown if a controller wants to respond with a 404.<br>
 * It is mainly used by <code>ControllerParameterChecker</code>.
 */
public class NotFoundHttpException extends RuntimeException {

    private static final long serialVersionUID = -2422079115672284108L;

    // Parameterless Constructor
    public NotFoundHttpException() {}

    // Constructor that accepts a message
    public NotFoundHttpException(String message) {
        super(message);
    }

}

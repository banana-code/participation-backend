package edu.kit.tm.cm.participationservice.security.domain.model;

import java.util.Date;

import javax.persistence.Entity;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;

import edu.kit.tm.cm.participationservice.domain.model.PersistableEntity;

/**
 * This class describes a token used for remember-me authentication. <br>
 * Keep in mind that we use an email address as username.
 */
@Entity
public class RememberMeToken extends PersistableEntity {

    private static final long serialVersionUID = 4659247731588125201L;

    private String series;
    private String email;
    private String tokenValue;
    private Date date;

    protected RememberMeToken() {}

    public RememberMeToken(PersistentRememberMeToken token) {
        this.email = token.getUsername();
        this.series = token.getSeries();
        this.tokenValue = token.getTokenValue();
        this.date = token.getDate();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String username) {
        this.email = username;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getTokenValue() {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return a new copy of PersistentRememberMeToken depending on the previous called setter/constructor.
     */
    public PersistentRememberMeToken getPersistentRememberMeToken() {
        return new PersistentRememberMeToken(email, series, tokenValue, date);
    }
}

package edu.kit.tm.cm.participationservice.domain.model;

import org.joda.time.DateTime;

import edu.kit.tm.cm.participationservice.domain.model.Issue.IssueDates;

/**
 * This class is used to create issue objects.
 */
public final class IssueBuilder {

    private String title;
    private String question;
    private String description;
    private String currentSolution;
    private DateTime startDate;
    private DateTime solutionEndDate;
    private DateTime voteEndDate;
    private GroupEntity group;

    private IssueBuilder() {}

    public static IssueBuilder create() {
        return new IssueBuilder();
    }

    public IssueBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public IssueBuilder setQuestion(String question) {
        this.question = question;
        return this;
    }

    public IssueBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public IssueBuilder setCurrentSolution(String currentSolution) {
        this.currentSolution = currentSolution;
        return this;
    }

    public IssueBuilder setStartDate(DateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public IssueBuilder setSolutionEndDate(DateTime solutionEndDate) {
        this.solutionEndDate = solutionEndDate;
        return this;
    }

    public IssueBuilder setVoteEndDate(DateTime voteEndDate) {
        this.voteEndDate = voteEndDate;
        return this;
    }

    public IssueBuilder setGroup(GroupEntity group) {
        this.group = group;
        return this;
    }

    public Issue build() {
        return new Issue(title, question, description, currentSolution, new IssueDates(startDate, solutionEndDate,
                voteEndDate), group);
    }

}

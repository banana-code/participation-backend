package edu.kit.tm.cm.participationservice.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.web.validator.utility.ValidatorList;

@RequestMapping(GlobalConstants.URI_PREFIX)
public abstract class AbstractController {

    @Autowired
    private ValidatorList validatorList;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setValidator(validatorList);
        binder.setDisallowedFields("id");
    }
}

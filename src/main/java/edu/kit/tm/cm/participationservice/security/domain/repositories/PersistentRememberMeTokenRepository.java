package edu.kit.tm.cm.participationservice.security.domain.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Component;

import edu.kit.tm.cm.participationservice.security.domain.model.RememberMeToken;

/**
 * This is used by RememberMeAuthenticationFilter to save/get tokens.
 */
@Component
public class PersistentRememberMeTokenRepository implements PersistentTokenRepository {

    @Autowired
    private RememberMeTokenRepository tokenRepository;

    @Override
    public void createNewToken(PersistentRememberMeToken token) {
        // TODO cleaning strategy, logout is the only way to delete tokens
        RememberMeToken newToken = new RememberMeToken(token);
        tokenRepository.save(newToken);
    }

    /**
     * we never update a session so this implementation is empty.
     */
    @Override
    public void updateToken(String series, String tokenValue, Date lastUsed) {
        RememberMeToken rememberMeToken = tokenRepository.findBySeries(series);
        if (rememberMeToken != null) {
            // we don't save new tokenValue/we don't invalidiate the tokenValue
            rememberMeToken.setDate(lastUsed);
            tokenRepository.save(rememberMeToken);
        }
    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(String seriesId) {
        RememberMeToken token = tokenRepository.findBySeries(seriesId);
        return token == null ? null : token.getPersistentRememberMeToken();
    }

    @Override
    public void removeUserTokens(String email) {
        List<RememberMeToken> tokens = tokenRepository.findByEmail(email);
        if (tokens != null && !tokens.isEmpty()) {
            tokenRepository.delete(tokens);
        }
    }

}

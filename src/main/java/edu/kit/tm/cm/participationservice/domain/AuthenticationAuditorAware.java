package edu.kit.tm.cm.participationservice.domain;

import org.jboss.logging.Logger;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;

/**
 * This class is used by Spring Data for audition. <br>
 * The <code>getCurrentAuditor()</code> is called before persisting to set the <code>lastModifiedBy</code> and
 * <code>createdBy</code> attributes.
 *
 */
@Component
public class AuthenticationAuditorAware implements AuditorAware<User> {

    @Override
    public User getCurrentAuditor() {
        // TODO maybe we can use @CurrentUser or @AuthenticationPrincipal to simplify this
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null || !auth.isAuthenticated()) {
            // should not happen
            return null;
        }
        Object principal = auth.getPrincipal();
        if (principal instanceof Account) {
            return ((Account) principal).getUser();
        } else {
            Logger.getLogger(AuthenticationAuditorAware.class).debug("principal not instanceof Account");
            return null;
        }
    }
}

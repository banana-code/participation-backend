package edu.kit.tm.cm.participationservice.web.controller.utility.aspect.sqlexception;

import org.springframework.dao.DataIntegrityViolationException;

import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException;

public interface AbstractSqlExceptionParser {
    /**
     *
     * @param entity
     * @param e
     * @return null if not parsed.
     */
    SqlException parseException(Object entity, DataIntegrityViolationException e);
}

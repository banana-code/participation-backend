package edu.kit.tm.cm.participationservice;

import java.io.FileNotFoundException;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11Protocol;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.util.ResourceUtils;

import edu.kit.tm.cm.participationservice.demo.Bootstrap;

@Configuration
@EnableJpaAuditing
@EnableAutoConfiguration
@ComponentScan
@PropertySource("classpath:application.properties")
public class Application extends SpringBootServletInitializer {

    @Value("${sslKeyStorePath}")
    private String sslKeyStorePath;

    @Value("${sslKeyStoreAlias}")
    private String sslKeyStoreAlias;

    @Value("${sslKeyStorePassword}")
    private String sslKeyStorePassword;

    @Value("${enableEmbeddedTomcatSsl}")
    private Boolean enableEmbeddedTomcatSsl;

    @Value("${serverHttpsPort}")
    private int serverHttpsPort;

    @Value("${addTestData}")
    private Boolean addTestData;

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        Application thisApp = context.getBean(Application.class);
        if (thisApp.addTestData) {
            context.getBean(Bootstrap.class).setup();
        }
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    /**
     * This bean creates an additional Connector for https requests.
     *
     * @return
     * @throws FileNotFoundException
     */
    @Bean
    public EmbeddedServletContainerCustomizer httpsContainerCustomizer() throws FileNotFoundException {

        if (!enableEmbeddedTomcatSsl) {
            return new EmbeddedServletContainerCustomizer() {
                @Override
                public void customize(ConfigurableEmbeddedServletContainer container) {}
            };
        }
        // In case this server is deployed as .jar, this may result in a FileNotFoundException.
        // To load an external key store, set VM argument -DsslKeyStorePath=/absolute/path/to/keystore.jks
        final String keystoreFile = ResourceUtils.getFile(sslKeyStorePath).getAbsolutePath();

        return new EmbeddedServletContainerCustomizer() {
            @Override
            public void customize(ConfigurableEmbeddedServletContainer factory) {
                if (!(factory instanceof TomcatEmbeddedServletContainerFactory)) {
                    return;
                }

                TomcatEmbeddedServletContainerFactory tomcatFactory = (TomcatEmbeddedServletContainerFactory) factory;

                Connector httpsConnector = new Connector("HTTP/1.1");
                Http11Protocol protocol = (Http11Protocol) httpsConnector.getProtocolHandler();
                httpsConnector.setSecure(true);
                httpsConnector.setScheme("https");
                httpsConnector.setPort(serverHttpsPort);
                protocol.setSSLEnabled(true);
                protocol.setClientAuth("false");
                // TODO file is not found after maven package
                protocol.setKeystoreFile(keystoreFile);
                protocol.setKeystorePass(sslKeyStorePassword);
                protocol.setKeyAlias(sslKeyStoreAlias);

                tomcatFactory.addAdditionalTomcatConnectors(httpsConnector);
            }
        };
    }
}

package edu.kit.tm.cm.participationservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import edu.kit.tm.cm.participationservice.domain.model.Comment;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.model.Vote;
import edu.kit.tm.cm.participationservice.domain.repositories.VoteRepository;
import edu.kit.tm.cm.participationservice.web.messages.request.VoteMessage;

@Service
public class VoteService {

    @Autowired
    private VoteRepository voteRepository;

    @PreAuthorize("@authorizationHandler.userIsAllowedToCreateVoteComment(#executer, #comment)")
    public Vote voteComment(User executer, Comment comment, VoteMessage voteMessage) {
        Vote existingVote = voteRepository.findByCreatedByAndComment(executer, comment);
        if (existingVote != null) {
            existingVote.setType(voteMessage.getVoteType());
            return voteRepository.save(existingVote);
        } else {
            return voteRepository.save(new Vote(comment, voteMessage.getVoteType()));
        }

    }
}

package edu.kit.tm.cm.participationservice.domain.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.domain.model.Issue.IssueState;

/**
 * A solution is a description to solve an issue.<br>
 * Solutions are rated and commented by users to differentiate good solutions from bad ones.
 */
@Entity
public class Solution extends AuditableEntity implements Comparable<Solution>, Publishable {

    private static final long serialVersionUID = -7080925692005268891L;

    @Column(nullable = false)
    private String title;

    @Column(length = GlobalConstants.MAX_SOLUTION_DESCRIPTION_LENGTH)
    private String description;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Issue issue;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "solution", fetch = FetchType.EAGER)
    private Set<Resistance> resistances = new HashSet<Resistance>();

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "solution", fetch = FetchType.EAGER)
    private Set<Comment> comments = new HashSet<Comment>();

    protected Solution() {}

    public Solution(String title, String desc, Issue issue) {
        setTitle(title);
        this.description = desc;
        Assert.notNull(issue, "The issue can not be NULL");
        this.issue = issue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String desc) {
        description = desc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonIgnore
    public Issue getIssue() {
        return issue;
    }

    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    @JsonIgnore
    public Set<Resistance> getResistances() {
        return resistances;
    }

    @JsonIgnore
    public Set<Comment> getComments() {
        return comments;
    }

    /**
     * @return the currently authenticated user's resistance or null if the user didn't rate this solution.
     */
    public Integer getUserResistance() {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return null;
        }
        for (Resistance resistance : resistances) {
            if (resistance.getCreatedBy().equals(currentUser)) {
                return resistance.getRating();
            }
        }
        return null;
    }

    /**
     * @return average resistance of all users who rated this solution.
     */
    public float getResistance() {
        if (resistances.size() == 0) {
            return 0;
        }

        float sum = 0.f;
        for (Resistance resistance : resistances) {
            sum += resistance.getRating();
        }
        return sum / resistances.size();
    }

    public int getVoteCount() {
        return resistances.size();
    }

    /**
     * a solution is published if the corresponding issue is not unpublished.<br>
     * aquivalent to <code>!getIssue().getState().equals(IssueState.UNPUBLISHED)</code>
     *
     * @return true if the solution can be seen by other group members.
     */
    @Override
    public boolean isPublished() {
        return issue.getState() != IssueState.UNPUBLISHED;
    }

    /**
     * This method is used to order solutions collections.
     */
    @Override
    public int compareTo(Solution o) {
        if (getResistance() > o.getResistance()) {
            return -1;
        } else if (getResistance() < o.getResistance()) {
            return 1;
        } else {
            return 0;
        }
    }
}

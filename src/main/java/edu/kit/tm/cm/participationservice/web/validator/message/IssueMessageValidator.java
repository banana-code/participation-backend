package edu.kit.tm.cm.participationservice.web.validator.message;

import org.joda.time.DateTime;
import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.web.messages.request.IssueMessage;
import edu.kit.tm.cm.participationservice.web.validator.utility.AbstractValidator;
import edu.kit.tm.cm.participationservice.web.validator.utility.ErrorsHelper;

public class IssueMessageValidator extends AbstractValidator<IssueMessage> {

    protected IssueMessageValidator() {
        super(IssueMessage.class);
    }

    @Override
    public void validateInternal(IssueMessage msg, ErrorsHelper errorsHelper) {

        DateTime startDate = msg.getStartDateTime();

        // voteend must be after solutionend
        if (msg.getSolutionEndDate() != null && msg.getVoteEndDate() != null
                && msg.getSolutionEndDate().isAfter(msg.getVoteEndDate())) {
            errorsHelper.reject("voteEndDate", GlobalConstants.EM_ISSUE_DEADLINE_ORDER);
        }

        // start and enddates must be set if published
        if (startDate != null && msg.getSolutionEndDate() == null) {
            errorsHelper.reject("solutionEndDate", GlobalConstants.EM_ISSUE_SOLUTION_DATE_INVALID);
        }
        if (startDate != null && msg.getVoteEndDate() == null) {
            errorsHelper.reject("voteEndDate", GlobalConstants.EM_ISSUE_VOTE_DATE_INVALID);
        }

        // enddates must be in the future
        if (msg.getSolutionEndDate() != null && msg.getSolutionEndDate().isBeforeNow()) {
            errorsHelper.reject("solutionEndDate", GlobalConstants.EM_ISSUE_SOLUTION_DATE_PAST);
        }
        if (msg.getVoteEndDate() != null && msg.getVoteEndDate().isBeforeNow()) {
            errorsHelper.reject("voteEndDate", GlobalConstants.EM_ISSUE_VOTE_DATE_PAST);
        }
    }
}

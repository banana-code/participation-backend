package edu.kit.tm.cm.participationservice.domain.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.User;

public interface GroupRepository extends JpaRepository<GroupEntity, Long> {

    GroupEntity findByTitle(String title);

    @Query("SELECT m.group "
            + "FROM Membership m "
            + "WHERE m.user = :user "
            + "AND (m.state = 'MEMBER' OR m.state = 'OWNER' OR m.state = 'MODERATOR')")
    List<GroupEntity> findMyGroups(@Param("user") User user);
}

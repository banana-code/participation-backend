package edu.kit.tm.cm.participationservice;

import org.reflections.Reflections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanDefinitions {

    @Bean
    public Reflections reflections() {
        return new Reflections(GlobalConstants.BASE_PACKAGE);
    }
}

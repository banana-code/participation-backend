package edu.kit.tm.cm.participationservice.web.messages.request;

import javax.validation.constraints.NotNull;

public class PasswordResetConfirmationMessage {

    @NotNull
    private String key;

    @NotNull
    private String newPassword;

    protected PasswordResetConfirmationMessage() {}

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}

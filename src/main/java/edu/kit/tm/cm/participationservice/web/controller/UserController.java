package edu.kit.tm.cm.participationservice.web.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.security.domain.repositories.AccountRepository;
import edu.kit.tm.cm.participationservice.web.validator.utility.ErrorsHelper;

public class UserController extends AbstractController {

    @Autowired
    private AccountRepository accountRepository;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @ResponseBody
    public Set<User> searchUserByAliasPrefix(
            @RequestParam(value = "aliasPrefix", required = false) String searchMessage) {
        if (searchMessage == null) {
            new ErrorsHelper(searchMessage, "aliasPrefix").reject(GlobalConstants.EM_SEARCH_INVALID_ALIAS_PREFIX)
                    .throwErrors();
        }
        return accountRepository.findUsersByUserAliasPrefix(searchMessage);
    }
}

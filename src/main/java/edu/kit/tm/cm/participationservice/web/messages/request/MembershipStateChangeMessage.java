package edu.kit.tm.cm.participationservice.web.messages.request;

public class MembershipStateChangeMessage {

    private String membershipState;

    protected MembershipStateChangeMessage() {}

    public MembershipStateChangeMessage(String membershipState) {
        this.membershipState = membershipState;
    }

    public String getMembershipState() {
        return membershipState;
    }

}

package edu.kit.tm.cm.participationservice.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import edu.kit.tm.cm.participationservice.security.domain.model.Account;

@Component
public class UnconfirmedLoginChecker extends AccountStatusUserDetailsChecker {

    private static final String UNCONFIRMED_ACCOUNT = "Account is not confirmed";

    @Value("${accountRegistrationRequiresConfirmation}")
    private Boolean accountRegistrationRequiresConfirmation;

    @Override
    public void check(UserDetails toCheck) {
        super.check(toCheck);
        if (!(toCheck instanceof Account)) {
            return;
        }
        Account userDetails = (Account) toCheck;
        if (accountRegistrationRequiresConfirmation && userDetails.getConfirmationKey() != null) {
            throw new UnconfirmedLoginException(UNCONFIRMED_ACCOUNT);
        }
    }
}

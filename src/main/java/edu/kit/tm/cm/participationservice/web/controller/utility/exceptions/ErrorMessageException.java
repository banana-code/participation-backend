package edu.kit.tm.cm.participationservice.web.controller.utility.exceptions;

import org.springframework.validation.Errors;

import edu.kit.tm.cm.participationservice.web.messages.response.ErrorMessage;
import edu.kit.tm.cm.participationservice.web.validator.utility.ErrorsHelper;

/**
 * This exception is thrown if a controller wants to respond with a ErrorList.
 */
public class ErrorMessageException extends RuntimeException {

    private static final long serialVersionUID = 8705718781479312333L;

    private final ErrorMessage errors;

    public ErrorMessageException(ErrorMessage errors) {
        this.errors = errors;
    }

    public ErrorMessageException(Errors errors) {
        this(new ErrorMessage(errors));
    }

    public ErrorMessageException(ErrorsHelper errors) {
        this(errors.getErrors());
    }

    public ErrorMessage getErrors() {
        return errors;
    }
}

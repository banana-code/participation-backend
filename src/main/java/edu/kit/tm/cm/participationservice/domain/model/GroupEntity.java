package edu.kit.tm.cm.participationservice.domain.model;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.domain.model.Membership.MembershipState;

/**
 * A Group is a number of users who use this system to create issues and solutions.
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(
        columnNames = { "title" },
        name = "unique__group_entity__title") )
public class GroupEntity extends AuditableEntity {

    private static final long serialVersionUID = 1L;
    private static final Set<MembershipState> MEMBER_STATES = EnumSet.of(MembershipState.MEMBER,
            MembershipState.MODERATOR, MembershipState.OWNER);

    @Column(nullable = false)
    private String title;

    @Column(length = GlobalConstants.MAX_GROUP_DESCRIPTION_LENGTH)
    private String description;

    // TODO tests will throw LazyInitializationException with lazy fetchtype
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "group", fetch = FetchType.EAGER)
    private Set<Issue> issues = new HashSet<Issue>();

    // TODO tests will throw LazyInitializationException with lazy fetchtype
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "group", fetch = FetchType.EAGER)
    private Set<Membership> members = new HashSet<Membership>();

    protected GroupEntity() {}

    public GroupEntity(String title, String description) {
        setTitle(title);
        setDescription(description);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonIgnore
    public Set<Issue> getIssues() {
        return issues;
    }

    @JsonIgnore
    public Set<Membership> getMembers() {
        return members;
    }

    public int getIssueCount() {
        return issues.size();
    }

    /**
     * A user is member if he is member or he has more permissions, e.g. moderation/owner rights.
     *
     * @return the number of members.
     */
    public int getMemberCount() {
        int count = 0;
        for (Membership member : members) {
            if (MEMBER_STATES.contains(member.getState())) {
                count++;
            }
        }
        return count;
    }

    /**
     * @return MembershipState relation between this group and the currently authenticated user.
     */
    public MembershipState getUserState() {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return null;
        }
        for (Membership member : members) {
            if (member.getUser().equals(currentUser)) {
                return member.getState();
            }
        }
        return null;
    }

    /**
     * @return id of the membership between this group and the currently authenticated user, <br>
     *         -1 if the user is not authenticated or not a member of this group.
     */
    public long getUserMembershipId() {
        User currentUser = getCurrentUser();
        if (currentUser == null) {
            return -1;
        }
        for (Membership member : members) {
            if (member.getUser().equals(currentUser)) {
                return member.getId();
            }
        }
        return -1;
    }
}

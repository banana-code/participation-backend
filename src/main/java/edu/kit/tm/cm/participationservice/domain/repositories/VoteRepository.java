package edu.kit.tm.cm.participationservice.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.kit.tm.cm.participationservice.domain.model.Comment;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.model.Vote;

public interface VoteRepository extends JpaRepository<Vote, Long> {

    Vote findByCreatedByAndComment(User user, Comment comment);

}

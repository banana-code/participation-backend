package edu.kit.tm.cm.participationservice.security.filter;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Custom UsernamePasswordAuthenticationFilter which uses the request's body as JSON to read email and password.
 */
public class CustomAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        if (!request.getMethod().equals(HttpMethod.POST.name())) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }

        // try default method
        String email = obtainUsername(request);
        String password = obtainPassword(request);

        // otherwise read body as json
        if (email == null || password == null) {
            ObjectMapper mapper = new ObjectMapper();
            LoginData account;
            try {
                account = mapper.readValue(request.getReader(), LoginData.class);
            } catch (IOException e) {
                throw new AuthenticationServiceException(
                        "could not read JSON body, it must contain an 'email' and 'password' field");
            }
            email = email == null ? account.email : email;
            password = password == null ? account.password : password;
        }

        if (email == null) {
            email = "";
        }

        if (password == null) {
            password = "";
        }

        email = email.trim();

        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(email, password);

        setDetails(request, authRequest);

        return this.getAuthenticationManager().authenticate(authRequest);
    }

    public static class LoginData {
        private String email, password;

        protected LoginData() {}

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}

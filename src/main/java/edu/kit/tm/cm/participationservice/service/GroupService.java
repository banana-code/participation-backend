package edu.kit.tm.cm.participationservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Membership;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.repositories.GroupRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.MembershipRepository;
import edu.kit.tm.cm.participationservice.web.messages.request.GroupChangeMessage;
import edu.kit.tm.cm.participationservice.web.messages.request.GroupCreationMessage;

@Service
public class GroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private MembershipRepository membershipRepository;

    /**
     * executer creates a group.<br>
     * he becomes owner automatically
     *
     * @param executer
     * @param groupCreationMessage
     * @return
     */
    public GroupEntity createGroup(User executer, GroupCreationMessage groupCreationMessage) {
        GroupEntity group = new GroupEntity(groupCreationMessage.getTitle(), groupCreationMessage.getDescription());
        group = groupRepository.save(group);
        Membership membership = new Membership(group, Membership.MembershipState.OWNER, executer);
        membershipRepository.save(membership);
        return group;
    }

    /**
     * executer modifies group depending on groupChangeMessage.
     *
     * @param executer
     * @param group
     * @param groupChangeMessage
     * @return
     */
    @PreAuthorize("@authorizationHandler.userIsAllowedToChangeGroup(#executer, #group)")
    public GroupEntity changeGroup(User executer, GroupEntity group, GroupChangeMessage groupChangeMessage) {
        group.setTitle(groupChangeMessage.getTitle());
        group.setDescription(groupChangeMessage.getDescription());
        return groupRepository.save(group);
    }

    @PreAuthorize("@authorizationHandler.userIsAllowedToDeleteGroup(#executer, #group)")
    public void deleteGroup(User executer, GroupEntity group) {
        groupRepository.delete(group);
    }
}

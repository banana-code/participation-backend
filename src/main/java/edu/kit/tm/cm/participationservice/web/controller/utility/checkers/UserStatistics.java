package edu.kit.tm.cm.participationservice.web.controller.utility.checkers;

import java.util.Set;

import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.Resistance;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.domain.model.User;

public class UserStatistics {

    protected UserStatistics() {}

    /**
     * a user is participating in an issue if he/she:<br>
     *
     * <ul>
     * <li>created this issue</li>
     * <li>created a solution</li>
     * <li>rated a solution</li>
     * </ul>
     *
     * @param user
     * @param issue
     * @return
     */
    public static boolean userIsParticipatingInIssue(User user, Issue issue) {
        if (user == null || issue == null) {
            return false;
        }
        if (issue.getCreatedBy().equals(user)) {
            // user is owner of the issue
            return true;
        }
        Set<Solution> solutions = issue.getSolutions();
        for (Solution solution : solutions) {
            if (solution.getCreatedBy().equals(user)) {
                // user created a solution
                return true;
            }
            Set<Resistance> resistances = solution.getResistances();
            for (Resistance resistance : resistances) {
                if (resistance.getCreatedBy().equals(user)) {
                    // user rated a solution
                    return true;
                }
            }
        }
        return false;
    }
}

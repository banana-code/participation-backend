package edu.kit.tm.cm.participationservice.security.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ForceHttpsFilter implements Filter {

    private static final int DEFAULT_HTTPS_PORT = 443;

    private boolean enabled;
    private int serverHttpsPort;

    public ForceHttpsFilter(boolean enable, int httpsPort) {
        this.enabled = enable;
        this.serverHttpsPort = httpsPort;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        if (!enabled) {
            // if disabled go to the next filter
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            HttpServletResponse response = (HttpServletResponse) servletResponse;

            if (!request.isSecure()) {
                String target = "https://" + request.getServerName();

                if (servletRequest.getServerPort() != serverHttpsPort && serverHttpsPort != DEFAULT_HTTPS_PORT) {
                    target += ":" + serverHttpsPort;
                }

                String path = request.getServletPath();
                if (path != null) {
                    target += path;
                }

                response.sendRedirect(target);
                // in case of a redirect, don't proceed with the FilterChain!
            } else {
                filterChain.doFilter(request, response);
            }
        }
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {}

    @Override
    public void destroy() {}

}

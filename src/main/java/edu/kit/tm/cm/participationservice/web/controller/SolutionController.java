package edu.kit.tm.cm.participationservice.web.controller;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.service.SolutionService;
import edu.kit.tm.cm.participationservice.web.messages.request.SolutionMessage;

public class SolutionController extends AbstractController {

    private static final String PREFIX = "/groups/{groupID}/issues/{issueID}/solutions";

    @Autowired
    private SolutionService solutionService;

    @RequestMapping(value = PREFIX, method = RequestMethod.GET)
    @ResponseBody
    public Set<Solution> getSolutionsFromIssue(
            @AuthenticationPrincipal Account account,
            @PathVariable("issueID") Issue issue,
            @PathVariable("groupID") GroupEntity group) {

        return solutionService.getVisibleSolutions(account.getUser(), issue);
    }

    @RequestMapping(value = PREFIX, method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Solution createSolution(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group,
            @PathVariable("issueID") Issue issue,
            @Valid @RequestBody SolutionMessage solutionMessage) {

        return solutionService.createSolution(account.getUser(), issue, solutionMessage);
    }

    @RequestMapping(value = PREFIX + "/{solutionID}", method = RequestMethod.PATCH)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Solution changeSolution(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group,
            @PathVariable("issueID") Issue issue,
            @PathVariable("solutionID") Solution currentSolution,
            @Valid @RequestBody SolutionMessage changeSolution) {

        return solutionService.changeSolution(account.getUser(), currentSolution, changeSolution);
    }

    @RequestMapping(value = PREFIX + "/{solutionID}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteSolution(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group,
            @PathVariable("issueID") Issue issue,
            @PathVariable("solutionID") Solution solution) {

        solutionService.deleteSolution(account.getUser(), solution);
    }
}

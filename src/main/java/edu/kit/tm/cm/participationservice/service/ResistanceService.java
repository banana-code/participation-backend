package edu.kit.tm.cm.participationservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import edu.kit.tm.cm.participationservice.domain.model.Resistance;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.repositories.ResistanceRepository;
import edu.kit.tm.cm.participationservice.web.messages.request.RatingMessage;

@Service
public class ResistanceService {

    @Autowired
    private ResistanceRepository resistanceRepository;

    /**
     * executer rates solution.
     *
     * @param executer
     * @param solution
     * @param ratingMessage
     */
    @PreAuthorize("@authorizationHandler.userIsAllowedToCreateRating(#executer, #solution)")
    public void createRating(User executer, Solution solution, RatingMessage ratingMessage) {
        Resistance resistance = resistanceRepository.findByCreatedByAndSolution(executer, solution);

        if (resistance != null) {
            resistance.setRating(ratingMessage.getRating());
            resistanceRepository.save(resistance);
        } else {
            resistance = new Resistance(solution, ratingMessage.getRating());
            resistanceRepository.save(resistance);
        }
    }
}

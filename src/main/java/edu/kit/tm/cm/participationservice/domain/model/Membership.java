package edu.kit.tm.cm.participationservice.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * This class contains information about the relation between a group and a user.<br>
 * If a user leaves a group the membership object is deleted.
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(
        columnNames = { "group_id", "user_id" },
        name = "unique__membership__group_id__user_id"))
public class Membership extends AuditableEntity {

    private static final long serialVersionUID = 962166423147743296L;

    /**
     * Different possible states between the User-Group-relation.<br>
     */
    public enum MembershipState {
        REQUESTED, INVITED, MEMBER, MODERATOR, OWNER;
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    private GroupEntity group;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private MembershipState state;

    protected Membership() {}

    public Membership(GroupEntity group, MembershipState state, User user) {
        setGroup(group);
        setState(state);
        setUser(user);
    }

    @JsonIgnore
    public GroupEntity getGroup() {
        return group;
    }

    public void setGroup(GroupEntity group) {
        this.group = group;
    }

    public MembershipState getState() {
        return state;
    }

    public void setState(MembershipState state) {
        this.state = state;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * automatically generated
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((group == null) ? 0 : group.hashCode());
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        return result;
    }

    /**
     * this reflects the unique constraint.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Membership)) {
            return false;
        }
        Membership param = (Membership) obj;
        return param.getCreatedBy().equals(this.getCreatedBy())
                && param.getGroup().equals(this.getGroup());
    }
}

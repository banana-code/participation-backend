package edu.kit.tm.cm.participationservice.demo;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import edu.kit.tm.cm.participationservice.domain.model.Comment;
import edu.kit.tm.cm.participationservice.domain.model.Comment.SolutionCommentType;
import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.IssueBuilder;
import edu.kit.tm.cm.participationservice.domain.model.Membership;
import edu.kit.tm.cm.participationservice.domain.model.Membership.MembershipState;
import edu.kit.tm.cm.participationservice.domain.model.Resistance;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.model.Vote;
import edu.kit.tm.cm.participationservice.domain.model.Vote.VoteType;
import edu.kit.tm.cm.participationservice.domain.repositories.CommentRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.GroupRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.IssueRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.MembershipRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.ResistanceRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.SolutionRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.UserRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.VoteRepository;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.security.domain.model.Account.AccountState;
import edu.kit.tm.cm.participationservice.security.domain.repositories.AccountRepository;

/**
 * this class is used to add test data to the database.
 */
@Component
public class Bootstrap {

    private static final String ADMIN_MAIL = "ad.min@example.com";

    private static final String LOCKED_MAIL = "lock.ed@example.com";

    private static final String[] USER_EMAILS = {
            LOCKED_MAIL,
            ADMIN_MAIL,
            "daniel.dusentrieb@example.com",
            "felix.gs2@example.com",
            "as.df@example.com",
            "leonard.davinci@example.com",
            "max.min@example.com",
            "maximilian.maximus@example.com",
            "qwer.q@qwer.de",
            "asdf.a@asdf.de",
            "pascal.newton@example.com",
            "rap.hael@example.com",
            "roland.ronald@example.com",
            "BootstrapRoutine.xxx@example.com",
    };

    private static final int N_USER_MEMBERS = 8;
    private static final MembershipState[] MEMBERSHIPSTATE = {
            MembershipState.INVITED,
            MembershipState.REQUESTED,
            MembershipState.MEMBER,
            MembershipState.MODERATOR,
            MembershipState.OWNER,
            MembershipState.MEMBER,
            MembershipState.MODERATOR,
            MembershipState.OWNER
    };
    private static final long[] RATING_DEMO_USERS = { 1L, 1L, 2L, 2L, 2L, 3L, 4L };
    private static final long[] RATING_DEMO_SOLUTIONS = { 1L, 2L, 3L, 1L, 2L, 1L, 1L };
    private static final int[] RATING_DEMO_RATINGS = { 1, 2, 3, 4, 5, 6, 7 };
    private static final long[] ISSUE_IDS = { 1L, 1L, 2L, 2L, 3L, 3L, 4L, 4L };

    @Autowired
    private MembershipRepository membershipRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SolutionRepository solutionRepository;

    @Autowired
    private IssueRepository issueRepository;

    @Autowired
    private ResistanceRepository resistanceRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    public void setup() {
        setupUsers();

        setupGroups();

        setupMemberships();

        setCurrentUser(USER_EMAILS[USER_EMAILS.length - 1]);

        setupIssues();

        setupSolutions();

        setupResistances();

        setupComments();

        setupVotes();
    }

    private void setupGroups() {
        setCurrentUser(USER_EMAILS[USER_EMAILS.length - 1]);
        // may be an admin

        String[] titles = {
                "Alle Studenten",
                "Infos",
                "InWis" };
        String[] descriptions = {
                "Die Gruppe in der alle Studenten sind.",
                "Alle Info Studenten.",
                "Alle InWi Studenten." };
        int n = titles.length;

        List<GroupEntity> groups = new ArrayList<GroupEntity>(n);
        for (int i = 0; i < n; i++) {
            groups.add(new GroupEntity(titles[i], descriptions[i]));
        }
        groupRepository.save(groups);
    }

    private void setupMemberships() {

        List<GroupEntity> groups = groupRepository.findAll();

        for (int j = 0; j < N_USER_MEMBERS; j++) {
            setCurrentUser(USER_EMAILS[USER_EMAILS.length - j - 1]);
            User currentUser = userRepository.findByAlias(USER_EMAILS[USER_EMAILS.length - j - 1].split("\\.")[0]);
            for (int i = 0; i < groups.size(); i++) {
                membershipRepository.save(new Membership(groups.get(i), MEMBERSHIPSTATE[j], currentUser));
            }
        }
    }

    private void setupUsers() {
        for (String email : USER_EMAILS) {
            String firstName = email.split("\\.")[0];
            User user = new User(firstName);
            Account details = new Account(user, email, firstName);
            // Take care of admin
            if (email.equals(ADMIN_MAIL)) {
                details.setState(AccountState.ADMIN);
            }
            // Take care of blocked user
            if (email.equals(LOCKED_MAIL)) {
                details.setState(AccountState.LOCKED);
            }

            accountRepository.save(details);
        }
    }

    private void setupIssues() {
        String[] titles = {
                "Kaffe am AKK",
                "IT Studium verbessern",
                "Reise zum Mond",
                "Informatik-Bachelor in 6 Semestern" };
        String[] descriptions = {
                "best description EUW",
                "das Ergebnis geht zur ASTA",
                "Hier soll es darum gehen, wie wir zum Mond kommen!",
                "Hier geht es darum eine Strategie zu finden, um den Informatik-Bachelor in 6 Semestern zu meistern." };
        String[] curSolutions = {
                "Ettli",
                "keine Änderung",
                "Wir bleiben hier, wie wir es immer getan haben...",
                "Man kann es nicht schaffen" };
        String[] questions = {
                null,
                "Wie können wir unser Studium verbessern?",
                "Wie kommen wir zum Mond?",
                "Wie schaffen wir das?" };
        String[] startDates = { "2014-03-11", "2018-06-11", "2014-03-11", "2014-03-11" };
        String[] solutionEndDates = { "2014-04-11", "2019-12-11", "2014-04-13", "2020-04-05" };
        String[] voteEndDates = { "2014-04-12", "2020-04-11", "2015-05-13", "2020-04-09" };
        long[] groupIds = { 1L, 1L, 1L, 2L };
        int n = titles.length;

        List<Issue> issues = new ArrayList<Issue>(n);
        for (int i = 0; i < n; i++) {
            issues.add(IssueBuilder.create()
                    .setDescription(descriptions[i])
                    .setTitle(titles[i])
                    .setCurrentSolution(curSolutions[i])
                    .setQuestion(questions[i])
                    .setStartDate(DateTime.parse(startDates[i]))
                    .setSolutionEndDate(
                            DateTime.parse(solutionEndDates[i]))
                    .setVoteEndDate(DateTime.parse(voteEndDates[i]))
                    .setGroup(groupRepository.findOne(groupIds[i]))
                    .build());
        }
        issueRepository.save(issues);
    }

    private void setupSolutions() {
        String[] titles = {
                "Kaffe am AKK",
                "IT Studium verbessern",
                "Trollen",
                "Testen",
                "Wir klauen ein SpaceShuttle",
                "Katapult bauen",
                "Gebt auf, es geht nicht",
                "Urlaubssemester beantragen und lernen" };
        String[] descriptions = {
                "Es soll eine neue Kaffeesorte am AKK ausgesucht werden die günstig, gut und einzigartig sein soll. "
                        + "Außerdem sollte die Beschreibung der Kaffesorte nicht zu groß sein, "
                        + "da sonst die TextLänge die Beschreibung unnötig befüllt. "
                        + "Falls allerdings der Name der Kaffesorte zu kurz ist "
                        + "könnte man ihn übersehen und niemand würde merken das das AKK eine neue Kaffesorte hat."
                        + "Vor allem aber sollte man sich bei einer Beschreibung "
                        + "nicht zu stark auf eine unnötige Sache konzentrieren",
                "Das Ergebnis geht zur ASTA. Die Ergebnisse sind aus dem Workshop zu holen, "
                        + "außerdem sollen weitere Lösungen durch die Benutzer des Systems erarbeitet werden",
                "Lorem ipsum non tincidunt justo magna, ante felis ut fusce posuere, elementum ad tincidunt "
                        + "faucibus platea erat blandit massa velit ac eros volutpat pulvinar, "
                        + "volutpat dictum ut egestas sit risus."
                        + "Potenti quis viverra rhoncus ac elit conubia malesuada nunc at a, "
                        + "nibh etiam vitae felis ut curabitur aliquam etiam litora pulvinar, "
                        + "a dictumst dolor cursus curae consectetur pulvinar per risus."
                        + "Augue diam taciti laoreet eleifend quam magna, "
                        + "risus dictum et class ornare pretium scelerisque, "
                        + "sagittis cursus praesent ut augue nec nulla erat ullamcorper "
                        + "ut etiam morbi mauris ac viverra."
                        + "Porta nec aenean sagittis taciti tristique aliquet neque curabitur, "
                        + "tempus commodo vitae mattis velit varius nunc, "
                        + "iaculis integer enim euismod ante curabitur vitae."
                        + "Curabitur tellus quisque bibendum convallis potenti senectus hendrerit lorem malesuada nec, "
                        + "duis placerat tincidunt dapibus libero cubilia ipsum nostra condimentum, "
                        + "malesuada vitae donec etiam netus facilisis fringilla curabitur eleifend.",
                "Testen was das zeug hält. Aber keine Zufallstest, "
                        + "da diese dafür sorgen könnten dass die Tests manchmal laufen und manchmal nicht",
                "Wir klauen ein Spaceshuttle bei der NASA und fliegen damit zum Mond... "
                        + "Mein Onkel hat einen Pilotenschein.",
                "Um den Mond zu erreichen benötigen wir nur ein Katapult, "
                        + "das genügend Impuls aufbringen kann. Hierzu habe"
                        + " ich bereits einige Berechnungen angestellt.",
                "Macht eucht doch nicht die Mühe Jungs, nichtmal die Ratefüchse schaffen das.",
                "Mit genügend Urlaubssemstern sollte es kein Problem sein, den Bachelor in 6 Semestern zu schaffen."
                        + " Einfach Prüfungen in einem Semester schreiben und vor diesem Semester "
                        + "3 Urlaubssemster zum Lernen einlegen." };
        Issue[] issues = new Issue[ISSUE_IDS.length];
        for (int i = 0; i < ISSUE_IDS.length; i++) {
            issues[i] = issueRepository.findOne(ISSUE_IDS[i]);
        }
        int n = titles.length;

        List<Solution> solutions = new ArrayList<Solution>(n);
        for (int i = 0; i < n; i++) {
            solutions.add(new Solution(titles[i], descriptions[i], issues[i]));
        }
        solutionRepository.save(solutions);
    }

    private void setupResistances() {
        for (int i = 0; i < RATING_DEMO_USERS.length; i++) {
            setCurrentUser(accountRepository.findOne(RATING_DEMO_USERS[i]).getEmail());
            Solution solution = solutionRepository.findOne(RATING_DEMO_SOLUTIONS[i]);
            resistanceRepository.save(new Resistance(solution, RATING_DEMO_RATINGS[i]));
        }
    }

    private void setupComments() {
        String issueComment = "Das ist ein Issue Kommentar";
        String[] solutionComments = {
                "Das ist ein Informations Kommentar einer Solution",
                "Das ist ein Pro Kommentar einer Solution",
                "Das ist ein Contra Kommentar einer Solution" };
        String responseComment = "Das ist ein Kommentar eines Kommentares";

        int counter = 1;

        for (int j = 0; j < N_USER_MEMBERS; j++) {
            setCurrentUser(USER_EMAILS[USER_EMAILS.length - j - 1]);

            List<Issue> issues = issueRepository.findAll();
            for (Issue issue : issues) {
                Comment comment = new Comment(issue, issueComment + counter);
                commentRepository.save(comment);
                counter++;
            }

            List<Solution> solutions = solutionRepository.findAll();
            for (Solution solution : solutions) {
                final int modulo = 3;

                SolutionCommentType solutionCommentType = SolutionCommentType.values()[j % modulo];
                String content = solutionComments[j % modulo];
                Comment comment = new Comment(solution, solutionCommentType, content + counter);
                commentRepository.save(comment);
                counter++;
            }

            List<Comment> comments = commentRepository.findAll();
            int commentCounter = j + 1;
            for (; commentCounter < comments.size(); commentCounter += (j + 1)) {
                // a bit random to generate a wide distribution

                Comment response = new Comment(comments.get(commentCounter), responseComment + counter);
                commentRepository.save(response);
                counter++;
            }

        }

    }

    private void setupVotes() {

        for (int j = 0; j < N_USER_MEMBERS; j++) {
            setCurrentUser(USER_EMAILS[USER_EMAILS.length - j - 1]);

            List<Comment> comments = commentRepository.findAll();

            int voteCounter = j + 2;
            for (; voteCounter < comments.size(); voteCounter += (j + 2)) {

                Vote vote = new Vote(comments.get(voteCounter), VoteType.values()[j % 2]);
                voteRepository.save(vote);
            }

        }
    }

    /**
     * Authenticates the user with the parameter email <code>email</code>.<br>
     * This method will only work if the user is from this bootstrap file.
     *
     * @param email
     */
    private void setCurrentUser(String email) {
        if (email.startsWith("lock")) {
            return;
        }
        UserDetails details = accountRepository.findByEmail(email);
        String pw = details.getUsername().split("\\.")[0];
        Authentication auth = new UsernamePasswordAuthenticationToken(email, pw);
        auth = authenticationManager.authenticate(auth);
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

}

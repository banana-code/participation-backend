package edu.kit.tm.cm.participationservice.security.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMethod;

public class SimpleCorsFilter implements Filter {

    private boolean enabled;
    private String allowedOrigin;

    public SimpleCorsFilter(String allowedOrigin, boolean enable) {
        this.allowedOrigin = allowedOrigin == null ? "" : allowedOrigin;
        this.enabled = enable;
    }

    // TODO consolidate!
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {

        if (!enabled) {
            // if disabled go to the next filter
            chain.doFilter(req, res);
        } else {

            HttpServletResponse response = (HttpServletResponse) res;
            HttpServletRequest request = (HttpServletRequest) req;

            response.setHeader("Access-Control-Allow-Origin", this.allowedOrigin);
            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PATCH, PUT, HEAD");
            response.setHeader("Access-Control-Max-Age", "3600");
            response.setHeader("Access-Control-Allow-Headers",
                    "x-requested-with, authorization, content-type, accept, X-Token");
            response.setHeader("Access-Control-Expose-Headers", "Location");
            if (!request.getMethod().equals(RequestMethod.OPTIONS.name())) {
                chain.doFilter(req, res);
            }
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {}

    @Override
    public void destroy() {}

}

package edu.kit.tm.cm.participationservice.security.domain.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Account findByEmail(@Param("email") String email);

    @Query("SELECT acc "
            + "FROM Account acc JOIN acc.confirmationKey k "
            + "WHERE k.authorizationKey = :key")
    Account findUnconfirmedAccountByRegistrationKey(@Param("key") String key);

    @Query("SELECT acc "
            + "FROM Account acc JOIN acc.passwordResetKey k "
            + "WHERE k.authorizationKey = :key")
    Account findAccountByPasswordResetKey(@Param("key") String key);

    @Query("SELECT a "
            + "FROM Account a "
            + "WHERE LOWER(SUBSTRING(a.user.alias, 1, LENGTH(:aliasPrefix))) = LOWER(:aliasPrefix)")
    Set<Account> findAccountsByUserAliasPrefix(@Param("aliasPrefix") String aliasPrefix);

    @Query("SELECT a.user "
            + "FROM Account a "
            + "WHERE LOWER(SUBSTRING(a.user.alias, 1, LENGTH(:aliasPrefix))) = LOWER(:aliasPrefix)"
            + "AND state != 'LOCKED'")
    Set<User> findUsersByUserAliasPrefix(@Param("aliasPrefix") String aliasPrefix);
}

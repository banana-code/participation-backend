package edu.kit.tm.cm.participationservice.web.messages.request;

import javax.validation.constraints.NotNull;

import edu.kit.tm.cm.participationservice.GlobalConstants;

public class InviteMessage {

    @NotNull(message = GlobalConstants.EM_INVITE_INVALID_ALIAS)
    private long userId;

    protected InviteMessage() {}

    public InviteMessage(long userId) {
        this.userId = userId;
    }

    public long getUserId() {
        return userId;
    }

}

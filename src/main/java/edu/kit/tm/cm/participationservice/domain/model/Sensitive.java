package edu.kit.tm.cm.participationservice.domain.model;

/**
 * admin are not be allowed to modify or delete objects inherited from this interface.
 *
 * @author Daniel
 *
 */
public interface Sensitive {

}

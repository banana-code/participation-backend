package edu.kit.tm.cm.participationservice.web.validator.utility;

public class StringUtil {

    protected StringUtil() {}

    public static String arrayToString(String... objects) {
        StringBuilder result = new StringBuilder("");
        for (int i = 0; i < objects.length - 1; i++) {
            result.append(objects[i]);
            result.append(", ");
        }
        result.append(objects[objects.length - 1]);
        return result.toString();
    }
}

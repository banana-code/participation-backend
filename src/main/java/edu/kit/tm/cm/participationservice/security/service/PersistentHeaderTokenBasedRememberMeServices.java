package edu.kit.tm.cm.participationservice.security.service;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

/**
 * This implementation change the way the session is read and set.
 */
public class PersistentHeaderTokenBasedRememberMeServices extends PersistentTokenBasedRememberMeServices {

    private static final String HEADER_NAME = "X-Token";

    private PersistentTokenRepository tokenRepository;

    public PersistentHeaderTokenBasedRememberMeServices(String key, UserDetailsService userDetailsService,
            PersistentTokenRepository tokenRepository) {
        super(key, userDetailsService, tokenRepository);
        this.tokenRepository = tokenRepository;
    }

    @Override
    protected void setCookie(String[] tokens, int maxAge, HttpServletRequest request, HttpServletResponse response) {
        response.addHeader(HEADER_NAME, encodeCookie(tokens));
    }

    @Override
    protected String extractRememberMeCookie(HttpServletRequest request) {
        return request.getHeader(HEADER_NAME);
    }

    /**
     * @param email
     *            login account which has the given email
     * @return token to authenticate
     */
    public String login(String email) {
        PersistentRememberMeToken persistentToken = new PersistentRememberMeToken(email, generateSeriesData(),
                generateTokenData(), new Date());
        tokenRepository.createNewToken(persistentToken);
        return encodeCookie(new String[] { persistentToken.getSeries(), persistentToken.getTokenValue() });
    }
}

package edu.kit.tm.cm.participationservice.web.messages.request;

import org.hibernate.validator.constraints.NotEmpty;

import edu.kit.tm.cm.participationservice.GlobalConstants;

public class PasswordChangeMessage {

    private String oldPassword;

    @NotEmpty(message = GlobalConstants.EM_PASSWORD_INVALID)
    private String newPassword;

    protected PasswordChangeMessage() {}

    public PasswordChangeMessage(String oldPassword, String newPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

}

package edu.kit.tm.cm.participationservice.web.messages.response;

import java.util.LinkedList;
import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

/**
 * This class contains a list of ValidationErrors used by validators and other classes to respond errors to the client.
 */
public class ErrorMessage {

    private final List<ValidationError> errors = new LinkedList<ValidationError>();

    public ErrorMessage() {}

    public ErrorMessage(Errors errors) {
        for (FieldError fieldError : errors.getFieldErrors()) {
            String message = fieldError.getDefaultMessage();
            this.errors.add(new ValidationError(fieldError.getObjectName(), message, String.format("%s",
                    fieldError.getRejectedValue()), fieldError.getField()));
        }
        for (ObjectError objError : errors.getGlobalErrors()) {
            String message = objError.getDefaultMessage();
            this.errors.add(new ValidationError(objError.getObjectName(), message, "", ""));
        }
    }

    public ErrorMessage(MethodArgumentNotValidException ex) {
        this(ex.getBindingResult());
    }

    public ErrorMessage addError(String entity, String message, String invalidValue, String property) {
        errors.add(new ValidationError(entity, message, invalidValue, property));
        return this;
    }

    public List<ValidationError> getErrors() {
        return errors;
    }

    public boolean hasErrors() {
        return errors.size() > 0;
    }

    public static class ValidationError {

        private final String entity;
        private final String message;
        private final String invalidValue;
        private final String property;

        public ValidationError(String entity, String message, String invalidValue, String property) {
            this.entity = entity;
            this.message = message;
            this.invalidValue = invalidValue;
            this.property = property;
        }

        public String getEntity() {
            return entity;
        }

        public String getMessage() {
            return message;
        }

        public String getInvalidValue() {
            return invalidValue;
        }

        public String getProperty() {
            return property;
        }
    }
}

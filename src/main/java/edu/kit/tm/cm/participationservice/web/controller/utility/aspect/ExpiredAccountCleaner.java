package edu.kit.tm.cm.participationservice.web.controller.utility.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.repositories.UserRepository;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.security.domain.model.AuthorizationKey;
import edu.kit.tm.cm.participationservice.security.domain.repositories.AccountRepository;

/**
 * this class checks each account/user that comes from the database, <br>
 * if the according account is is expired and deletes it. <br>
 * <br>
 * this advice is called after sqlexceptioncatcher, (just in case if this throws an exception).
 */
@Aspect
@Component
@Order(1)
public class ExpiredAccountCleaner {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    @Around("execution(* edu.kit.tm.cm.participationservice.security.domain.repositories."
            + "AccountRepository.find*(..)) || "
            + "execution(* edu.kit.tm.cm.participationservice.domain.repositories."
            + "UserRepository.find*(..))")
    public Object removeExpiredAccounts(ProceedingJoinPoint joinPoint) throws Throwable {
        Object oldReturnValue = joinPoint.proceed();
        Object returnValue = oldReturnValue;
        if (returnValue != null && returnValue instanceof User) {
            User user = (User) returnValue;
            returnValue = user.getAccount();
        }
        if (returnValue != null && returnValue instanceof Account) {
            Account account = ((Account) returnValue);
            AuthorizationKey confirmationKey = account.getConfirmationKey();
            if (confirmationKey != null && confirmationKey.isExpired()) {
                accountRepository.delete(account);
                userRepository.delete(account.getUser());
                return null;
            }
        }
        return oldReturnValue;
    }
}

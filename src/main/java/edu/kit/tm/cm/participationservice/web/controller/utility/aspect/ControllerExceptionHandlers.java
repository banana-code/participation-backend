package edu.kit.tm.cm.participationservice.web.controller.utility.aspect;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.ErrorMessageException;
import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.NotFoundHttpException;
import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException;
import edu.kit.tm.cm.participationservice.web.messages.response.ErrorMessage;

/**
 * This will handle exceptions thrown by controller.
 */
@ControllerAdvice
public class ControllerExceptionHandlers {

    @ExceptionHandler(SqlException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorMessage handleSqlException(SqlException ex) {
        return new ErrorMessage(ex.getErrors());
    }

    @ExceptionHandler(ErrorMessageException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorMessage handleErrorException(ErrorMessageException ex) {
        return ex.getErrors();
    }

    @ExceptionHandler(NotFoundHttpException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handlePathNotFoundException(NotFoundHttpException ex) {}

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorMessage handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        return new ErrorMessage(ex);
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public void handleAccessDeniedException(AccessDeniedException ex) {}
}

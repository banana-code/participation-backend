package edu.kit.tm.cm.participationservice.web.messages.request;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import edu.kit.tm.cm.participationservice.GlobalConstants;

public class AliasChangeMessage {

    @NotEmpty(message = GlobalConstants.EM_ALIAS_EMPTY)
    @Size(max = GlobalConstants.MAX_ALIAS_LENGTH, message = GlobalConstants.EM_ALIAS_TOO_LONG)
    private String alias;

    protected AliasChangeMessage() {}

    public AliasChangeMessage(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return alias;
    }
}

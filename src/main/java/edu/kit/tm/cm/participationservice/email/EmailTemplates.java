package edu.kit.tm.cm.participationservice.email;

import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import edu.kit.tm.cm.participationservice.security.domain.model.Account;

@Service
public class EmailTemplates {

    private static final String TEMPLATE_ENCODING = "UTF-8";
    private static final String TEMPLATE_PREFIX = "/email/";

    @Autowired
    private VelocityEngine velocityEngine;

    @Autowired
    private JavaMailSender mailSender;

    @Value("${frontendServerUrl}")
    private String frontendServerUrl;

    @Value("${backendServerUrl}")
    private String backendServerUrl;

    @Value("${senderEmail}")
    private String senderEmail;

    /**
     * @param to
     *            destination account
     * @param key
     *            key to confirm account.
     * @return
     * @throws MessagingException
     */
    public MimeMessage createRegistrationConfirmationMail(Account to, String key)
            throws MessagingException {
        MimeMessageHelper message = createHtmlMimeMessageFromTemplate("registrationConfirmation.html", to,
                "url", frontendServerUrl + "/#/activate/" + key,
                "code", key);
        message.setSubject("Aktivierung deines Accounts");
        return message.getMimeMessage();
    }

    /**
     * @param to
     *            destination account
     * @param key
     *            url in email, where the password reset request can be confirmed.
     */
    public MimeMessage createPasswordResetRequestMail(Account to, String key) throws MessagingException {
        MimeMessageHelper message = createHtmlMimeMessageFromTemplate("passwordResetRequest.html", to,
                "url", frontendServerUrl + "/#/activate/" + key,
                "code", key);
        message.setSubject("Zurücksetzen deines Passworts");
        return message.getMimeMessage();
    }

    /**
     * @param templateName
     *            name of template file in src/main/resources/templates/email
     * @param to
     * @param model
     * @return
     * @throws MessagingException
     */
    private MimeMessageHelper createHtmlMimeMessageFromTemplate(String templateName, Account to,
            Map<String, Object> model) throws MessagingException {
        model.put("alias", to.getUser().getAlias());
        model.put("email", to.getEmail());
        model.put("backendUrl", backendServerUrl);
        model.put("frontendUrl", frontendServerUrl);
        MimeMessage simpleMail = mailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(simpleMail, TEMPLATE_ENCODING);
        mimeMessageHelper.setFrom(senderEmail);
        mimeMessageHelper.setTo(to.getEmail());
        mimeMessageHelper.setText(getTemplate(templateName, model), true);
        return mimeMessageHelper;
    }

    /**
     * @param templateName
     *            name of template file in src/main/resources/templates/email
     * @param to
     *            destination account
     * @param model
     *            number of model parameter must be even, they do have the following format: <br>
     *            key, value, key, value, ...
     * @return
     * @throws MessagingException
     */
    private MimeMessageHelper createHtmlMimeMessageFromTemplate(String templateName, Account to, String... model)
            throws MessagingException {
        if (model.length % 2 == 1) {
            throw new IllegalArgumentException("number of model parameter must be even/multiple of two");
        }
        Map<String, Object> modelMap = new HashMap<String, Object>();
        for (int i = 0; i < model.length; i += 2) {
            String key = model[i];
            String value = i < model.length + 1 ? model[i + 1] : null;
            modelMap.put(key, value);
        }
        return createHtmlMimeMessageFromTemplate(templateName, to, modelMap);
    }

    /**
     * @param templateName
     *            name of template file in src/main/resources/templates/email/
     * @param model
     *            parameter used in the template file.
     * @return
     */
    private String getTemplate(String templateName, Map<String, Object> model) {
        return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
                TEMPLATE_PREFIX + templateName,
                TEMPLATE_ENCODING,
                model);
    }
}

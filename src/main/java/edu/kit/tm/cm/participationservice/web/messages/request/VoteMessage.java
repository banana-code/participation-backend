package edu.kit.tm.cm.participationservice.web.messages.request;

import edu.kit.tm.cm.participationservice.domain.model.Vote.VoteType;

public class VoteMessage {

    private VoteType voteType;

    public VoteMessage() {};

    public VoteType getVoteType() {
        return voteType;
    }

}

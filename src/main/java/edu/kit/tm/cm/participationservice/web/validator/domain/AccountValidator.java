package edu.kit.tm.cm.participationservice.web.validator.domain;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException.SqlExceptionType;
import edu.kit.tm.cm.participationservice.web.validator.utility.AbstractValidator;
import edu.kit.tm.cm.participationservice.web.validator.utility.MultiKeyMap;

public class AccountValidator extends AbstractValidator<Account> {

    protected AccountValidator() {
        super(Account.class);
    }

    @Override
    protected void sqlExceptionErrorMessageMapping(MultiKeyMap errorMapping) {
        errorMapping.put(GlobalConstants.EM_REGISTER_EMAIL_TAKEN, SqlExceptionType.UNIQUE, "email");
        // we must also catch alias unique exceptions since the user is cascade persisted.
        errorMapping.put(GlobalConstants.EM_ALIAS_TAKEN, SqlExceptionType.UNIQUE, "user.alias");
    }
}

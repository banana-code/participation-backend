package edu.kit.tm.cm.participationservice.security.config;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.RememberMeAuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.security.AuthenticationEventHandler;
import edu.kit.tm.cm.participationservice.security.UnconfirmedLoginChecker;
import edu.kit.tm.cm.participationservice.security.filter.CustomAuthenticationFilter;
import edu.kit.tm.cm.participationservice.security.filter.CustomLogoutFilter;
import edu.kit.tm.cm.participationservice.security.service.PersistentHeaderTokenBasedRememberMeServices;

@Configuration
@PropertySource("classpath:application.properties")
public class SecurityBeans {

    private static final String KEY = UUID.randomUUID().toString();

    @Value("${tokenValiditySeconds}")
    private int tokenValiditySeconds;

    @Value("${seriesLength}")
    private int seriesLength;

    @Value("${tokenLength}")
    private int tokenLength;

    /**
     * We use a custom UserDetailsService which is a AccountService object
     */
    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * Bcrypt password encoder
     */
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * used by authenticationManager to authenticate via token.
     */
    @Autowired
    private RememberMeAuthenticationProvider rememberMeAuthenticationProvider;

    /**
     * asserts only confirmed accounts can login.
     */
    @Autowired
    private UnconfirmedLoginChecker unconfirmedLoginChecker;

    /**
     * used by authenticationManager to authenticate with username/password.
     */
    @Autowired
    private DaoAuthenticationProvider daoAuthenticationProvider;

    /**
     * to authenticate users.
     */
    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * This is used by rememberMeAuthentication to save tokens. <br>
     * It is responsible to persist tokens to the database.
     */
    @Autowired
    private PersistentTokenRepository tokenRepository;

    /**
     * this object contains methods which are executed when login fails/succeds or logout suceeds.
     */
    @Autowired
    private AuthenticationEventHandler authenticationEventHandler;

    /**
     * This object is used by RememberMeAuthenticationFilter.<br>
     * It is an instance of PersistentHeaderTokenBasedRememberMeServices<br>
     * and it sets the session data (X-Token)
     */
    @Autowired
    private RememberMeServices rememberMeServices;

    @Bean
    public AuthenticationManager authenticationManagerBean() {
        List<AuthenticationProvider> authenticationProvider = new LinkedList<AuthenticationProvider>();
        authenticationProvider.add(rememberMeAuthenticationProvider);
        authenticationProvider.add(daoAuthenticationProvider);
        ProviderManager providerManager = new ProviderManager(authenticationProvider);
        return providerManager;
    }

    @Bean
    public RememberMeAuthenticationProvider rememberMeAuthenticationProvider() {
        return new RememberMeAuthenticationProvider(KEY);
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder);
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        daoAuthenticationProvider.setPreAuthenticationChecks(unconfirmedLoginChecker);
        daoAuthenticationProvider.setHideUserNotFoundExceptions(false);
        return daoAuthenticationProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public PersistentHeaderTokenBasedRememberMeServices rememberMeService() {
        PersistentHeaderTokenBasedRememberMeServices rememberMeServices =
                new PersistentHeaderTokenBasedRememberMeServices(KEY, userDetailsService, tokenRepository);
        rememberMeServices.setAlwaysRemember(true);
        rememberMeServices.setTokenValiditySeconds(tokenValiditySeconds);
        rememberMeServices.setSeriesLength(seriesLength);
        rememberMeServices.setTokenLength(tokenLength);
        return rememberMeServices;
    }

    public RememberMeAuthenticationFilter rememberMeAuthenticationFilter() {
        return new RememberMeAuthenticationFilter(authenticationManager, rememberMeServices);
    }

    public UsernamePasswordAuthenticationFilter usernamePasswordAuthenticationFilter() {
        UsernamePasswordAuthenticationFilter authenticationFilter = new CustomAuthenticationFilter();
        authenticationFilter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(
                GlobalConstants.URI_PREFIX + "/login"));
        authenticationFilter.setUsernameParameter("email");
        authenticationFilter.setPasswordParameter("password");
        authenticationFilter.setAuthenticationSuccessHandler(authenticationEventHandler);
        authenticationFilter.setAuthenticationFailureHandler(authenticationEventHandler);
        authenticationFilter.setRememberMeServices(rememberMeServices);
        authenticationFilter.setAuthenticationManager(authenticationManager);
        authenticationFilter.setPostOnly(true);
        return authenticationFilter;
    }

    public LogoutFilter logoutFilter() {
        LogoutHandler[] handlers = new LogoutHandler[]
        {
                (LogoutHandler) rememberMeServices,
                new SecurityContextLogoutHandler()
        };
        LogoutFilter logoutFilter = new CustomLogoutFilter(authenticationEventHandler, handlers);
        logoutFilter.setLogoutRequestMatcher(new AntPathRequestMatcher(GlobalConstants.URI_PREFIX + "/logout"));
        return logoutFilter;
    }
}

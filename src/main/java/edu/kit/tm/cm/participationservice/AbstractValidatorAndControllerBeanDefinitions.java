package edu.kit.tm.cm.participationservice;

import java.util.Set;

import org.reflections.Reflections;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.stereotype.Component;

import edu.kit.tm.cm.participationservice.web.controller.AbstractController;
import edu.kit.tm.cm.participationservice.web.validator.utility.AbstractValidator;

@Component
public class AbstractValidatorAndControllerBeanDefinitions implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
        BeanDefinitionRegistry beanRegistry = (BeanDefinitionRegistry) beanFactory;
        Reflections reflections = new Reflections(GlobalConstants.BASE_PACKAGE);

        // TODO generalize for loop/fix warning
        // validator
        @SuppressWarnings("rawtypes")
        Set<Class<? extends AbstractValidator>> validatorClasses =
                reflections.getSubTypesOf(AbstractValidator.class);
        for (Class<?> curClass : validatorClasses) {
            beanRegistry.registerBeanDefinition(curClass.getSimpleName(), new RootBeanDefinition(curClass));
        }

        // controller
        Set<Class<? extends AbstractController>> controllerClasses =
                reflections.getSubTypesOf(AbstractController.class);
        for (Class<?> curClass : controllerClasses) {
            beanRegistry.registerBeanDefinition(curClass.getSimpleName(), new RootBeanDefinition(curClass));
        }
    }
}

package edu.kit.tm.cm.participationservice.domain.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.domain.model.User;

public interface SolutionRepository extends JpaRepository<Solution, Long> {

    /**
     * for user means: get solution the user is allowed to see
     *
     * @param issue
     * @param user
     * @return
     */
    @Query("SELECT s "
            + "FROM Solution s "
            + "WHERE s.issue = :issue "
            + "AND (s.issue.startDate IS NOT NULL AND s.issue.startDate < CURRENT_TIMESTAMP "
            + "OR s.createdBy = :user)")
    Set<Solution> findVisibleForUserByIssue(@Param("issue") Issue issue, @Param("user") User user);

}

package edu.kit.tm.cm.participationservice.web.validator.message;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.web.messages.request.RegistrationMessage;
import edu.kit.tm.cm.participationservice.web.validator.utility.AbstractValidator;
import edu.kit.tm.cm.participationservice.web.validator.utility.ErrorsHelper;

@Lazy
@PropertySource("classpath:application.properties")
public class RegistrationValidator extends AbstractValidator<RegistrationMessage> {

    @Value("${emailDomainRegex}")
    private String emailDomainRegex;

    protected RegistrationValidator() {
        super(RegistrationMessage.class);
    }

    @Override
    public void validateInternal(RegistrationMessage registration, ErrorsHelper errorsHelper) {
        errorsHelper.useField("email");
        if (registration.getEmail() != null) {
            if (!registration.getEmail().matches(GlobalConstants.EMAIL_FORMAT_REGEX + emailDomainRegex)) {
                errorsHelper.reject(GlobalConstants.EM_REGISTER_EMAIL_INVALID);
            }
        }
    }
}

package edu.kit.tm.cm.participationservice.web.controller.utility.aspect.sqlexception;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException;
import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException.SqlExceptionType;
import edu.kit.tm.cm.participationservice.web.validator.utility.IndexParser;
import edu.kit.tm.cm.participationservice.web.validator.utility.IndexParser.Index;

@Component
public class MySQLSqlExceptionCatcher implements AbstractSqlExceptionParser {

    private static final String FIELD_NAME = "field";
    private static final String INDEX_NAME = "index";

    @Value("${dbType}")
    private String dbType;

    private Map<Pattern, SqlExceptionType> exceptionMapping = new HashMap<>();

    public MySQLSqlExceptionCatcher() {
        // Duplicate entry 'Group name' for key 'unique__group_entity__title'
        exceptionMapping.put(Pattern.compile("Duplicate\\sentry\\s'(.*?)'\\sfor\\skey\\s'(?<" + INDEX_NAME + ">.*?)'"),
                SqlExceptionType.UNIQUE);
        // Column 'group_id' cannot be null
        exceptionMapping.put(Pattern.compile("Column\\s'(?<" + FIELD_NAME + ">.*?)'\\scannot\\sbe\\snull"),
                SqlExceptionType.NULL);
    }

    @Override
    public SqlException parseException(Object entity, DataIntegrityViolationException e) {
        if (!dbType.equals("MySQL")) {
            return null;
        }
        String exceptionMessage = e.getMostSpecificCause().getMessage();
        for (Pattern pattern : exceptionMapping.keySet()) {
            Matcher matcher = pattern.matcher(exceptionMessage);
            if (matcher.find()) {
                // exceptionType
                SqlExceptionType exceptionType = exceptionMapping.get(pattern);

                // fields involved in exception
                String[] fields;
                switch (exceptionType) {
                case UNIQUE:
                    String indexName = matcher.group(INDEX_NAME);
                    Index indexObj = IndexParser.parseIndex(indexName);
                    fields = indexObj.getFields();
                    break;
                case NULL:
                    String field = matcher.group(FIELD_NAME);
                    fields = field.split(",");
                    break;
                default:
                    return null;
                }
                Errors errors = new BeanPropertyBindingResult(entity, entity.getClass().getSimpleName());
                for (int i = 0; i < fields.length; i++) {
                    fields[i] = fields[i].trim();
                }

                return new SqlException(fields, exceptionType, errors);
            }
        }
        return null;
    }
}

package edu.kit.tm.cm.participationservice.security;

import org.springframework.security.core.AuthenticationException;

public class UnconfirmedLoginException extends AuthenticationException {

    private static final long serialVersionUID = -2754303655512401694L;

    public UnconfirmedLoginException(String msg) {
        super(msg);
    }
}

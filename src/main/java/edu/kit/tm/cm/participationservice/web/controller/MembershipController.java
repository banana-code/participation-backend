package edu.kit.tm.cm.participationservice.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Membership;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.service.MembershipService;
import edu.kit.tm.cm.participationservice.web.messages.request.InviteMessage;
import edu.kit.tm.cm.participationservice.web.messages.request.MembershipStateChangeMessage;

public class MembershipController extends AbstractController {

    private static final String PREFIX = "/groups/{groupID}/members";

    @Autowired
    private MembershipService membershipService;

    @RequestMapping(value = PREFIX, method = RequestMethod.GET)
    @ResponseBody
    public List<Membership> getMembers(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group) {

        return membershipService.getMembers(account.getUser(), group);
    }

    @RequestMapping(value = PREFIX, method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Membership invite(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group,
            @Valid @RequestBody InviteMessage inviteMessage) {

        return membershipService.inviteOrRequestMembershipInGroup(account.getUser(), group, inviteMessage);

    }

    @RequestMapping(value = PREFIX + "/{memberID}", method = RequestMethod.PATCH)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Membership changeMembership(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group,
            @PathVariable("memberID") Membership currentMembership,
            @Valid @RequestBody MembershipStateChangeMessage membershipStateChangeMessage) {

        return membershipService.changeMembership(account.getUser(), currentMembership, membershipStateChangeMessage);
    }

    @RequestMapping(value = PREFIX + "/{memberID}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteMembership(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group,
            @PathVariable("memberID") Membership deleteMembership) {

        membershipService.deleteMembership(account.getUser(), deleteMembership);
    }
}

package edu.kit.tm.cm.participationservice.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.util.Assert;

import edu.kit.tm.cm.participationservice.GlobalConstants;

/**
 * A resistance object describes the rating of one user for one solution.<br>
 * The rating value is between 0 and 10, high ratings are worse while low ratings are better.
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(
        columnNames = { "solution_id", "created_by_id" },
        name = "unique__resistance__solution_id__created_by_id"))
public class Resistance extends AuditableEntity implements Sensitive {

    private static final long serialVersionUID = -1307430549184220678L;

    @Column(nullable = false)
    private int rating;

    @ManyToOne(optional = false)
    @JoinColumn(nullable = false)
    private Solution solution;

    protected Resistance() {}

    public Resistance(Solution solution, int rating) {
        Assert.notNull(solution, "The solution can not be null");
        this.solution = solution;
        setRating(rating);
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        assertRating(rating);
        this.rating = rating;
    }

    public Solution getSolution() {
        return solution;
    }

    private static void assertRating(int rating) {
        if (!(rating >= GlobalConstants.MIN_RATING_VALUE && rating <= GlobalConstants.MAX_RATING_VALUE)) {
            throw new IllegalArgumentException(
                    String.format("Rating must be bewteen %d and %d", GlobalConstants.MIN_RATING_VALUE,
                            GlobalConstants.MAX_RATING_VALUE));
        }
    }

    /**
     * Custom equals implementation to reflect the unique constraints.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Resistance)) {
            return false;
        }
        Resistance param = (Resistance) obj;
        return param.getCreatedBy().equals(this.getCreatedBy())
                && param.getSolution().equals(this.getSolution());
    }

    /**
     * auto generated function.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + rating;
        result = prime * result + ((solution == null) ? 0 : solution.hashCode());
        return result;
    }
}

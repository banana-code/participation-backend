package edu.kit.tm.cm.participationservice;

/**
 * Class for global constants used in the whole project.
 */
public final class GlobalConstants {

    public static final String BASE_PACKAGE = "edu.kit.tm.cm";

    public static final String URI_PREFIX = "/api";

    public static final String ALIAS_DELETED_USER = "[Benutzer gelöscht]";
    public static final String ALIAS_LOCKED_USER = "[Benutzer gesperrt]";

    public static final int MAX_ALIAS_LENGTH = 32;

    public static final int MAX_GROUP_TITLE_LENGTH = 64;
    public static final int MAX_GROUP_DESCRIPTION_LENGTH = 1024;

    public static final int MAX_ISSUE_TITLE_LENGTH = 64;
    public static final int MAX_ISSUE_DESCRIPTION_LENGTH = 1024;

    public static final int MAX_RATING_VALUE = 10;
    public static final int MIN_RATING_VALUE = 0;

    public static final String EMAIL_FORMAT_REGEX = "[^@]+@";

    public static final int MAX_SOLUTION_TITLE_LENGTH = 64;
    public static final int MAX_SOLUTION_DESCRIPTION_LENGTH = 1024;

    public static final String ZERO_OPTION_DEFAULT_TITLE = "Nulllösung";
    public static final String ZERO_OPTION_DEFAULT_DESCRIPTION =
            "Alles bleibt beim Alten oder wir treffen keine Entscheidung";

    public static final int MAX_COMMENT_LENGTH = 1024;

    /**
     * Error messages (EM) that are returned to the client
     */

    public static final String EM_PASSWORD_RESET_EMAIL_INVALID = "email_invalid";

    public static final String EM_CONFIRMATION_KEY_INVALID = "already_confirmed_or_invalid_key";

    public static final String EM_INVITE_ALREADY_MEMBER = "invite_already_member";
    public static final String EM_INVITE_ALREADY_INVITED = "invite_already_invited";
    public static final String EM_INVITE_ALREADY_REQUESTED = "invite_already_requested";
    public static final String EM_INVITE_INVALID_ALIAS = "invite_invalid_alias";
    public static final String EM_INVITE_USER_ID = "invite_invalid_id";

    public static final String EM_MEMBERSHIP_TOO_FEW_OWNER = "membership_too_few_owner";
    public static final String EM_MEMBERSHIP_STATE_INVALID = "membership_state_invalid";

    public static final String EM_PASSWORD_INVALID = "password_invalid";
    public static final String EM_PASSWORD_CHANGE_INCORRECT = "password_change_incorrect";

    public static final String EM_REGISTER_EMAIL_INVALID = "register_email_invalid";
    public static final String EM_REGISTER_EMAIL_TAKEN = "register_email_taken";

    public static final String EM_RESISTANCE_CREATION_FAILED = "resistance_creation_failed";
    public static final String EM_RESISTANCE_INVALID = "resistance_invalid_number";

    public static final String EM_SOLUTION_DELETION_ZERO_OPTION = "solution_deletion_zero_option";
    public static final String EM_SOLUTION_TITLE_EMPTY = "solution_title_empty";
    public static final String EM_SOLUTION_TITLE_TOO_LONG = "solution_title_toolong";
    public static final String EM_SOLUTION_DESCRIPTION_TOO_LONG = "solution_description_toolong";

    public static final String EM_SEARCH_INVALID_ALIAS_PREFIX = "search_invalid_aliasPrefix";

    public static final String EM_ISSUE_TITLE_EMPTY = "issue_title_empty";
    public static final String EM_ISSUE_TITLE_TOO_LONG = "issue_title_toolong";
    public static final String EM_ISSUE_DESCRIPTION_TOO_LONG = "issue_description_toolong";
    public static final String EM_ISSUE_DEADLINE_ORDER = "issue_deadline_order";
    public static final String EM_ISSUE_SOLUTION_DATE_INVALID = "issue_solution_date_invalid";
    public static final String EM_ISSUE_VOTE_DATE_INVALID = "issue_vote_date_invalid";
    public static final String EM_ISSUE_SOLUTION_DATE_PAST = "issue_solution_date_past";
    public static final String EM_ISSUE_VOTE_DATE_PAST = "issue_vote_date_past";

    public static final String EM_GROUP_INVALID_TITLE = "group_invalid_title";
    public static final String EM_GROUP_TITLE_TOO_LONG = "group_title_toolong";
    public static final String EM_GROUP_DESCRIPTION_TOO_LONG = "group_description_toolong";
    public static final String EM_GROUP_TITLE_TAKEN = "group_title_taken";

    public static final String EM_ALIAS_EMPTY = "alias_empty";
    public static final String EM_ALIAS_TOO_LONG = "alias_toolong";
    public static final String EM_ALIAS_TAKEN = "alias_taken";

    public static final String EM_COMMENT_EMPTY = "comment_empty";
    public static final String EM_COMMENT_TOO_LONG = "comment_toolong";

    private GlobalConstants() {}
}

package edu.kit.tm.cm.participationservice.web.validator.utility;

import org.springframework.validation.Errors;

import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException.SqlExceptionType;

/**
 * a interface to specify a handler which handles sql exceptions.
 */
public interface SqlExceptionHandler {

    /**
     *
     * @param target
     * @param errors
     * @param type
     * @param fields
     */
    void handleSqlException(Object target, Errors errors, SqlExceptionType type, String... fields);
}

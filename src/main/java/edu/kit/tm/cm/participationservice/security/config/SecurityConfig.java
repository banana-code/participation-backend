package edu.kit.tm.cm.participationservice.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.header.HeaderWriterFilter;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.security.filter.ForceHttpsFilter;
import edu.kit.tm.cm.participationservice.security.filter.SimpleCorsFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, order = 1)
@PropertySource("classpath:application.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${enableForceHttpsFilter}")
    private Boolean enableForceHttpsFilter;

    @Value("${enableCorsFilter}")
    private Boolean enableCorsFilter;

    @Value("${serverHttpsPort}")
    private int serverHttpsPort;

    @Value("${allowedOrigin}")
    private String allowedOrigin;

    @Autowired
    private SecurityBeans securityBeans;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Overwrite the default HttpSessionRequestCache with one
        // that doesn't create empty Sessions. This should be helpful
        // towards preventing the Tomcat container to set a JSESSIONID
        // cookie!
        HttpSessionRequestCache httpSessionRequestCache = new HttpSessionRequestCache();
        httpSessionRequestCache.setCreateSessionAllowed(false);
        http.setSharedObject(RequestCache.class, httpSessionRequestCache);

        http
                // we don't need this because we have our own token based authentication without cookies
                .csrf()
                .disable()

                // disable cookie check
                .sessionManagement()
                .disable()
                .securityContext()
                .disable()

                .authorizeRequests()

                // Prevents authentication of HTTP OPTIONS preflight requests which would otherwise break CORS
                .antMatchers(HttpMethod.OPTIONS, "/**")
                .anonymous()

                // used for account creation
                // TODO requires-no-confirmation annotation in controller? or something similiar
                .antMatchers(HttpMethod.POST,
                        GlobalConstants.URI_PREFIX + "/register",
                        GlobalConstants.URI_PREFIX + "/confirm",
                        GlobalConstants.URI_PREFIX + "/resetPassword",
                        GlobalConstants.URI_PREFIX + "/setNewPassword")
                .anonymous()

                // api must be authenticated
                .antMatchers(GlobalConstants.URI_PREFIX + "/**")
                .authenticated()

                // everything else can be accessed by everyone
                .anyRequest()
                .permitAll()
                .and()

                // disable default behavior
                .formLogin()
                .disable()
                .logout()
                .disable()

                .addFilter(securityBeans.usernamePasswordAuthenticationFilter())
                .addFilter(securityBeans.logoutFilter())

                // force https in production mode
                .addFilterAfter(new ForceHttpsFilter(enableForceHttpsFilter, serverHttpsPort), HeaderWriterFilter.class)

                // enable cors filter in production mode
                .addFilterAfter(new SimpleCorsFilter(allowedOrigin, enableCorsFilter), ForceHttpsFilter.class)

                // the rememberMeAuthenticationFilter sets the authentication object depending on X-Token, so it
                // must be
                // at least before logoutFilter
                .addFilterAfter(securityBeans.rememberMeAuthenticationFilter(), SimpleCorsFilter.class);
    }
}

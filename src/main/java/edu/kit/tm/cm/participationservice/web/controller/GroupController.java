package edu.kit.tm.cm.participationservice.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.repositories.GroupRepository;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.service.GroupService;
import edu.kit.tm.cm.participationservice.web.messages.request.GroupChangeMessage;
import edu.kit.tm.cm.participationservice.web.messages.request.GroupCreationMessage;

public class GroupController extends AbstractController {

    private static final String PREFIX = "/groups";

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private GroupService groupService;

    @RequestMapping(value = PREFIX, method = RequestMethod.GET)
    @ResponseBody
    public List<GroupEntity> getAllGroups() {
        return groupRepository.findAll();
    }

    @RequestMapping(value = PREFIX + "/myGroups", method = RequestMethod.GET)
    @ResponseBody
    public List<GroupEntity> getMyGroups(@AuthenticationPrincipal Account account) {
        return groupRepository.findMyGroups(account.getUser());
    }

    @RequestMapping(value = PREFIX + "/{groupID}", method = RequestMethod.GET)
    @ResponseBody
    public GroupEntity getOneGroup(@PathVariable("groupID") GroupEntity group) {
        return group;
    }

    @RequestMapping(value = PREFIX, method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public GroupEntity createGroup(@AuthenticationPrincipal Account account,
            @Valid @RequestBody GroupCreationMessage groupCreationMessage) {

        return groupService.createGroup(account.getUser(), groupCreationMessage);
    }

    @RequestMapping(value = PREFIX + "/{groupID}", method = RequestMethod.PATCH)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public GroupEntity changeGroup(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity currentGroup,
            @Valid @RequestBody GroupChangeMessage groupChangeMessage) {

        return groupService.changeGroup(account.getUser(), currentGroup, groupChangeMessage);
    }

    @RequestMapping(value = PREFIX + "/{groupID}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteGroup(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group) {

        groupService.deleteGroup(account.getUser(), group);
    }
}

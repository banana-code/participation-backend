package edu.kit.tm.cm.participationservice.web.controller;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.service.IssueService;
import edu.kit.tm.cm.participationservice.web.messages.request.IssueMessage;

public class IssueController extends AbstractController {

    private static final String PREFIX = "/groups/{groupID}/issues";

    @Autowired
    private IssueService issueService;

    @RequestMapping(value = PREFIX, method = RequestMethod.GET)
    @ResponseBody
    public Set<Issue> getIssuesFromGroup(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group) {

        return issueService.getVisibleIssuesOfGroup(account.getUser(), group);
    }

    @RequestMapping(value = PREFIX + "/{issueID}", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("@authorizationHandler.userIsAllowedToGetSingleIssue(#account.user, #issue)")
    public Issue getOneIssue(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group,
            @PathVariable("issueID") Issue issue) {

        return issue;
    }

    @RequestMapping(value = PREFIX, method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Issue createIssueInGroup(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group,
            @Valid @RequestBody IssueMessage issueMessage) {

        return issueService.createIssue(account.getUser(), group, issueMessage);
    }

    @RequestMapping(value = PREFIX + "/{issueID}", method = RequestMethod.PATCH)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Issue changeIssue(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group,
            @PathVariable("issueID") Issue currentIssue,
            @Valid @RequestBody IssueMessage changeIssue) {

        return issueService.changeIssue(account.getUser(), currentIssue, changeIssue);
    }

    @RequestMapping(value = PREFIX + "/{issueID}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteIssue(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group,
            @PathVariable("issueID") Issue issue) {

        issueService.deleteIssue(account.getUser(), issue);
    }
}

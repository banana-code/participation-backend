package edu.kit.tm.cm.participationservice.web.messages.request;

public class AliasSearchMessage {

    private String aliasPrefix;

    protected AliasSearchMessage() {}

    public AliasSearchMessage(String aliasPrefix) {
        this.aliasPrefix = aliasPrefix;
    }

    public String getAliasPrefix() {
        return aliasPrefix;
    }
}

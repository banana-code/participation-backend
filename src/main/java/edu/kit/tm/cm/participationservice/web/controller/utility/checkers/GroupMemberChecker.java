package edu.kit.tm.cm.participationservice.web.controller.utility.checkers;

import java.util.EnumSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Membership;
import edu.kit.tm.cm.participationservice.domain.model.Membership.MembershipState;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.repositories.MembershipRepository;

@Component
public class GroupMemberChecker {

    private static final Set<MembershipState> MEMBER_STATES = EnumSet.of(MembershipState.MEMBER,
            MembershipState.MODERATOR, MembershipState.OWNER);

    private static final Set<MembershipState> MODERATORATION_STATES = EnumSet.of(MembershipState.MODERATOR,
            MembershipState.OWNER);

    private static MembershipRepository membershipRepository;

    @Autowired
    protected GroupMemberChecker(MembershipRepository membershipRepository) {
        GroupMemberChecker.membershipRepository = membershipRepository;
    }

    /**
     *
     * @param user
     * @param group
     * @return true if the given user is in group.
     */
    public boolean userIsInGroup(User user, GroupEntity group) {
        Membership membership = membershipRepository.findByUserAndGroup(user, group);
        if (membership != null) {
            return MEMBER_STATES.contains(membership.getState());
        } else {
            return false;
        }
    }

    /**
     *
     * @param user
     * @param group
     * @return true if the given user is in group.
     */
    public boolean userHasModerationRightsInGroup(User user, GroupEntity group) {
        Membership membership = membershipRepository.findByUserAndGroup(user, group);
        if (membership != null) {
            return MODERATORATION_STATES.contains(membership.getState());
        } else {
            return false;
        }
    }

    /**
     *
     * @param user
     * @param group
     * @return true if the given user is in group.
     */
    public boolean userIsOwnerOfGroup(User user, GroupEntity group) {
        Membership membership = membershipRepository.findByUserAndGroup(user, group);
        if (membership != null) {
            return MembershipState.OWNER.equals(membership.getState());
        } else {
            return false;
        }
    }

    /**
     *
     * @param user
     * @param group
     * @return true if the given user is in group.
     */
    public boolean userIsModeratorOfGroup(User user, GroupEntity group) {
        Membership membership = membershipRepository.findByUserAndGroup(user, group);
        if (membership != null) {
            return MembershipState.MODERATOR.equals(membership.getState());
        } else {
            return false;
        }
    }

    /**
     *
     * @param user
     * @param group
     * @return true if the given user is in group.
     */
    public boolean userIsMemberOfGroup(User user, GroupEntity group) {
        Membership membership = membershipRepository.findByUserAndGroup(user, group);
        if (membership != null) {
            return MembershipState.MEMBER.equals(membership.getState());
        } else {
            return false;
        }
    }
}

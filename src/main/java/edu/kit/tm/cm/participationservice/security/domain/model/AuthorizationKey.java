package edu.kit.tm.cm.participationservice.security.domain.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import edu.kit.tm.cm.participationservice.domain.model.PersistableEntity;

/**
 * general key class for account confirmation or password reset.
 */
@Entity
public class AuthorizationKey extends PersistableEntity {

    private static final long serialVersionUID = 3185097015455846302L;

    @Column(unique = true)
    private String authorizationKey;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime expirationDate;

    public AuthorizationKey() {}

    public AuthorizationKey(DateTime expirationDate) {
        // TODO the random value might conflict with the unique key constraint, which results in a sql exception, then
        // no key is created (very unlikely).
        this.authorizationKey = UUID.randomUUID().toString();
        this.expirationDate = expirationDate;
    }

    public String getKey() {
        return authorizationKey;
    }

    public void setKey(String key) {
        this.authorizationKey = key;
    }

    public DateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(DateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean isExpired() {
        return DateTime.now().isAfter(expirationDate);
    }
}

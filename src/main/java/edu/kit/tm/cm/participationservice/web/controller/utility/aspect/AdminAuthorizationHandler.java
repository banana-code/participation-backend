package edu.kit.tm.cm.participationservice.web.controller.utility.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import edu.kit.tm.cm.participationservice.domain.model.Publishable;
import edu.kit.tm.cm.participationservice.domain.model.Sensitive;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.security.domain.model.Account.AccountState;

/**
 * this class catches exceptions from <code>AuthorizationHandler</code> and and opens all get request to admins
 */
@Aspect
@Component
@Order(2)
public class AdminAuthorizationHandler {

    @Pointcut("execution(* edu.kit.tm.cm.participationservice.security."
            + "AuthorizationHandler.userIsAllowedTo*(..))")
    private void allAuthorizationMethod() {}

    @Pointcut("execution(* edu.kit.tm.cm.participationservice.security."
            + "AuthorizationHandler.userIsAllowedToCreate*(..))")
    private void createAuthorizationMethod() {}

    @Around("allAuthorizationMethod() && !createAuthorizationMethod()")
    public Object aroundPermissionCheck(ProceedingJoinPoint joinPoint) throws Throwable {
        Account account = getAuthenticatedAccount();
        Object[] args = joinPoint.getArgs();
        Object entity = null;
        if (args != null && args.length >= 2) {
            entity = args[1];
        }

        // unauthenticated users don't have permission
        if (account == null) {
            return false;
        }

        // admin has permission to do everything
        if (account.getState().equals(AccountState.ADMIN)) {
            if (entity != null) {
                if (entity instanceof Publishable) {
                    boolean entityIsPublished = ((Publishable) entity).isPublished();
                    if (!entityIsPublished) {
                        // admin is not allowed to access unpublished content
                        return (boolean) joinPoint.proceed();
                    }
                }
                if (entity instanceof Sensitive) {
                    // admin is not allowed to access sensitive data
                    return (boolean) joinPoint.proceed();
                }
            }
            return true;
        }

        // return proceeded return value
        try {
            return (boolean) joinPoint.proceed();
        } catch (Exception e) {
            // if method throws an exception, the user does not have the permission
            return false;
        }
    }

    private Account getAuthenticatedAccount() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null || !auth.isAuthenticated()) {
            return null;
        }
        Object principal = auth.getPrincipal();
        if (principal != null && principal instanceof Account) {
            return ((Account) principal);
        } else {
            return null;
        }
    }
}

package edu.kit.tm.cm.participationservice.web.validator.utility;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException.SqlExceptionType;

/**
 * This class is used to map SqlExceptions to error messages.
 */
public class MultiKeyMap {

    private Map<SqlExceptionType, Map<String, Result>> data = new HashMap<>();

    public Result get(SqlExceptionType type, String... fields) {
        Map<String, Result> innerMap = data.get(type);
        return innerMap != null ? innerMap.get(prepFields(fields)) : null;
    }

    public Result put(String errorMessage, SqlExceptionType exceptionType, String... fields) {
        Map<String, Result> innerMap = data.get(exceptionType);
        if (innerMap == null) {
            innerMap = new HashMap<>();
            data.put(exceptionType, innerMap);
        }
        // Since the key is lowercased we save the original key in the result object.
        Result result = new Result(errorMessage, exceptionType, fields);
        return innerMap.put(prepFields(fields), result);
    }

    /**
     * Prepares the fields array to be used as string parameter for the inner map. <br>
     * This is necessary, because the fields shall be case insensitive and the order should not matter.<br>
     *
     * @param fields
     * @return
     */
    private String prepFields(String... fields) {
        // toLowerCase each field's name
        // e.g. "rofl.CopteRama" will be processed to copterama
        String[] fieldsCopy = new String[fields.length];
        System.arraycopy(fields, 0, fieldsCopy, 0, fields.length);
        for (int i = 0; i < fieldsCopy.length; i++) {
            String[] fieldNavigation = fieldsCopy[i].split("\\.");
            String fieldName = fieldNavigation[fieldNavigation.length - 1];
            fieldsCopy[i] = fieldName.toLowerCase();
        }
        // sort lowercased strings
        Arrays.sort(fieldsCopy);
        return StringUtil.arrayToString(fieldsCopy);
    }

    public static class Result {

        private SqlExceptionType exceptionType;
        private String[] fields;
        private String errorMessage;

        public Result(String errorMessage, SqlExceptionType exceptionType, String... fields) {
            this.errorMessage = errorMessage;
            this.exceptionType = exceptionType;
            this.fields = fields;
        }

        public SqlExceptionType getExceptionType() {
            return exceptionType;
        }

        public String[] getFields() {
            return fields;
        }

        public String getErrorMessage() {
            return errorMessage;
        }
    }
}

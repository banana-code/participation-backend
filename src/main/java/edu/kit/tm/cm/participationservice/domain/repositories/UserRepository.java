package edu.kit.tm.cm.participationservice.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import edu.kit.tm.cm.participationservice.domain.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByAlias(@Param("alias") String alias);

    // Inner join needed therefore located in accountrepository
    // @Query("SELECT u "
    // + "FROM User u "
    // + "WHERE LOWER(SUBSTRING(u.alias, 1, LENGTH(:aliasPrefix))) = LOWER(:aliasPrefix)")
    // Set<User> findByAliasPrefix(@Param("aliasPrefix") String aliasPrefix);

    @Override
    void delete(User entity);

}

package edu.kit.tm.cm.participationservice.web.validator.utility;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException.SqlExceptionType;
import edu.kit.tm.cm.participationservice.web.validator.utility.MultiKeyMap.Result;

/**
 * A validator which supports a certain class.
 *
 * @param <T>
 *            the type to validate.
 */
public abstract class AbstractValidator<T> implements Validator, SqlExceptionHandler {

    private Class<T> supportedClass;

    protected AbstractValidator(Class<T> supportedClass) {
        this.supportedClass = supportedClass;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        if (clazz.isAssignableFrom(supportedClass)) {
            return true;
        }
        return false;
    }

    @Override
    public final void validate(Object target, Errors errors) {
        if (!supports(target.getClass())) {
            return;
        }
        validateInternal(supportedClass.cast(target), new ErrorsHelper(errors));
    }

    @Override
    public final void handleSqlException(Object target, Errors errors, SqlExceptionType type, String... fields) {
        if (!supports(target.getClass())) {
            return;
        }
        MultiKeyMap errorMapping = new MultiKeyMap();
        sqlExceptionErrorMessageMapping(errorMapping);
        Result result = errorMapping.get(type, fields);
        if (result == null) {
            handleSqlExceptionInternal(supportedClass.cast(target), new ErrorsHelper(errors), type, fields);
        } else {
            ErrorsHelper errorsHelper = new ErrorsHelper(errors);
            if (result.getFields().length > 1) {
                // more than 1 field is involved in the exception, so we do not specify a single field in the error
                errorsHelper.reject(null, result.getErrorMessage());
            } else if (result.getFields().length == 1) {
                errorsHelper.reject(result.getFields()[0], result.getErrorMessage());
            }
        }
    }

    /**
     * handles sqlException, this method has higher precedence over <code>handleSqlExceptionInternal</code>.
     *
     * @param errorMapping
     *            (field, exceptionType) -> errorMessage
     */
    protected void sqlExceptionErrorMessageMapping(MultiKeyMap errorMapping) {}

    /**
     * this method is called if a repositry tried to persist <code>target</code>, but failed because of a sql exception,
     * which is not handled by <code>sqlExceptionErrorMessageMapping</code>.<br>
     * the sql exception details are stated in the parameters <code>field</code> and <code>type</code><br>
     * <br>
     * this method has lower precedence over <code>sqlExceptionErrorMapping</code>.
     *
     * @param target
     *            the object tried to persist.
     * @param field
     *            the field which caused an exception
     * @param type
     *            type of exception
     * @param errors
     *            to add errors
     */
    protected void handleSqlExceptionInternal(T target, ErrorsHelper errors, SqlExceptionType type, String... field) {}

    protected void validateInternal(T target, ErrorsHelper errors) {}

    public final Class<T> getSupportedClass() {
        return supportedClass;
    }
}

package edu.kit.tm.cm.participationservice.web.controller.utility.aspect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.Membership;
import edu.kit.tm.cm.participationservice.domain.model.PersistableEntity;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.web.controller.utility.checkers.PathChecker;
import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.NotFoundHttpException;

/**
 * This class uses Spring AOP to check parameters validity, if issues belong to the group and solutions belong to the
 * issue. <br>
 * this advice is called before @preauthorize.
 */
@Aspect
@Component
// TODO atm unauthenticated users can see if an entity exists, we might give @preauthorize precedence over this.
@Order(Ordered.HIGHEST_PRECEDENCE + 2)
public class ControllerParameterChecker {

    @Autowired
    private PathChecker pathChecker;

    /**
     * This method extracts entity parameters from their corresponding repository and checks if the references are
     * correct.
     *
     * @param joinPoint
     */
    @Before("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
    public void check(JoinPoint joinPoint) {
        Object[] arguments = joinPoint.getArgs();
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        Class<?>[] parameterTypes = method.getParameterTypes();
        Annotation[][] annotations = method.getParameterAnnotations();
        // assert parameterTypes.length == arguments.length == annotations.length;

        // these objects represent the objects from the corresponding repository depending on the given index of the uri
        GroupEntity group = null;
        Issue issue = null;
        Solution solution = null;
        Membership membership = null;

        // extract parameters
        for (int i = 0; i < parameterTypes.length && i < arguments.length; i++) {
            Class<?> curParameterType = parameterTypes[i];
            Object curArgument = arguments[i];
            Annotation[] curAnnotations = annotations[i];
            // assert curArgument.getClass().equals(curParameterType);

            // check if current parameter has @PathVariable Annotation
            if (curAnnotations.length == 0) {
                continue;
            }
            boolean containsPathVariableAnnotation = false;
            for (Annotation curAnnotation : curAnnotations) {
                if (curAnnotation instanceof PathVariable) {
                    containsPathVariableAnnotation = true;
                    break;
                }
            }
            if (!containsPathVariableAnnotation) {
                continue;
            }

            if (curArgument == null) {
                // @Pathvariable parameter is null
                throw new NotFoundHttpException();
            }

            // invalid type
            if (!(curArgument instanceof PersistableEntity)) {
                // @ pathvariable parameter is not an entity
                continue;
            }

            // new object/not found/invalid id (id is null if new object)
            if (((PersistableEntity) curArgument).getId() == null) {
                throw new NotFoundHttpException();
            }

            // Check if current parameter is an entity that has associations to other entities
            if (curParameterType.equals(GroupEntity.class)) {
                group = (GroupEntity) curArgument;
            } else if (curParameterType.equals(Issue.class)) {
                issue = (Issue) curArgument;
            } else if (curParameterType.equals(Solution.class)) {
                solution = (Solution) curArgument;
            } else if (curParameterType.equals(Membership.class)) {
                membership = (Membership) curArgument;
            }
        }

        // check if objects/parameters are valid using pathchecker
        if (group != null && issue != null) {
            pathChecker.checkPath(group, issue);
        }
        if (issue != null && solution != null) {
            pathChecker.checkPath(issue, solution);
        }
        if (group != null && membership != null) {
            pathChecker.checkPath(group, membership);
        }
    }
}

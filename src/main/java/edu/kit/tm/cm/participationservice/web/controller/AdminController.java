package edu.kit.tm.cm.participationservice.web.controller;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.service.AccountService;
import edu.kit.tm.cm.participationservice.service.GroupService;
import edu.kit.tm.cm.participationservice.web.messages.request.AccountStateChangeMessage;
import edu.kit.tm.cm.participationservice.web.validator.utility.ErrorsHelper;

public class AdminController extends AbstractController {

    private static final String PREFIX = "/admin";

    @Autowired
    private GroupService groupService;

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = PREFIX + "/accounts", method = RequestMethod.GET)
    @ResponseBody
    public Set<Account> searchAccountByAliasPrefix(
            @AuthenticationPrincipal Account account,
            @RequestParam(value = "aliasPrefix", required = false) String searchMessage) {
        if (searchMessage == null) {
            new ErrorsHelper(searchMessage, "aliasPrefix").reject(GlobalConstants.EM_SEARCH_INVALID_ALIAS_PREFIX)
                    .throwErrors();
        }
        return accountService.findAccountsByUserAliasPrefix(account.getUser(), searchMessage);
    }

    @RequestMapping(value = PREFIX + "/accounts/{accountID}", method = RequestMethod.PATCH)
    @ResponseBody
    public Account changeAccountState(
            @AuthenticationPrincipal Account executerAccount,
            @PathVariable("accountID") Account account,
            @Valid @RequestBody AccountStateChangeMessage accountStateChangeMessage) {
        return accountService.changeAccountState(executerAccount.getUser(), account,
                accountStateChangeMessage.getAccountState());
    }

    @RequestMapping(value = PREFIX + "/groups/{groupID}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteGroup(@AuthenticationPrincipal Account account,
            @PathVariable("groupID") GroupEntity group) {
        groupService.deleteGroup(account.getUser(), group);
    }

}

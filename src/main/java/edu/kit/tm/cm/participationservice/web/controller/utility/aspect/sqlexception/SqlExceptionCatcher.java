package edu.kit.tm.cm.participationservice.web.controller.utility.aspect.sqlexception;

import java.util.List;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.ErrorMessageException;
import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException;
import edu.kit.tm.cm.participationservice.web.controller.utility.exceptions.SqlException.SqlExceptionType;
import edu.kit.tm.cm.participationservice.web.validator.utility.ValidatorList;

/**
 * This class catches sql exceptions and refactors them to extract more information about the exception.<br>
 * highest precedence to be called before other advices.
 */
@Aspect
@Component
@Order(0)
public class SqlExceptionCatcher {

    @Autowired
    private ValidatorList validators;

    @Autowired
    private SqlExceptionParserList sqlExceptionParsers;

    @Around("execution(* edu.kit.tm.cm.participationservice.security.domain.repositories."
            + "*Repository.save*(..)) || "
            + "execution(* edu.kit.tm.cm.participationservice.domain.repositories."
            + "*Repository.save*(..))")
    public Object catchSqlExceptions(ProceedingJoinPoint joinPoint) throws Throwable {
        Object oldReturnValue = null;
        try {
            oldReturnValue = joinPoint.proceed();
        } catch (DataIntegrityViolationException e) {
            Object[] args = joinPoint.getArgs();
            // TODO we might ignore list (should not be thrown)
            // because save(List) utilizes save(Entity), at least simplejparepository does
            Object entity = args[0];
            if (entity == null || entity instanceof List) {
                throw e;
            }

            SqlException eNew = sqlExceptionParsers.parseException(entity, e);
            if (eNew == null) {
                // could not parse excepetion
                throw e;
            }

            Errors errors = eNew.getErrors();
            SqlExceptionType exceptionType = eNew.getExceptionType();
            String[] fields = eNew.getFields();
            // call all validators to check for exceptionhandling
            if (validators.supports(entity.getClass())) {
                validators.handleSqlException(entity, errors, exceptionType, fields);
                if (errors.hasErrors()) {
                    throw new ErrorMessageException(errors);
                }
            }
            throw new SqlException(fields, exceptionType, errors);
        }
        return oldReturnValue;
    }
}

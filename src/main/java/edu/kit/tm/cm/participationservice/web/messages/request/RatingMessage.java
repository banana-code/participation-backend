package edu.kit.tm.cm.participationservice.web.messages.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import edu.kit.tm.cm.participationservice.GlobalConstants;

public class RatingMessage {

    @NotNull(message = GlobalConstants.EM_RESISTANCE_CREATION_FAILED)
    @Min(value = GlobalConstants.MIN_RATING_VALUE, message = GlobalConstants.EM_RESISTANCE_INVALID)
    @Max(value = GlobalConstants.MAX_RATING_VALUE, message = GlobalConstants.EM_RESISTANCE_INVALID)
    private int rating;

    protected RatingMessage() {}

    public RatingMessage(int rating) {
        this.rating = rating;
    }

    public Integer getRating() {
        return rating;
    }
}

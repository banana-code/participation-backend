package edu.kit.tm.cm.participationservice.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(
        columnNames = { "comment_id", "created_by_id" },
        name = "unique__vote__comment__created_by_id"))
public class Vote extends AuditableEntity {

    public enum VoteType {
        UP, DOWN, NEUTRAL
    }

    private static final long serialVersionUID = 681833385817163621L;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private VoteType type;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Comment comment;

    protected Vote() {};

    public Vote(Comment comment, VoteType voteType) {
        this.comment = comment;
        this.type = voteType;
    }

    public void setType(VoteType voteType) {
        type = voteType;
    }

    public VoteType getType() {
        return type;
    }

}

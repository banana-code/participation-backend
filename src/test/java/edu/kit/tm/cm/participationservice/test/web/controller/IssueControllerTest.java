package edu.kit.tm.cm.participationservice.test.web.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;

import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.JSONBuilder;
import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.domain.model.Issue;

public class IssueControllerTest extends AbstractTest {

    private long GROUP_ID;

    @Before
    public void setup() throws Exception {
        createAccountAndLogin(TG.VALID_EMAIL, TG.VALID_PASSWORD,
                TG.VALID_ALIAS);

        // create group
        String json = JSONBuilder.create()
                .add("title", TG.VALID_GROUP_TITLE)
                .add("description", TG.VALID_GROUP_DESCRIPTION)
                .build();
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        GROUP_ID = groupRepository.findAll().get(0).getId();
        assertTrue("There should be one group", groupRepository.findAll().size() == 1);
    }

    @Test
    public void createIssues() throws Exception {
        String json;
        // create simple issue
        json = JSONBuilder.create()
                .add("title", "issue title")
                .build();
        mockMvc.perform(post("/api/groups/" + GROUP_ID + "/issues/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated())
                .andExpect(jsonPath(".title").value("issue title"));

        Issue issue = issueRepository.findAll().get(0);
        assertTrue("issue title was not saved correctly", issue.getTitle().equals("issue title"));
        assertTrue("issue startDate must be null", issue.getStartDate() == null);

        mockMvc.perform(delete("/api/groups/" + GROUP_ID + "/issues/" + issue.getId())).andExpect(status().isOk());
        assertTrue("issue is not deleted", issueRepository.count() == 0);
    }

    @Test
    public void invalidDate() throws Exception {
        String json;
        json = JSONBuilder.create()
                .add("title", "issue title")
                .add("startDate", DateTime.now().toString())
                .add("voteEndDate", DateTime.now().toString())
                .build();
        mockMvc.perform(post("/api/groups/" + GROUP_ID + "/issues/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
        assertTrue("issue is not deleted", issueRepository.count() == 0);

        json = JSONBuilder.create()
                .add("title", "issue title")
                .add("startDate", DateTime.now().toString())
                .add("solutionEndDate", DateTime.now().toString())
                .build();
        mockMvc.perform(post("/api/groups/" + GROUP_ID + "/issues/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
        assertTrue("issue is not deleted", issueRepository.count() == 0);

        json = JSONBuilder.create()
                .add("title", "issue title")
                .add("startDate", DateTime.now().toString())
                .build();
        mockMvc.perform(post("/api/groups/" + GROUP_ID + "/issues/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
        assertTrue("issue is not deleted", issueRepository.count() == 0);
    }

    @Test
    public void solutionEndDateAfterVoteEndDate() throws Exception {
        String json;
        json = JSONBuilder.create()
                .add("title", "issue title")
                .add("startDate", DateTime.now().toString())
                .add("solutionEndDate", DateTime.now().plusMinutes(2).toString())
                .add("voteEndDate", DateTime.now().plusMinutes(1).toString())
                .build();
        mockMvc.perform(post("/api/groups/" + GROUP_ID + "/issues/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
        assertTrue("issue is not deleted", issueRepository.count() == 0);
    }

    @Test
    public void validDates() throws Exception {
        String json;
        json = JSONBuilder.create()
                .add("title", "issue title")
                .add("startDate", DateTime.now().toString())
                .add("solutionEndDate", DateTime.now().plusMinutes(1).toString())
                .add("voteEndDate", DateTime.now().plusMinutes(1).toString())
                .build();
        mockMvc.perform(post("/api/groups/" + GROUP_ID + "/issues/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        assertTrue("issue is not deleted", issueRepository.count() == 1);
    }

    @Test
    public void invalidIssueChange() throws Exception {
        // Post issue
        String json;
        json = JSONBuilder.create()
                .add("title", TG.VALID_ISSUE_TITLE)
                .add("startDate", TG.VALID_ISSUE_START_DATE.toString())
                .add("solutionEndDate", TG.VALID_ISSUE_SOL_END_DATE.toString())
                .add("voteEndDate", TG.VALID_ISSUE_VOTE_END_DATE.toString())
                .build();
        mockMvc.perform(post("/api/groups/" + GROUP_ID + "/issues/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        assertTrue("issue is not deleted", issueRepository.count() == 1);
        // Change issue
        String newTitle = "Different title";
        json = JSONBuilder.create()
                .add("title", newTitle)
                .add("startDate", TG.INVALID_ISSUE_START_DATE.toString())
                .add("solutionEndDate", TG.INVALID_ISSUE_SOL_END_DATE.toString())
                .add("voteEndDate", TG.INVALID_ISSUE_VOTE_END_DATE.toString())
                .build();
        Issue issue = issueRepository.findAll().get(0);
        mockMvc.perform(patch("/api/groups/" + GROUP_ID + "/issues/" + issue.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
        assertTrue("there still is only one issue", issueRepository.count() == 1);
        issue = issueRepository.findAll().get(0);
        assertTrue("The issue should still have the old title", issue.getTitle().equals(TG.VALID_ISSUE_TITLE));
    }

    @Test
    public void validIssueChange() throws Exception {
        // Create valid issue
        String json = JSONBuilder.create()
                .add("title", TG.VALID_ISSUE_TITLE)
                .add("startDate", TG.VALID_ISSUE_START_DATE.toString())
                .add("solutionEndDate", TG.VALID_ISSUE_SOL_END_DATE.toString())
                .add("voteEndDate", TG.VALID_ISSUE_VOTE_END_DATE.toString())
                .build();
        mockMvc.perform(post("/api/groups/" + GROUP_ID + "/issues/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        assertTrue("issue is not deleted", issueRepository.count() == 1);
        Issue issue = issueRepository.findAll().get(0);
        // Make valid issue change
        String newTitle = "different title";
        json = JSONBuilder.create()
                .add("title", newTitle)
                .add("startDate", TG.VALID_ISSUE_START_DATE.toString())
                .add("solutionEndDate", TG.VALID_ISSUE_SOL_END_DATE.toString())
                .add("voteEndDate", TG.VALID_ISSUE_VOTE_END_DATE.toString())
                .build();
        mockMvc.perform(patch("/api/groups/" + GROUP_ID + "/issues/" + issue.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        issue = issueRepository.findAll().get(0);
        assertTrue("Issue should have a new title", issue.getTitle().equals(newTitle));
    }
}

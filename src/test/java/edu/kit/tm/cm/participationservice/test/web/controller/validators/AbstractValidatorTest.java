package edu.kit.tm.cm.participationservice.test.web.controller.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.web.validator.utility.ValidatorList;

public abstract class AbstractValidatorTest extends AbstractTest {

    @Autowired
    private ValidatorList validator;

    protected Errors validate(Object obj) {
        Errors errors = new BeanPropertyBindingResult(obj, "testSubject");
        ValidationUtils.invokeValidator(validator, obj, errors);
        return errors;
    }
}

package edu.kit.tm.cm.participationservice.test.errorMessage;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.MediaType;

import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.JSONBuilder;
import edu.kit.tm.cm.participationservice.TG;

public class AccountCreationErrorMessageTest extends AbstractTest {

    @Test
    public void mailTakenTest() throws Exception {
        createAccount(TG.VALID_EMAIL, TG.VALID_PASSWORD, TG.VALID_ALIAS);
        String json = JSONBuilder.create()
                .add("alias", TG.DIFFERENT_VALID_ALIAS)
                .add("email", TG.VALID_EMAIL)
                .add("password", TG.VALID_PASSWORD)
                .build();
        mockMvc.perform(post("/api/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors").isArray())
                .andExpect(
                        jsonPath("$.errors[0].message", Matchers.isOneOf(GlobalConstants.EM_REGISTER_EMAIL_TAKEN,
                                GlobalConstants.EM_ALIAS_TAKEN)));
    }

    @Test
    public void aliasTakenTest() throws Exception {
        createAccount(TG.VALID_EMAIL, TG.VALID_PASSWORD, TG.VALID_ALIAS);
        String json = JSONBuilder.create()
                .add("alias", TG.VALID_ALIAS)
                .add("email", TG.DIFFERENT_VALID_EMAIL)
                .add("password", TG.VALID_PASSWORD)
                .build();
        mockMvc.perform(post("/api/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors").isArray())
                .andExpect(
                        jsonPath("$.errors[0].message", Matchers.isOneOf(GlobalConstants.EM_REGISTER_EMAIL_TAKEN,
                                GlobalConstants.EM_ALIAS_TAKEN)));
    }
}

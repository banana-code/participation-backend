package edu.kit.tm.cm.participationservice;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Membership;
import edu.kit.tm.cm.participationservice.domain.model.Membership.MembershipState;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.repositories.CommentRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.GroupRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.IssueRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.MembershipRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.ResistanceRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.SolutionRepository;
import edu.kit.tm.cm.participationservice.domain.repositories.UserRepository;
import edu.kit.tm.cm.participationservice.security.config.SecurityBeans;
import edu.kit.tm.cm.participationservice.security.config.SecurityConfig;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.security.domain.model.Account.AccountState;
import edu.kit.tm.cm.participationservice.security.domain.repositories.AccountRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { Application.class, SecurityBeans.class,
        SecurityConfig.class })
public abstract class AbstractTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private FilterChainProxy filterChain;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    protected SolutionRepository solutionRepository;

    @Autowired
    protected IssueRepository issueRepository;

    @Autowired
    protected AccountRepository accountRepository;

    @Autowired
    protected ResistanceRepository resistanceRepository;

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected GroupRepository groupRepository;

    @Autowired
    protected MembershipRepository membershipRepository;

    @Autowired
    protected CommentRepository commentRepository;

    protected MockMvc mockMvc;
    protected MockMvc mockMvcSecurity;

    @Before
    public final void before() {
        mockMvcSecurity = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilters(filterChain).build();
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @After
    public final void deleteAll() {
        SecurityContextHolder.getContext().setAuthentication(null);

        resistanceRepository.deleteAll();
        solutionRepository.deleteAll();
        issueRepository.deleteAll();

        membershipRepository.deleteAll();
        groupRepository.deleteAll();

        accountRepository.deleteAll();
        userRepository.deleteAll();
    }

    /**
     * creates a valid account using parameter and uses the created account as logged in.
     *
     * @param email
     * @param password
     * @param alias
     * @return
     */
    protected final User createAccountAndLogin(String email, String password, String alias) {
        return createAccountAndLogin(email, password, alias, AccountState.VALID);
    }

    protected final User createAccountAndLogin(String email, String password, String alias, AccountState state) {
        User user = createAccount(email, password, alias, state);
        login(email, password);
        return user;
    }

    protected final User createAccount(String email, String password, String alias) {
        return createAccount(email, password, alias, AccountState.VALID);
    }

    protected final User createAccount(String email, String password, String alias, AccountState state) {
        return createAndGetAccount(email, password, alias, state).getUser();
    }

    protected final Account createAndGetAccount(String email, String password, String alias) {
        return createAndGetAccount(email, password, alias, AccountState.VALID);

    }

    protected final Account createAndGetAccount(String email, String password, String alias, AccountState state) {
        Account account = new Account(new User(alias), email, password);
        account.setState(state);
        return accountRepository.save(account);
    }

    protected final void login(String email, String password) {
        Authentication auth = new UsernamePasswordAuthenticationToken(email, password);
        auth = authenticationManager.authenticate(auth);
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    /**
     * uses TG.VALID_PASSWORD as password.
     *
     * @param email
     */
    protected final void login(String email) {
        login(email, TG.VALID_PASSWORD);
    }

    /**
     * uses TG.VALID_PASSWORD as password.
     *
     * @param account
     */
    protected final void login(Account account) {
        login(account.getEmail());
    }

    protected final GroupEntity createGroup(String title, String description, User owner) {
        GroupEntity group = groupRepository.save(new GroupEntity(title, description));
        membershipRepository.save(new Membership(group, MembershipState.OWNER, owner));
        return group;
    }

}

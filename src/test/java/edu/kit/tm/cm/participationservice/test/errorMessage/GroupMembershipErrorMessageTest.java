package edu.kit.tm.cm.participationservice.test.errorMessage;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.MediaType;

import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.GlobalConstants;
import edu.kit.tm.cm.participationservice.JSONBuilder;
import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Membership;

public class GroupMembershipErrorMessageTest extends AbstractTest {

    @Test
    public void removeLastOwner() throws Exception {
        createAccountAndLogin(TG.VALID_EMAIL, TG.VALID_PASSWORD, TG.VALID_ALIAS);
        String json = JSONBuilder.create()
                .add("title", TG.VALID_GROUP_TITLE)
                .add("description", TG.VALID_GROUP_DESCRIPTION)
                .build();
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json));
        GroupEntity group = groupRepository.findAll().get(0);
        Membership membership = membershipRepository.findAll().get(0);
        json = JSONBuilder.create()
                .add("membershipState", "MEMBER")
                .build();
        // change membership to member
        mockMvc.perform(patch("/api/groups/" + group.getId() + "/members/" + membership.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0].message", Matchers.is(GlobalConstants.EM_MEMBERSHIP_TOO_FEW_OWNER)));
        // delete membership
        mockMvc.perform(delete("/api/groups/" + group.getId() + "/members/" + membership.getId()))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0].message", Matchers.is(GlobalConstants.EM_MEMBERSHIP_TOO_FEW_OWNER)));
    }
}

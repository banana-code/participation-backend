package edu.kit.tm.cm.participationservice;

import java.util.ArrayList;
import java.util.List;

public final class JSONBuilder {
    private List<Pair> pairs;

    private JSONBuilder() {
        pairs = new ArrayList<Pair>();
    }

    public static JSONBuilder create() {
        return new JSONBuilder();
    }

    public JSONBuilder add(String a, String b) {
        if (a == null || b == null) {
            return this;
        }
        pairs.add(new StringPair(a, b));
        return this;
    }

    public JSONBuilder add(String a, long b) {
        if (a == null) {
            return this;
        }
        pairs.add(new NumberPair(a, b));
        return this;
    }

    public String build() {
        String result = "{";
        for (int i = 0; i < pairs.size(); i++) {
            result += pairs.get(i).format() + (i == pairs.size() - 1 ? "}" : ",");
        }
        return result;
    }

    private abstract class Pair {
        String a;

        Pair(String x) {
            a = x;
        }

        abstract String format();
    }

    private class StringPair extends Pair {
        String b;

        StringPair(String x, String y) {
            super(x);
            b = y;
        }

        @Override
        String format() {
            return "\"" + a + "\":" + "\"" + b + "\"";
        }
    }

    private class NumberPair extends Pair {
        long b;

        NumberPair(String x, long y) {
            super(x);
            b = y;
        }

        @Override
        String format() {
            return "\"" + a + "\":" + b;
        }
    }
}

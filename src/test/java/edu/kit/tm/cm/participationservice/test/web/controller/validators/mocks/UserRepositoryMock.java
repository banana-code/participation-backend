package edu.kit.tm.cm.participationservice.test.web.controller.validators.mocks;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.repositories.UserRepository;

public class UserRepositoryMock implements UserRepository {

    private List<User> users = new ArrayList<User>();

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public void deleteInBatch(Iterable<User> arg0) {

    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public List<User> findAll(Sort arg0) {
        return null;
    }

    @Override
    public List<User> findAll(Iterable<Long> arg0) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public User getOne(Long arg0) {
        return null;
    }

    @Override
    public <S extends User> List<S> save(Iterable<S> arg0) {
        return null;
    }

    @Override
    public <S extends User> S saveAndFlush(S arg0) {
        return null;
    }

    @Override
    public Page<User> findAll(Pageable arg0) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(Long arg0) {

    }

    @Override
    public void delete(Iterable<? extends User> arg0) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public boolean exists(Long arg0) {
        return false;
    }

    @Override
    public User findOne(Long arg0) {
        return null;
    }

    @Override
    public <S extends User> S save(S arg0) {
        users.add(arg0);
        return arg0;
    }

    @Override
    public User findByAlias(String alias) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getAlias().equals(alias)) {
                return users.get(i);
            }
        }
        return null;
    }

    @Override
    public void delete(User entity) {

    }

}

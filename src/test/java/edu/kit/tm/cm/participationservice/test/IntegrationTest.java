package edu.kit.tm.cm.participationservice.test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.IssueBuilder;
import edu.kit.tm.cm.participationservice.domain.model.Resistance;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;

public class IntegrationTest extends AbstractTest {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void testUser() {
        final List<User> users = userRepository.findAll();
        assertThat(users.size(), is(1));
        final User user = users.get(0);
        final Account details = user.getAccount();

        assertThat(user.getAlias(), is("john"));
        assertThat(details.getEmail(), is("john@example.com"));
        assertNotNull(user.getRegistrationDate());
        assertNotNull(user.getId());

        assertEquals(details.getUser(), user);
        assertTrue(passwordEncoder.matches(user.getAlias(), details.getPassword()));
        assertThat(details.getUsername(), is("john@example.com"));
        assertNotNull(details.getId());
    }

    @Test
    public void testIssue() {
        final List<Issue> issues = issueRepository.findAll();
        assertThat(issues.size(), is(1));
        final Issue issue = issues.get(0);

        assertThat(issue.getTitle(), is("Kaffe am AKK"));
        assertThat(issue.getCurrentSolution(), is("Ettli"));
        assertThat(issue.getDescription(), is("Wir brauchen besseren Kaffee am AKK"));
        assertNull(issue.getQuestion());
        assertThat(issue.getSolutions().size(), is(1));
        assertEquals(issue.getSolutions().toArray()[0], solutionRepository.findAll().get(0));
        assertEquals(issue.getCreatedBy(), userRepository.findAll().get(0));
        assertEquals(issue.getLastModifiedBy(), userRepository.findAll().get(0));
        assertNotNull(issue.getCreatedDate());
        assertNotNull(issue.getLastModifiedDate());
        assertNotNull(issue.getId());
    }

    @Test
    public void testSolution() {
        final List<Solution> solutions = solutionRepository.findAll();
        assertThat(solutions.size(), is(1));
        final Solution solution = solutions.get(0);

        assertThat(solution.getTitle(), is("Arabica"));
        assertThat(solution.getDescription(), is("Beste Kaffesorte südlich vom Nordpol"));
        assertThat(solution.getResistance(), is(10f));
        assertThat(solution.getVoteCount(), is(1));
        assertEquals(solution.getIssue(), issueRepository.findAll().get(0));
        assertEquals(solution.getCreatedBy(), userRepository.findAll().get(0));
        assertEquals(solution.getLastModifiedBy(), userRepository.findAll().get(0));
        assertNotNull(solution.getCreatedDate());
        assertNotNull(solution.getLastModifiedDate());
        assertNotNull(solution.getId());
    }

    @Test
    public void testResistance() {
        final List<Resistance> resistances = resistanceRepository.findAll();
        assertThat(resistances.size(), is(1));
        final Resistance resistance = resistances.get(0);

        assertThat(resistance.getRating(), is(10));
        assertEquals(resistance.getSolution(), solutionRepository.findAll().get(0));
        assertEquals(resistance.getCreatedBy(), userRepository.findAll().get(0));
        assertEquals(resistance.getLastModifiedBy(), userRepository.findAll().get(0));
        assertNotNull(resistance.getCreatedDate());
        assertNotNull(resistance.getLastModifiedDate());
        assertNotNull(resistance.getId());
    }

    @Before
    public void setUp() {

        createAccountAndLogin("john@example.com", "john", "john");

        // setting up a group
        GroupEntity group = new GroupEntity("TestGroup", "testDescription");

        groupRepository.save(group);

        // setting up an issue
        final Issue issue = IssueBuilder.create()
                .setTitle("Kaffe am AKK")
                .setCurrentSolution("Ettli")
                .setStartDate(DateTime.parse("2014-04-10"))
                .setSolutionEndDate(DateTime.parse("2014-04-11"))
                .setVoteEndDate(DateTime.parse("2014-04-11"))
                .setDescription("Wir brauchen besseren Kaffee am AKK")
                .setGroup(group)
                .build();

        issueRepository.save(issue);

        // editing the zeroOption
        final Solution zeroOption = issue.getZeroOption();
        zeroOption.setTitle("Arabica");
        zeroOption.setDescription("Beste Kaffesorte südlich vom Nordpol");

        solutionRepository.save(zeroOption);

        // setting up a resistance
        resistanceRepository.save(new Resistance(zeroOption, 10));
    }
}

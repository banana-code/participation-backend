package edu.kit.tm.cm.participationservice.test.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.TG;

public class UserControllerTest extends AbstractTest {

    private final static String EMAIL_1 = "abc@example.com";
    private final static String EMAIL_2 = "def@example.com";
    private final static String EMAIL_3 = "ghi@example.com";
    private final static String EMAIL_4 = "jkl@example.com";
    private final static String PREFIX = "abc";
    private final static String NOT_PREFIX = "cba";
    private final static String PREFIX_FREE_ALIAS = "xxx";
    private final static String ALIAS_WITH_PREFIX = "abcd";

    @Before
    public void setUp() throws Exception {
        // Create accounts
        createAccount(EMAIL_1, TG.VALID_PASSWORD, PREFIX);
        createAccount(EMAIL_2, TG.VALID_PASSWORD, NOT_PREFIX);
        createAccount(EMAIL_3, TG.VALID_PASSWORD, PREFIX_FREE_ALIAS);
        createAccount(EMAIL_4, TG.VALID_PASSWORD, ALIAS_WITH_PREFIX);
        createAccountAndLogin(TG.VALID_EMAIL, TG.VALID_PASSWORD, TG.VALID_ALIAS);
    }

    @Test
    public void searchByAliasTest() throws Exception {
        login(TG.VALID_EMAIL, TG.VALID_PASSWORD);
        mockMvc.perform(get("/api/users?aliasPrefix=" + PREFIX)
                .accept(org.springframework.http.MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect((jsonPath("$", Matchers.hasSize(2))))
                // return value is a hashset therefore it is guaranteed that the alias is different in both json 
                // because alias is unique
                .andExpect(jsonPath("$[0].alias", Matchers.isOneOf("abc", "abcd")))
                .andExpect(jsonPath("$[1].alias", Matchers.isOneOf("abc", "abcd")));
    }
}

package edu.kit.tm.cm.participationservice.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;

import edu.kit.tm.cm.participationservice.AbstractTest;

public class AuthorizationTest extends AbstractTest {

    @Test
    public void authorizationTest() throws Exception {
        mockMvcSecurity.perform(get("/api/groups")).andExpect(status().isForbidden());
        mockMvcSecurity.perform(get("/api/groups/1")).andExpect(status().isForbidden());
        mockMvcSecurity.perform(get("/api/groups/1/issues")).andExpect(status().isForbidden());
        mockMvcSecurity.perform(get("/api/groups/1/issues/1")).andExpect(status().isForbidden());
        mockMvcSecurity.perform(get("/api/groups/1/issues/1/solutions")).andExpect(status().isForbidden());
        mockMvcSecurity.perform(get("/api/groups/1/issues/1/solutions/1")).andExpect(status().isForbidden());
        mockMvcSecurity.perform(get("/api/me/")).andExpect(status().isForbidden());
        mockMvcSecurity.perform(get("/api/me/password")).andExpect(status().isForbidden());
        mockMvcSecurity.perform(get("/api/me/alias")).andExpect(status().isForbidden());
    }
}

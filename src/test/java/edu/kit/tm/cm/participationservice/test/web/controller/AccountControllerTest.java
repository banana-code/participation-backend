package edu.kit.tm.cm.participationservice.test.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.http.MediaType;

import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.JSONBuilder;
import edu.kit.tm.cm.participationservice.TG;

public class AccountControllerTest extends AbstractTest {

    @Test
    public void testRegister() throws Exception {
        String json = JSONBuilder.create()
                .add("alias", TG.VALID_ALIAS)
                .add("email", TG.VALID_EMAIL)
                .add("password", TG.VALID_PASSWORD)
                .build();
        mockMvc.perform(post("/api/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        assertNotNull(accountRepository.findByEmail(TG.VALID_EMAIL));
    }
}

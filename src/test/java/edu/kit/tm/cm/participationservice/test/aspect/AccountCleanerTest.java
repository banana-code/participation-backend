package edu.kit.tm.cm.participationservice.test.aspect;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.joda.time.DateTime;
import org.junit.Test;

import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.security.domain.model.AuthorizationKey;

public class AccountCleanerTest extends AbstractTest {

    @Test
    public void testAccountExpiration() {
        accountRepository.save(new Account(new User(TG.VALID_ALIAS), TG.VALID_EMAIL, TG.VALID_PASSWORD, new AuthorizationKey(
                DateTime.now().minus(1000L))));
        assertNull("expired account should be deleted", accountRepository.findByEmail(TG.VALID_EMAIL));
        assertNull("expired account should be deleted", userRepository.findByAlias(TG.VALID_ALIAS));

        accountRepository.save(new Account(new User(TG.VALID_ALIAS), TG.VALID_EMAIL, TG.VALID_PASSWORD, new AuthorizationKey(
                DateTime.now().minus(1000L))));
        assertNull("expired account should be deleted", userRepository.findByAlias(TG.VALID_ALIAS));
        assertNull("expired account should be deleted", accountRepository.findByEmail(TG.VALID_EMAIL));

        accountRepository.save(new Account(new User(TG.VALID_ALIAS), TG.VALID_EMAIL, TG.VALID_PASSWORD,
                new AuthorizationKey(DateTime.now().plusDays(7))));
        assertNotNull("expired account should not be deleted", userRepository.findByAlias(TG.VALID_ALIAS));
        deleteAccountFully();

        accountRepository.save(new Account(new User(TG.VALID_ALIAS), TG.VALID_EMAIL, TG.VALID_PASSWORD,
                new AuthorizationKey(DateTime.now().plusDays(7))));
        assertNotNull("expired account should not be deleted", userRepository.findByAlias(TG.VALID_ALIAS));
        deleteAccountFully();
    }

    private void deleteAccountFully() {
        Account account = accountRepository.findByEmail(TG.VALID_EMAIL);
        accountRepository.delete(account);
        userRepository.delete(account.getUser());
    }

}

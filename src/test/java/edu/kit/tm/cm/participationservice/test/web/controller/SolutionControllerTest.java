package edu.kit.tm.cm.participationservice.test.web.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;

import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.JSONBuilder;
import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.IssueBuilder;
import edu.kit.tm.cm.participationservice.domain.model.Membership;
import edu.kit.tm.cm.participationservice.domain.model.Membership.MembershipState;
import edu.kit.tm.cm.participationservice.domain.model.Resistance;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;

public class SolutionControllerTest extends AbstractTest {
    private GroupEntity group;
    private Issue issue;
    private Account creator;
    private Account member;

    @Before
    public void createEnvironment() throws Exception {
        member = createAndGetAccount(TG.VALID_EMAIL, TG.VALID_PASSWORD, TG.VALID_ALIAS);
        creator = createAndGetAccount(TG.DIFFERENT_VALID_EMAIL, TG.VALID_PASSWORD, TG.DIFFERENT_VALID_ALIAS);

        login(creator);

        group = createGroup(TG.VALID_GROUP_TITLE, TG.VALID_GROUP_DESCRIPTION, creator.getUser());
        membershipRepository.save(new Membership(group, MembershipState.MEMBER, member.getUser()));

        issue = IssueBuilder.create()
                .setTitle(TG.VALID_ISSUE_TITLE)
                .setQuestion(TG.VALID_ISSUE_QUESTION)
                .setDescription(TG.VALID_ISSUE_DESCRIPTION)
                .setCurrentSolution(TG.VALID_ISSUE_CURRENT_SOLUTION)
                .setSolutionEndDate(TG.VALID_ISSUE_SOL_END_DATE)
                .setVoteEndDate(TG.VALID_ISSUE_VOTE_END_DATE)
                .setStartDate(TG.VALID_ISSUE_START_DATE)
                .build();
        issue.setGroup(group);
        issue = issueRepository.save(issue);

        // Default user for tests below
        login(member);
    }

    @Test
    public void creationTest() throws Exception {
        // add solution
        String json = JSONBuilder.create()
                .add("title", TG.VALID_SOLUTION_TITLE)
                .add("description", TG.VALID_SOLUTION_DESCRIPTION)
                .build();
        mockMvc.perform(post(getSolutionsUrl())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());

        assertEquals("There should be two solutions (zeroOption and the new one)", 2, solutionRepository.count());
    }

    @Test
    public void deleteParentIssueTest() throws Exception {
        // delete issue
        login(creator);
        mockMvc.perform(delete("/api/groups/" + group.getId() + "/issues/" + issue.getId()))
                .andExpect(status().is2xxSuccessful());

        // solutions must also be deleted
        mockMvc.perform(get(getSolutionsUrl()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void changeRatingTest() throws Exception {
        Solution solution = createValidSolution();

        // change rating
        int newRating = TG.VALID_RATING;
        String json = JSONBuilder.create().add("rating", newRating).build();
        mockMvc.perform(post(getSolutionsUrl() + solution.getId() + "/resistances")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());

        Resistance resistance = resistanceRepository.findByCreatedByAndSolution(member.getUser(), solution);
        assertEquals("Resistance should have been updated", newRating, resistance.getRating());
    }

    @Test
    public void validSolutionDeletion() throws Exception {
        Solution solution = createValidSolution();
        assertEquals("There should be two solutions (zeroOption and the new one)", 2, solutionRepository.count());

        // Delete solution
        login(creator);
        mockMvc.perform(delete(getSolutionsUrl() + solution.getId()))
                .andExpect(status().isOk());
        assertEquals("There should only be the zeroOption", 1, solutionRepository.count());
    }

    @Test
    public void invalidSolutionDeletion() throws Exception {
        Solution solution = createValidSolution();
        assertEquals("There should be two solutions (zeroOption and the new one)", 2, solutionRepository.count());

        // Try to delete a solution as member
        mockMvc.perform(delete(getSolutionsUrl() + solution.getId()))
                .andExpect(status().isForbidden());
        assertEquals("There should still be two solutions", 2, solutionRepository.count());
    }

    @Test
    public void invalidSolutionChange() throws Exception {
        Solution solution = createValidSolution();

        // Change solution
        String json = JSONBuilder.create().add("title", TG.INVALID_SOLUTION_TITLE).build();
        mockMvc.perform(patch(getSolutionsUrl() + solution.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
        Solution updatedSolution = solutionRepository.findOne(solution.getId());
        assertEquals("Invalid title must not be saved", solution.getTitle(), updatedSolution.getTitle());
    }

    @Test
    public void validSolutionChange() throws Exception {
        // User creates solution
        Solution solution = createValidSolution();

        // Owner changes solution
        login(creator);
        String newTitle = "NEW UNUSED TITLE";
        String json = JSONBuilder.create().add("title", newTitle).build();
        mockMvc.perform(patch(getSolutionsUrl() + solution.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        Solution updatedSolution = solutionRepository.findOne(solution.getId());
        assertEquals("New title should be saved", newTitle, updatedSolution.getTitle());
    }

    private Solution createValidSolution() {
        Solution solution = new Solution(TG.VALID_SOLUTION_TITLE, TG.VALID_SOLUTION_DESCRIPTION, issue);
        return solutionRepository.save(solution);
    }

    private String getSolutionsUrl() {
        return "/api/groups/" + group.getId() + "/issues/" + issue.getId() + "/solutions/";
    }
}

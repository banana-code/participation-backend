package edu.kit.tm.cm.participationservice.test.web.controller.validators;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.validation.Errors;

import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.web.messages.request.GroupCreationMessage;

public class GroupEntityValidatorTest extends AbstractValidatorTest {

    @Test
    public void validGroupTest() {
        Errors errors = validate(new GroupCreationMessage(TG.VALID_GROUP_TITLE,
                TG.VALID_GROUP_DESCRIPTION));
        assertFalse("Valid group must not be rejected", errors.hasErrors());
    }

    @Test
    public void invalidGroupTitleTest() {
        Errors errors = validate(new GroupCreationMessage(TG.INVALID_GROUP_TITLE,
                TG.VALID_GROUP_DESCRIPTION));
        assertTrue("Invalid group name must be rejected", errors.hasErrors());
    }

    @Test
    public void invalidGroupDescriptionTest() {
        Errors errors = validate(new GroupCreationMessage(TG.VALID_GROUP_TITLE,
                TG.INVALID_GROUP_DESCRIPTION));
        assertFalse("Invalid group description must be rejected", errors.hasErrors());
    }

    @Test
    public void tooLongGroupTitleTest() {
        Errors errors = validate(new GroupCreationMessage(TG.TOO_LONG_GROUP_TITLE,
                TG.VALID_GROUP_DESCRIPTION));
        assertTrue("Invalid group description must be rejected", errors.hasErrors());
    }

    @Test
    public void tooLongGroupDescriptionTest() {
        Errors errors = validate(new GroupCreationMessage(TG.VALID_GROUP_TITLE,
                TG.TOO_LONG_GROUP_DESCRIPTION));
        assertTrue("Invalid group description must be rejected", errors.hasErrors());
    }
}

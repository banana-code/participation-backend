package edu.kit.tm.cm.participationservice.test.web.controller.validators.mocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.domain.repositories.UserRepository;
import edu.kit.tm.cm.participationservice.security.domain.model.Account;
import edu.kit.tm.cm.participationservice.security.domain.repositories.AccountRepository;

public class AccountRepositoryMock implements AccountRepository {

    private List<Account> accounts = new ArrayList<Account>();
    private UserRepository userRepository;

    public AccountRepositoryMock(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public void deleteInBatch(Iterable<Account> arg0) {

    }

    @Override
    public List<Account> findAll() {
        return null;
    }

    @Override
    public List<Account> findAll(Sort arg0) {
        return null;
    }

    @Override
    public List<Account> findAll(Iterable<Long> arg0) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Account> List<S> save(Iterable<S> arg0) {
        return null;
    }

    @Override
    public <S extends Account> S saveAndFlush(S arg0) {
        return null;
    }

    @Override
    public Page<Account> findAll(Pageable arg0) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(Long arg0) {

    }

    @Override
    public void delete(Account arg0) {

    }

    @Override
    public void delete(Iterable<? extends Account> arg0) {}

    @Override
    public void deleteAll() {

    }

    @Override
    public boolean exists(Long arg0) {
        return false;
    }

    @Override
    public Account findOne(Long arg0) {
        return null;
    }

    @Override
    public <S extends Account> S save(S arg0) {
        accounts.add(arg0);
        userRepository.save(arg0.getUser());
        return arg0;
    }

    @Override
    public Account findByEmail(String email) {
        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getEmail().equals(email)) {
                return accounts.get(i);
            }
        }
        return null;
    }

    @Override
    public Account getOne(Long id) {
        return null;
    }

    @Override
    public Account findUnconfirmedAccountByRegistrationKey(String key) {
        return null;
    }

    @Override
    public Account findAccountByPasswordResetKey(String key) {
        return null;
    }

    @Override
    public Set<Account> findAccountsByUserAliasPrefix(String aliasPrefix) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Set<User> findUsersByUserAliasPrefix(String aliasPrefix) {
        // TODO Auto-generated method stub
        return null;
    }

}

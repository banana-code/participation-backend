package edu.kit.tm.cm.participationservice.test.web.controller;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;

import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.JSONBuilder;
import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Membership;
import edu.kit.tm.cm.participationservice.domain.model.Membership.MembershipState;
import edu.kit.tm.cm.participationservice.domain.model.User;

public class MembershipControllerTest extends AbstractTest {
    private User creator;
    private User user;
    private GroupEntity newGroup;

    @Before
    public void createEnvironment() throws Exception {
        // create 2 accounts
        createAccount(TG.VALID_EMAIL, TG.VALID_PASSWORD, TG.VALID_ALIAS);
        user = userRepository.findByAlias(TG.VALID_ALIAS);
        createAccount(TG.DIFFERENT_VALID_EMAIL, TG.VALID_PASSWORD,
                TG.DIFFERENT_VALID_ALIAS);
        creator = userRepository.findByAlias(TG.DIFFERENT_VALID_ALIAS);
        // create group
        newGroup = createGroup(TG.VALID_GROUP_TITLE, TG.VALID_GROUP_DESCRIPTION, creator);
    }

    @Test
    public void listMembers() throws Exception {
        login(TG.DIFFERENT_VALID_EMAIL, TG.VALID_PASSWORD);
        mockMvc.perform(get("/api/groups/" + newGroup.getId() + "/members"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void inviteMember() throws Exception {
        login(TG.DIFFERENT_VALID_EMAIL, TG.VALID_PASSWORD);
        String json = JSONBuilder.create()
                .add("userId", String.valueOf(user.getId()))
                .build();
        mockMvc.perform(post("/api/groups/" + newGroup.getId() + "/members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        assertTrue("User should be invited",
                membershipRepository.findByUserAndGroup(user, newGroup).getState() == MembershipState.INVITED);
    }

    @Test
    public void acceptInvite() throws Exception {
        // Creator invites user
        login(TG.DIFFERENT_VALID_EMAIL, TG.VALID_PASSWORD);
        String json = JSONBuilder.create()
                .add("userId", String.valueOf(user.getId()))
                .build();
        mockMvc.perform(post("/api/groups/" + newGroup.getId() + "/members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        // User accepts invite
        login(TG.VALID_EMAIL, TG.VALID_PASSWORD);
        Membership membership = membershipRepository.findByUserAndGroup(user, newGroup);
        json = JSONBuilder.create()
                .add("membershipState", "MEMBER")
                .build();
        mockMvc.perform(patch("/api/groups/" + newGroup.getId() + "/members/" + membership.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        assertTrue("There should be 2 members in the group", groupRepository.findByTitle(newGroup.getTitle())
                .getMemberCount() == 2);
    }

    @Test
    public void refuseInvite() throws Exception {
        // Creator invites user
        login(TG.DIFFERENT_VALID_EMAIL, TG.VALID_PASSWORD);
        String json = JSONBuilder.create()
                .add("userId", String.valueOf(user.getId()))
                .build();
        mockMvc.perform(post("/api/groups/" + newGroup.getId() + "/members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        // User refuses invite
        login(TG.VALID_EMAIL, TG.VALID_PASSWORD);
        Membership membership = membershipRepository.findByUserAndGroup(user, newGroup);
        json = JSONBuilder.create()
                .add("membershipState", "MEMBER")
                .build();
        mockMvc.perform(delete("/api/groups/" + newGroup.getId() + "/members/" + membership.getId()))
                .andExpect(status().isOk());
        assertTrue("There should be one member in the group", groupRepository.findByTitle(newGroup.getTitle())
                .getMemberCount() == 1);
    }

    @Test
    public void invalidLeaveGroup() throws Exception {
        login(TG.DIFFERENT_VALID_EMAIL, TG.VALID_PASSWORD);
        Membership membership = membershipRepository.findByUserAndGroup(creator, newGroup);
        mockMvc.perform(delete("/api/groups/" + newGroup.getId() + "/members/" + membership.getId()))
                .andExpect(status().isBadRequest());
        assertTrue("Group should have at least 1 owner", membershipRepository.findOwnerByGroup(newGroup).size() >= 1);
    }

    @Test
    public void validLeaveGroup() throws Exception {
        Membership membership = new Membership(newGroup, MembershipState.MEMBER, user);
        membershipRepository.save(membership);
        assertTrue("User should be member", membershipRepository.findByUserAndGroup(user, newGroup) != null);
        login(TG.VALID_EMAIL, TG.VALID_PASSWORD);
        mockMvc.perform(delete("/api/groups/" + newGroup.getId() + "/members/" + membership.getId()))
                .andExpect(status().isOk());
        assertTrue("User should not be member anymore", membershipRepository.findByUserAndGroup(user, newGroup) == null);
    }

    @Test
    public void degradeOnlyOwner() throws Exception {
        login(TG.DIFFERENT_VALID_EMAIL, TG.VALID_PASSWORD);
        Membership membership = membershipRepository.findByUserAndGroup(creator, newGroup);
        String json = JSONBuilder.create()
                .add("membershipState", "MEMBER")
                .build();
        mockMvc.perform(patch("/api/groups/" + newGroup.getId() + "/members/" + membership.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
        membership = membershipRepository.findByUserAndGroup(user, newGroup);
        assertTrue("Only owner should still be owner", membershipRepository.findByUserAndGroup(creator, newGroup)
                .getState() == MembershipState.OWNER);
    }

    @Test
    public void requestMembership() throws Exception {
        login(TG.VALID_EMAIL, TG.VALID_PASSWORD);
        String json = JSONBuilder.create()
                .add("userId", String.valueOf(user.getId()))
                .build();
        mockMvc.perform(post("/api/groups/" + newGroup.getId() + "/members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        Membership membership = membershipRepository.findByUserAndGroup(user, newGroup);
        assertTrue("Membership should be REQUESTED", membership.getState() == MembershipState.REQUESTED);
    }

    @Test
    public void inviteAlthoughInvited() throws Exception {
        Membership membership = new Membership(newGroup, MembershipState.INVITED, user);
        membershipRepository.save(membership);
        login(TG.DIFFERENT_VALID_EMAIL, TG.VALID_PASSWORD);
        String json = JSONBuilder.create()
                .add("userId", String.valueOf(user.getId()))
                .build();
        mockMvc.perform(post("/api/groups/" + newGroup.getId() + "/members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
        assertTrue("User should still be invited",
                membershipRepository.findByUserAndGroup(user, newGroup).getState() == MembershipState.INVITED);
    }

    @Test
    public void inviteAlthoughMember() throws Exception {
        Membership membership = new Membership(newGroup, MembershipState.MEMBER, user);
        membershipRepository.save(membership);
        login(TG.DIFFERENT_VALID_EMAIL, TG.VALID_PASSWORD);
        String json = JSONBuilder.create()
                .add("userId", String.valueOf(user.getId()))
                .build();
        mockMvc.perform(post("/api/groups/" + newGroup.getId() + "/members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
        assertTrue("User should still be member",
                membershipRepository.findByUserAndGroup(user, newGroup).getState() == MembershipState.MEMBER);
    }

    @Test
    public void inviteAlthoughMembershipRequested() throws Exception {
        Membership membership = new Membership(newGroup, MembershipState.REQUESTED, user);
        membershipRepository.save(membership);
        login(TG.DIFFERENT_VALID_EMAIL, TG.VALID_PASSWORD);
        String json = JSONBuilder.create()
                .add("userId", String.valueOf(user.getId()))
                .build();
        mockMvc.perform(post("/api/groups/" + newGroup.getId() + "/members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        assertTrue("request and invite simultaneously changes to member directly",
                membershipRepository.findByUserAndGroup(user, newGroup).getState() == MembershipState.MEMBER);
    }

    @Test
    public void refuseMembership() throws Exception {
        // User requests membership
        login(TG.VALID_EMAIL, TG.VALID_PASSWORD);
        String json = JSONBuilder.create()
                .add("userId", String.valueOf(user.getId()))
                .build();
        mockMvc.perform(post("/api/groups/" + newGroup.getId() + "/members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        Membership membership = membershipRepository.findByUserAndGroup(user, newGroup);
        assertTrue("Membership should be REQUESTED", membership.getState() == MembershipState.REQUESTED);

        // refuse request
        login(TG.DIFFERENT_VALID_EMAIL, TG.VALID_PASSWORD);
        mockMvc.perform(delete("/api/groups/" + newGroup.getId() + "/members/" + membership.getId()))
                .andExpect(status().isOk());
        membership = membershipRepository.findByUserAndGroup(user, newGroup);
        assertTrue("Membership should no longer exist", membership == null);
    }

    @Test
    public void acceptMembership() throws Exception {
        // User requests membership
        login(TG.VALID_EMAIL, TG.VALID_PASSWORD);
        String json = JSONBuilder.create()
                .add("userId", String.valueOf(user.getId()))
                .build();
        mockMvc.perform(post("/api/groups/" + newGroup.getId() + "/members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        Membership membership = membershipRepository.findByUserAndGroup(user, newGroup);
        assertTrue("Membership should be REQUESTED", membership.getState() == MembershipState.REQUESTED);
        // Creator accepts membership
        login(TG.DIFFERENT_VALID_EMAIL, TG.VALID_PASSWORD);
        json = JSONBuilder.create()
                .add("membershipState", "MEMBER")
                .build();
        mockMvc.perform(patch("/api/groups/" + newGroup.getId() + "/members/" + membership.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        membership = membershipRepository.findByUserAndGroup(user, newGroup);
        assertTrue("User should be Member now", membership.getState() == MembershipState.MEMBER);
    }
}

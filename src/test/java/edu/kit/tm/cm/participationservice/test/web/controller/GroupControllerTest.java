package edu.kit.tm.cm.participationservice.test.web.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.springframework.http.MediaType;

import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.JSONBuilder;
import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;

public class GroupControllerTest extends AbstractTest {

    @Test
    public void createGroup() throws Exception {
        createAccountAndLogin(TG.VALID_EMAIL, TG.VALID_PASSWORD,
                TG.VALID_ALIAS);
        String json = JSONBuilder.create()
                .add("title", TG.VALID_GROUP_TITLE)
                .add("description", TG.VALID_GROUP_DESCRIPTION)
                .build();
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        assertTrue("There should be one group", groupRepository.findAll().size() == 1);
    }

    @Test
    public void createAndDeleteGroup() throws Exception {
        createAccountAndLogin(TG.VALID_EMAIL, TG.VALID_PASSWORD,
                TG.VALID_ALIAS);
        String json = JSONBuilder.create()
                .add("title", TG.VALID_GROUP_TITLE)
                .add("description", TG.VALID_GROUP_DESCRIPTION)
                .build();
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        assertTrue("There should be one group", groupRepository.findAll().size() == 1);
        final long GROUP_ID = groupRepository.findAll().get(0).getId();
        mockMvc.perform(delete("/api/groups/" + GROUP_ID))
                .andExpect(status().isOk());
        assertTrue("There should be one group", groupRepository.findAll().isEmpty());
    }

    @Test
    public void createGroupTwice() throws Exception {
        createAccountAndLogin(TG.VALID_EMAIL, TG.VALID_PASSWORD,
                TG.VALID_ALIAS);
        String json = JSONBuilder.create()
                .add("title", TG.VALID_GROUP_TITLE)
                .add("description", TG.VALID_GROUP_DESCRIPTION)
                .build();
        System.out.println(json);
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
        assertTrue("There should be one group", groupRepository.findAll().size() == 1);
    }

    @Test
    public void createInvalidGroup() throws Exception {
        createAccountAndLogin(TG.VALID_EMAIL, TG.VALID_PASSWORD,
                TG.VALID_ALIAS);
        String json = JSONBuilder.create()
                .add("title", TG.INVALID_GROUP_TITLE)
                .add("description", TG.VALID_GROUP_DESCRIPTION)
                .build();
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void validGroupChange() throws Exception {
        // Create valid group
        createAccountAndLogin(TG.VALID_EMAIL, TG.VALID_PASSWORD,
                TG.VALID_ALIAS);
        String json = JSONBuilder.create()
                .add("title", TG.VALID_GROUP_TITLE)
                .add("description", TG.VALID_GROUP_DESCRIPTION)
                .build();
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        assertTrue("There should be one group", groupRepository.findAll().size() == 1);
        // Make valid group change
        GroupEntity group = groupRepository.findAll().get(0);
        String newTitle = "new Title";
        String newDescription = "new Description";
        json = JSONBuilder.create()
                .add("title", newTitle)
                .add("description", newDescription)
                .build();
        mockMvc.perform(patch("/api/groups/" + group.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        group = groupRepository.findByTitle(newTitle);
        assertTrue("Group should have the new description",
                group != null && group.getDescription().equals(newDescription));
    }

    @Test
    public void invalidGroupChange() throws Exception {
        // Create valid group
        createAccountAndLogin(TG.VALID_EMAIL, TG.VALID_PASSWORD,
                TG.VALID_ALIAS);
        String json = JSONBuilder.create()
                .add("title", TG.VALID_GROUP_TITLE)
                .add("description", TG.VALID_GROUP_DESCRIPTION)
                .build();
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        assertTrue("There should be one group", groupRepository.findAll().size() == 1);
        // Make invalid group change
        GroupEntity group = groupRepository.findAll().get(0);
        json = JSONBuilder.create()
                .add("title", TG.INVALID_GROUP_TITLE)
                .add("description", TG.VALID_GROUP_DESCRIPTION)
                .build();
        mockMvc.perform(patch("/api/groups/" + group.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
        group = groupRepository.findByTitle(TG.INVALID_GROUP_TITLE);
        assertTrue("There should not be a group with the invalid title",
                group == null);
    }
}

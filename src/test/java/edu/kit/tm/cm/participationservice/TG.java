package edu.kit.tm.cm.participationservice;

import org.joda.time.DateTime;

import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.IssueBuilder;

/**
 * This class is only used for test globals
 */
public final class TG {

    public final static String INVALID_GROUP_TITLE = "";
    public final static String VALID_GROUP_TITLE = "Group name";
    public final static String VALID_GROUP_DESCRIPTION = "A new group...";
    public final static String INVALID_GROUP_DESCRIPTION = null;
    public final static String TOO_LONG_GROUP_TITLE = repeat('a', GlobalConstants.MAX_GROUP_TITLE_LENGTH * 2);
    public final static String TOO_LONG_GROUP_DESCRIPTION = repeat('a',
            GlobalConstants.MAX_GROUP_DESCRIPTION_LENGTH * 2);

    public final static String VALID_ISSUE_TITLE = "New Issue";
    public final static String VALID_ISSUE_QUESTION = "issue question";
    public final static String VALID_ISSUE_DESCRIPTION = "issue description";
    public final static String VALID_ISSUE_CURRENT_SOLUTION = "current solution";
    public final static DateTime VALID_ISSUE_START_DATE = DateTime.now();
    public final static DateTime VALID_ISSUE_SOL_END_DATE = DateTime.now().plusHours(1);
    public final static DateTime VALID_ISSUE_VOTE_END_DATE = DateTime.now().plusHours(2);
    public final static GroupEntity VALID_GROUP = new GroupEntity(VALID_GROUP_TITLE,
            VALID_GROUP_DESCRIPTION);
    public final static String INVALID_ISSUE_TITLE = "";
    public final static String INVALID_ISSUE_QUESTION = repeat('a', 300);
    public final static String INVALID_ISSUE_DESCRIPTION = repeat('a', 2000);
    public final static String INVALID_ISSUE_CURRENT_SOLUTION = null;
    public final static DateTime INVALID_ISSUE_START_DATE = DateTime.now().plusYears(1);
    public final static DateTime INVALID_ISSUE_SOL_END_DATE = DateTime.now().minusYears(1);
    public final static DateTime INVALID_ISSUE_VOTE_END_DATE = DateTime.now().minusYears(2);
    public final static DateTime NULL_ISSUE_SOL_END_DATE = null;
    public final static DateTime NULL_ISSUE_VOTE_END_DATE = null;
    public final static String TOO_LONG_ISSUE_TITLE = repeat('b', GlobalConstants.MAX_ISSUE_TITLE_LENGTH * 2);
    public final static String TOO_LONG_ISSUE_DESCRIPTION = repeat('x',
            GlobalConstants.MAX_ISSUE_DESCRIPTION_LENGTH * 2);

    public static final String COMMENT_CONTENT = "example comment";

    public final static int VALID_RATING = 5;
    public final static int INVALID_RATING = -4;

    public final static String VALID_EMAIL = "vorname.nachname@example.com";
    public final static String DIFFERENT_VALID_EMAIL = "abc.123@example.com";
    public final static String INVALID_EMAIL = "vorname.nachnameexample.com";
    public final static String VALID_PASSWORD = "password";
    public final static String INVALID_PASSWORD = null;
    public final static String VALID_ALIAS = "alias";
    public final static String DIFFERENT_VALID_ALIAS = "anonymous";
    public final static String INVALID_ALIAS = null;

    public final static String INVALID_SOLUTION_TITLE = null;
    public final static String VALID_SOLUTION_TITLE = "solution_title";
    public final static String VALID_SOLUTION_DESCRIPTION = "this is a description";
    public final static Issue VALID_SOLUTION_ISSUE = IssueBuilder.create()
            .setCurrentSolution("testCurrentSolution")
            .setTitle("testTitle")
            .build();

    private TG() {}

    private static String repeat(char c, int length) {
        String result = "";
        for (int i = 0; i < length; i++) {
            result += c;
        }
        return result;
    }
}

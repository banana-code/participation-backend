package edu.kit.tm.cm.participationservice.test.web.controller.validators;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.validation.Errors;

import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.web.messages.request.GroupChangeMessage;

public class GroupChangeMessageValidatorTest extends AbstractValidatorTest {

    @Test
    public void validGroupTest() {
        Errors errors = validate(new GroupChangeMessage(TG.VALID_GROUP_TITLE,
                TG.VALID_GROUP_DESCRIPTION));
        assertFalse("Valid group change message must not be rejected", errors.hasErrors());
    }

    @Test
    public void invalidGroupNameTest() {
        Errors errors = validate(new GroupChangeMessage(TG.INVALID_GROUP_TITLE,
                TG.VALID_GROUP_DESCRIPTION));
        assertTrue("Invalid group change message must be rejected", errors.hasErrors());
    }

    @Test
    public void invalidGroupDescriptionTest() {
        Errors errors = validate(new GroupChangeMessage(TG.INVALID_GROUP_TITLE,
                TG.VALID_GROUP_DESCRIPTION));
        assertTrue("Invalid group change message must be rejected", errors.hasErrors());
    }

    @Test
    public void tooLongGroupDescriptionTest() {
        Errors errors = validate(new GroupChangeMessage(TG.VALID_GROUP_TITLE,
                TG.TOO_LONG_GROUP_DESCRIPTION));
        assertTrue("Invalid group change message must be rejected", errors.hasErrors());
    }

    @Test
    public void tooLongGroupTitleTest() {
        Errors errors = validate(new GroupChangeMessage(TG.TOO_LONG_GROUP_TITLE,
                TG.VALID_GROUP_DESCRIPTION));
        assertTrue("Invalid group change message must be rejected", errors.hasErrors());
    }

}

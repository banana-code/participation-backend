package edu.kit.tm.cm.participationservice.test.web.controller.validators;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.validation.Errors;

import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.web.messages.request.IssueMessage;

public class IssueMessageValidatorTest extends AbstractValidatorTest {

    @Test
    public void validMessageWithoutDates() {
        IssueMessage msg = new IssueMessage(TG.VALID_ISSUE_TITLE, TG.VALID_ISSUE_QUESTION,
                TG.VALID_ISSUE_DESCRIPTION, TG.VALID_ISSUE_CURRENT_SOLUTION, null, null, null);
        Errors errors = validate(msg);
        assertFalse("Valid issue message must not be rejected", errors.hasErrors());
    }

    @Test
    public void validMessageWithDates() {
        IssueMessage msg = new IssueMessage(TG.VALID_ISSUE_TITLE, TG.VALID_ISSUE_QUESTION,
                TG.VALID_ISSUE_DESCRIPTION, TG.VALID_ISSUE_CURRENT_SOLUTION, TG.VALID_ISSUE_START_DATE.toString(),
                TG.VALID_ISSUE_SOL_END_DATE, TG.VALID_ISSUE_VOTE_END_DATE);
        System.out.println(msg.getStartDate());
        Errors errors = validate(msg);
        assertFalse("Valid issue message must not be rejected", errors.hasErrors());
    }

    @Test
    public void titleAndDescriptionTooLong() {
        IssueMessage msg = new IssueMessage(TG.TOO_LONG_ISSUE_TITLE, TG.VALID_ISSUE_QUESTION,
                TG.TOO_LONG_ISSUE_DESCRIPTION, TG.VALID_ISSUE_CURRENT_SOLUTION, TG.VALID_ISSUE_START_DATE.toString(),
                TG.VALID_ISSUE_SOL_END_DATE, TG.VALID_ISSUE_VOTE_END_DATE);
        System.out.println(msg.getStartDate());
        Errors errors = validate(msg);
        assertTrue("Issue message with too long title and description must be rejected", errors.getErrorCount() == 2);
    }

    @Test
    public void invalidDates() {
        IssueMessage msg = new IssueMessage(TG.VALID_ISSUE_TITLE, TG.VALID_ISSUE_QUESTION,
                TG.VALID_ISSUE_DESCRIPTION, TG.VALID_ISSUE_CURRENT_SOLUTION, TG.VALID_ISSUE_START_DATE.toString(),
                TG.INVALID_ISSUE_SOL_END_DATE, TG.INVALID_ISSUE_VOTE_END_DATE);
        System.out.println(msg.getStartDate());
        Errors errors = validate(msg);
        assertTrue("Invalid dates must be rejected", errors.getErrorCount() >= 3);
    }

    @Test
    public void nullDates() {
        IssueMessage msg = new IssueMessage(TG.VALID_ISSUE_TITLE, TG.VALID_ISSUE_QUESTION,
                TG.VALID_ISSUE_DESCRIPTION, TG.VALID_ISSUE_CURRENT_SOLUTION, TG.VALID_ISSUE_START_DATE.toString(),
                TG.NULL_ISSUE_SOL_END_DATE, TG.NULL_ISSUE_VOTE_END_DATE);
        System.out.println(msg.getStartDate());
        Errors errors = validate(msg);
        assertTrue("Invalid dates must be rejected", errors.getErrorCount() == 2);
    }

}

package edu.kit.tm.cm.participationservice.test.web.controller.validators;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;

import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.domain.repositories.UserRepository;
import edu.kit.tm.cm.participationservice.security.domain.repositories.AccountRepository;
import edu.kit.tm.cm.participationservice.web.messages.request.RegistrationMessage;

public class RegistrationValidatorTest extends AbstractValidatorTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AccountRepository accountRepository;

    @Test
    public void validAccountTest() {
        Errors errors = validate(new RegistrationMessage(TG.VALID_ALIAS,
                TG.VALID_EMAIL, TG.VALID_PASSWORD));
        assertFalse("Valid account must not be rejected", errors.hasErrors());
    }

    @Test
    public void invalidEmailTest() {
        Errors errors = validate(new RegistrationMessage(TG.VALID_ALIAS,
                TG.INVALID_EMAIL, TG.VALID_PASSWORD));
        assertTrue("Invalid account must be rejected", errors.hasErrors());
    }

    @Test
    public void invalidPasswordTest() {
        Errors errors = validate(new RegistrationMessage(TG.VALID_ALIAS,
                TG.VALID_EMAIL, TG.INVALID_PASSWORD));
        assertTrue("Invalid account must be rejected", errors.hasErrors());
    }

    @Test
    public void invalidAliasTest() {
        Errors errors = validate(new RegistrationMessage(TG.INVALID_ALIAS,
                TG.VALID_EMAIL, TG.VALID_PASSWORD));
        assertTrue("Invalid account must be rejected", errors.hasErrors());
    }
}

package edu.kit.tm.cm.participationservice.test.web.controller.validators;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.validation.Errors;

import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.web.messages.request.SolutionMessage;

public class SolutionValidatorTest extends AbstractValidatorTest {

    @Test
    public void validTitleTest() {
        Errors errors = validate(new SolutionMessage(TG.VALID_SOLUTION_TITLE, null));
        assertFalse("Valid solution must not be rejected", errors.hasErrors());
    }

    @Test
    public void invalidTitleTest() {
        Errors errors = validate(new SolutionMessage(null, null));
        assertTrue("Invalid solution must be rejected", errors.hasErrors());
    }

}

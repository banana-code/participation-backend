package edu.kit.tm.cm.participationservice.test.web.controller.validators;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.validation.Errors;

import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.web.messages.request.RatingMessage;

public class RatingValidatorTest extends AbstractValidatorTest {

    @Test
    public void validRatingTest() {
        Errors errors = validate(new RatingMessage(TG.VALID_RATING));
        assertFalse("Valid rating must not be rejected", errors.hasErrors());
    }

    @Test
    public void invalidRatingTest() {
        Errors errors = validate(new RatingMessage(TG.INVALID_RATING));
        assertTrue("Invalid rating must not rejected", errors.hasErrors());
    }
}

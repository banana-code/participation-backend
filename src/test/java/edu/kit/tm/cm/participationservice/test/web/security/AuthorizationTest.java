package edu.kit.tm.cm.participationservice.test.web.security;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;

import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.JSONBuilder;
import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.domain.model.Comment;
import edu.kit.tm.cm.participationservice.domain.model.GroupEntity;
import edu.kit.tm.cm.participationservice.domain.model.Issue;
import edu.kit.tm.cm.participationservice.domain.model.Membership;
import edu.kit.tm.cm.participationservice.domain.model.Membership.MembershipState;
import edu.kit.tm.cm.participationservice.domain.model.Solution;
import edu.kit.tm.cm.participationservice.domain.model.User;
import edu.kit.tm.cm.participationservice.security.domain.model.Account.AccountState;

public class AuthorizationTest extends AbstractTest {

    private static final int nGroups = 2;
    private static final int nIssues = 2;
    private static final int nSolutions = 2;
    private static final int nComments = 2;
    private static final int nAccounts = 3;

    private static AccountState[] states = {
            AccountState.ADMIN,
            AccountState.VALID,
            AccountState.VALID
    };

    @Before
    public void setup() throws Exception {
        int index;
        // create accouns/users
        for (int h = 0; h < nAccounts; h++) {
            createAccount(TG.VALID_EMAIL + String.valueOf(h), TG.VALID_PASSWORD, TG.VALID_ALIAS + String.valueOf(h),
                    states[h]);
        }

        // create groups
        index = 0;
        for (int h = 0; h < nAccounts; h++) {
            login(TG.VALID_EMAIL + String.valueOf(h));
            for (int i = 0; i < nGroups; i++) {
                String json = JSONBuilder.create()
                        .add("title", TG.VALID_GROUP_TITLE + String.valueOf(index))
                        .add("description", TG.VALID_GROUP_DESCRIPTION)
                        .build();
                mockMvc.perform(post("/api/groups")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                        .andExpect(status().isCreated());
                index++;
            }
        }

        // create issues
        index = 0;
        for (GroupEntity group : groupRepository.findAll()) {
            login(group.getCreatedBy().getAccount().getEmail());
            for (int j = 0; j < nIssues; j++) {
                String json = JSONBuilder.create()
                        .add("title", "issue title" + String.valueOf(index))
                        .build();
                mockMvc.perform(post("/api/groups/" + group.getId() + "/issues/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                        .andExpect(status().isCreated());
                index++;
                json = JSONBuilder.create()
                        .add("title", "issue title" + String.valueOf(index))
                        .add("startDate", "now")
                        .add("solutionEndDate", DateTime.now().plusHours(1).toString())
                        .add("voteEndDate", DateTime.now().plusHours(1).toString())
                        .build();
                mockMvc.perform(post("/api/groups/" + group.getId() + "/issues/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                        .andExpect(status().isCreated());
                index++;
            }
        }

        // create solutions
        index = 0;
        for (Issue issue : issueRepository.findAll()) {
            login(issue.getCreatedBy().getAccount().getEmail());
            for (int k = 0; k < nSolutions; k++) {
                String json = JSONBuilder.create()
                        .add("title", TG.VALID_SOLUTION_TITLE + String.valueOf(index))
                        .add("description", TG.VALID_SOLUTION_DESCRIPTION)
                        .build();
                mockMvc.perform(
                        post("/api/groups/" + issue.getGroupId() + "/issues/" + issue.getId()
                                + "/solutions/")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json))
                        .andExpect(status().isCreated());
                index++;
            }
        }

        // create comments
        index = 0;
        for (Solution solution : solutionRepository.findAll()) {
            login(solution.getCreatedBy().getAccount().getEmail());
            for (int l = 0; l < nComments; l++) {
                String json = JSONBuilder.create()
                        .add("content", TG.COMMENT_CONTENT + String.valueOf(index))
                        .add("solutionCommentType", "PRO")
                        .build();
                mockMvc.perform(
                        post(
                                "/api/groups/" + solution.getIssue().getGroupId() + "/issues/"
                                        + solution.getIssue().getId()
                                        + "/solutions/" + solution.getId() + "/comments")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json))
                        .andExpect(status().isCreated());
                index++;
            }
        }
        for (Issue issue : issueRepository.findAll()) {
            login(issue.getCreatedBy().getAccount().getEmail());
            for (int l = 0; l < nComments; l++) {
                String json = JSONBuilder.create()
                        .add("content", TG.COMMENT_CONTENT + String.valueOf(index))
                        .build();
                mockMvc.perform(
                        post("/api/groups/" + issue.getGroupId() + "/issues/" + issue.getId() + "/comments")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json))
                        .andExpect(status().isCreated());
                index++;
            }
        }
    }

    @Test
    public void testGetAuthorizations() throws Exception {
        for (User user : userRepository.findAll()) {
            login(user.getAccount().getEmail());
            for (Issue issue : issueRepository.findAll()) {
                boolean isAuthorized = (user.getAccount().getState() == AccountState.ADMIN && issue.isPublished())
                        || issue.getCreatedBy().equals(user)
                        || (userIsInGroup(user, issue.getGroup()) && issue.isPublished());
                if (isAuthorized) {
                    mockMvc.perform(get("/api/groups/" + issue.getGroupId() + "/issues/" + issue.getId()))
                            .andExpect(status().isOk());
                    mockMvc.perform(
                            get("/api/groups/" + issue.getGroupId() + "/issues/" + issue.getId() + "/solutions/"))
                            .andExpect(status().isOk());
                    mockMvc.perform(
                            get("/api/groups/" + issue.getGroupId() + "/issues/" + issue.getId() + "/comments/"))
                            .andExpect(status().isOk());
                } else {
                    mockMvc.perform(get("/api/groups/" + issue.getGroupId() + "/issues/" + issue.getId()))
                            .andExpect(status().isForbidden());
                    mockMvc.perform(
                            get("/api/groups/" + issue.getGroupId() + "/issues/" + issue.getId() + "/solutions/"))
                            .andExpect(status().isForbidden());
                    mockMvc.perform(
                            get("/api/groups/" + issue.getGroupId() + "/issues/" + issue.getId() + "/comments/"))
                            .andExpect(status().isForbidden());
                }
                for (Solution solution : issue.getSolutions()) {
                    if (isAuthorized) {
                        mockMvc.perform(
                                get("/api/groups/" + issue.getGroupId() + "/issues/" + issue.getId() + "/solutions/"
                                        + solution.getId() + "/comments"))
                                .andExpect(status().isOk());
                    } else {
                        mockMvc.perform(
                                get("/api/groups/" + issue.getGroupId() + "/issues/" + issue.getId() + "/solutions/"
                                        + solution.getId() + "/comments"))
                                .andExpect(status().isForbidden());
                    }
                    for (Comment comment : solution.getComments()) {
                        if (isAuthorized) {
                            mockMvc.perform(get("/api/comments/" + comment.getId()))
                                    .andExpect(status().isOk());
                        } else {
                            mockMvc.perform(get("/api/comments/" + comment.getId()))
                                    .andExpect(status().isForbidden());
                        }
                    }
                }
                for (Comment comment : issue.getComments()) {
                    if (isAuthorized) {
                        mockMvc.perform(get("/api/comments/" + comment.getId()))
                                .andExpect(status().isOk());
                    } else {
                        mockMvc.perform(get("/api/comments/" + comment.getId()))
                                .andExpect(status().isForbidden());
                    }
                }
            }
        }
    }

    private boolean userIsInGroup(User user, GroupEntity group) {
        for (Membership membership : group.getMembers()) {
            if (membership.getUser().equals(user) &&
                    (membership.getState().equals(MembershipState.OWNER)
                            || membership.getState().equals(MembershipState.MODERATOR)
                            || membership.getState().equals(MembershipState.MEMBER))) {
                return true;
            }
        }
        return false;
    }
}

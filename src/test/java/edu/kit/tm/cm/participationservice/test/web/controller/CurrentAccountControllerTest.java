package edu.kit.tm.cm.participationservice.test.web.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.springframework.http.MediaType;

import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.JSONBuilder;
import edu.kit.tm.cm.participationservice.TG;
import edu.kit.tm.cm.participationservice.domain.model.User;

public class CurrentAccountControllerTest extends AbstractTest {

    @Test
    public void testCurrentUser() throws Exception {
        createAccountAndLogin(TG.VALID_EMAIL, TG.VALID_PASSWORD,
                TG.VALID_ALIAS);

        final int accountID = accountRepository.findByEmail(TG.VALID_EMAIL).getId().intValue();
        final String userAlias = accountRepository.findByEmail(TG.VALID_EMAIL).getUser().getAlias();

        mockMvc.perform(
                get("/api/me"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.id").value(accountID))
                .andExpect(jsonPath(".user.alias").value(userAlias));
    }

    @Test
    public void changePassword() throws Exception {
        createAccountAndLogin(TG.VALID_EMAIL, TG.VALID_PASSWORD,
                TG.VALID_ALIAS);
        String newPassword = "newPassword";
        String json = JSONBuilder.create()
                .add("oldPassword", TG.VALID_PASSWORD)
                .add("newPassword", newPassword)
                .build();
        mockMvc.perform(patch("/api/me/password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        // Login with new password and create group
        login(TG.VALID_EMAIL, newPassword);
        json = JSONBuilder.create()
                .add("title", TG.VALID_GROUP_TITLE)
                .add("description", TG.VALID_GROUP_DESCRIPTION)
                .build();
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        assertTrue("There should be one group", groupRepository.findAll().size() == 1);

    }

    @Test
    public void changeAlias() throws Exception {
        createAccountAndLogin(TG.VALID_EMAIL, TG.VALID_PASSWORD,
                TG.VALID_ALIAS);
        String newAlias = "newAlias";
        String json = JSONBuilder.create()
                .add("alias", newAlias)
                .build();
        mockMvc.perform(patch("/api/me/alias")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        User user = userRepository.findByAlias(newAlias);
        assertTrue("User should have the new alias", user != null);
    }

}

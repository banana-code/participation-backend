package edu.kit.tm.cm.participationservice.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import edu.kit.tm.cm.participationservice.AbstractTest;
import edu.kit.tm.cm.participationservice.JSONBuilder;
import edu.kit.tm.cm.participationservice.TG;

public class LoginLogoutTest extends AbstractTest {

    @Test
    public void testLogin() throws Exception {
        createAccount(TG.VALID_EMAIL, TG.VALID_PASSWORD, TG.VALID_ALIAS);
        String json = JSONBuilder.create()
                .add("email", TG.VALID_EMAIL)
                .add("password", TG.VALID_PASSWORD)
                .build();
        MvcResult result = mockMvcSecurity.perform(post("/api/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk()).andReturn();
        String xToken = result.getResponse().getHeader("X-Token");
        mockMvcSecurity.perform(post("/api/logout")
                .header("X-Token", xToken))
                .andExpect(status().isOk());
    }

    @Test
    public void loginWithWrongPassword() throws Exception {
        createAccount(TG.VALID_EMAIL, TG.VALID_PASSWORD, TG.VALID_ALIAS);
        String json = JSONBuilder.create()
                .add("email", TG.VALID_EMAIL)
                .add("password", "foobar")
                .build();
        mockMvcSecurity.perform(post("/api/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testFailedLoginLogout() throws Exception {
        String json = JSONBuilder.create()
                .add("email", TG.VALID_EMAIL)
                .add("password", TG.VALID_PASSWORD)
                .build();
        mockMvcSecurity.perform(post("/api/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isUnauthorized());
        mockMvcSecurity.perform(post("/api/logout")
                .header("X-Token", "randomString"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void logoutWithoutLoginTest() throws Exception {
        mockMvcSecurity.perform(post("/api/logout"))
                .andExpect(status().isBadRequest());
    }
}

# Participation Backend
The Participation Service is a web based tool to help finding joint decisions. The decision is made using the principle of the [systemic consensus](http://www.ic.org/wiki/systemic-consensus-principle/).

This is the backend of the Participation Service written in Java using [Spring](https://spring.io/).
We use [Spring Data](http://projects.spring.io/spring-data/) to access the database and persist objects and [Spring Security](http://projects.spring.io/spring-security/) to configure the user authentication.

It is designed to work well with a running instance of the [Participation Frontend](https://gitlab.com/banana-code/participation-frontend).

# Required Tools
- JDK 7 or higher
- [Maven](https://maven.apache.org/)
- [git](https://git-scm.com/)

The project was developed using the [Eclipse IDE](https://eclipse.org/) so it's highly recommended using it as well.

# How To Use

## To run a local development server
- clone this repository and `cd` into it
- run `mvn exec:java`

## To deploy
- [optional] put the frontend files into the `src/main/resources/static/*` directory (if you want to serve the frontend from the `*.war` too)
- run `maven package` from the project root directory which will create a `*.war` file in the target directory
- Deploy the `*.war` e.g. with Tomcat.

## Configuration
Configuration options in [`src/main/resources/application.properties`](src/main/resources/application.properties):
  - to set the database type (we support H2 and mysql), location and authentication options. (H2 is for development only)
  - to change the uri location for the static content (frontend) and the backend
  - to force https
  - to enable CORS and a regex which describes allowed origins
  - to enable e-mail verification, a regex with allowed e-mail patterns for registration and the senders e-mail address for account verification/password reset
  - to change login token size/validity duration
  - to enable generation of test data

  How a change can be made is documented in the application.properties file

## Test Data
If you want to test your running server or your work on the frontend there is some test data in [`edu.kit.tm.cm.participationservice.demo.Bootstrap`](src/main/java/edu/kit/tm/cm/participationservice/demo/Bootstrap.java).
The account with email **ad.min@example.com** and password **ad** has administrator privileges and can be used to traverse the frontend.

If you have H2 database enabled (as it is recommended for development) you can access the database on port 8082

## REST Interface
The backend offers a REST interface which allows the user to create, change or delete domain objects.

Documentation of the REST interface can be found in [`documentation/Backend Interface.ods`](documentation/Backend%20Interface.ods).


# License

*Copyright 2015 Felix Hofmann, Leonard Otto, Maximilian Schick, Daniel Stroh and
Raphael von der Grün*

The words *this program* refer to this repository in its entirety. Unless
explicitly stated otherwise, the following license conditions apply to any files
that are included in this repository.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Afffero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Afffero General Public License for more
details.

You should have received a copy of the GNU Afffero General Public License along
with this program. If not, see <http://www.gnu.org/licenses/>.
